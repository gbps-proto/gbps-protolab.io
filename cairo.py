# -*- coding: utf-8 -*-



class cairo:
	
	ANTIALIAS_SUBPIXEL = 0
	EXTEND_REPEAT = 0
	FILTER_NEAREST = 0
	FONT_SLANT_NORMAL = 0
	FONT_WEIGHT_BOLD = 1
	FONT_WEIGHT_NORMAL = 2
	FORMAT_ARGB32 = 0
	FORMAT_RGB24 = 0
	OPERATOR_DEST_OUT = 1
	
	class Context:
		
		def __init__(self,*arg):
			#print "API CALL: Context.__init__"
			if arg[0] == surf: self.name = "window.ctx"
			if arg[0] == surf_mask: self.name = "window.ctx_mask"
			self.pos = (0,0)
			self.scalee = 1
			self.path = 0
		
		def arc(self,*arg):
			#print "API CALL: Context.arc"
			if not self.path:
				self.path = 1
				js.eval(self.name + ".beginPath();")
			js.eval(self.name + ".arc(" + str(arg[0]) + "," + str(arg[1]) + "," + str(arg[2]) + "," + str(arg[3]) + "," + str(arg[4]) + ",false);")
		
		def clip(self,*arg):
			#print "API CALL: Context.clip"
			self.path = 0
			js.eval(self.name + ".save();")
			js.eval(self.name + ".clip();")
		
		def fill(self,*arg):
			#print "API CALL: Context.fill"
			self.path = 0
			js.eval(self.name + ".fill();")
		
		def get_font_options(self,*arg):
			#print "API CALL: Context.get_font_options"
			return cairo.FontOptions()
		
		def get_source(self,*arg):
			#print "API CALL: Context.get_source"
			return cairo.ImageSurface()
		
		def line_to(self,*arg):
			#print "API CALL: Context.line_to"
			js.eval(self.name + ".lineTo(" + str(arg[0]) + "," + str(arg[1]) + ");")
		
		def mask_surface(self,*arg):
			#TODO
			#print "API CALL: Context.mask_surface"
			
			js.eval("window.ctx_mask.globalCompositeOperation = 'source-in';")
			js.eval("window.ctx_mask.fillStyle = " + self.name + ".fillStyle;")
			js.eval("window.ctx_mask.fillRect(0,0," + str(width) + "," + str(height) + ");")
			js.eval("window.ctx_mask.globalCompositeOperation = 'source-over';")
			
			js.eval(self.name + ".drawImage(document.getElementById('surface_mask'),0,0);")
		
		def move_to(self,*arg):
			#print "API CALL: Context.move_to"
			self.pos = arg
			if not self.path:
				self.path = 1
				js.eval(self.name + ".beginPath();")
			js.eval(self.name + ".moveTo(" + str(arg[0]) + "," + str(arg[1]) + ");")
		
		def rectangle(self,*arg):
			#print "API CALL: Context.rectangle"
			if not self.path:
				self.path = 1
				js.eval(self.name + ".beginPath();")
			js.eval(self.name + ".rect(" + str(arg[0]) + "," + str(arg[1]) + "," + str(arg[2]) + "," + str(arg[3]) + ");")
		
		def reset_clip(self,*arg):
			#print "API CALL: Context.reset_clip"
			js.eval(self.name + ".restore();")
		
		def restore(self,*arg):
			#print "API CALL: Context.restore"
			self.scalee = 1
			js.eval(self.name + ".restore();")
			#js.eval(self.name + ".globalCompositeOperation = 'source-over';")
		
		def save(self,*arg):
			#print "API CALL: Context.save"
			js.eval(self.name + ".save();")
		
		def scale(self,*arg):
			#print "API CALL: Context.scale"
			self.scalee = arg[0]
			js.eval(self.name + ".scale(" + str(arg[0]) + "," + str(arg[1]) + ");")
		
		def select_font_face(self,*arg):
			#print "API CALL: Context.select_font_face"
			if arg[0] == "Droid Sans" and not arg[2] == cairo.FONT_WEIGHT_BOLD: self.font = "sans-normal"
			if arg[0] == "Droid Sans" and arg[2] == cairo.FONT_WEIGHT_BOLD: self.font = "sans-bold"
			if arg[0] == "DejaVu Sans Mono" and not arg[2] == cairo.FONT_WEIGHT_BOLD: self.font = "mono-normal"
			if arg[0] == "DejaVu Sans Mono" and arg[2] == cairo.FONT_WEIGHT_BOLD: self.font = "mono-bold"
		
		def set_font_options(self,*arg):
			#print "API CALL: Context.set_font_options"
			pass
		
		def set_font_size(self,*arg):
			#print "API CALL: Context.set_font_size"
			self.size = arg[0]
		
		def set_line_width(self,*arg):
			#print "API CALL: Context.set_line_width"
			js.eval(self.name + ".lineWidth = " + str(arg[0]) + ";")
		
		def set_operator(self,*arg):
			#TODO
			#print "API CALL: Context.set_operator"
			op = "source-over"
			if arg[0] == cairo.OPERATOR_DEST_OUT: op = "destination-out"
			js.eval(self.name + ".globalCompositeOperation = '" + op + "';")
		
		def set_source(self,*arg):
			#print "API CALL: Context.set_source"
			js.eval("var gradient = " + self.name + ".createLinearGradient(" + str(arg[0].pos[0]) + "," + str(arg[0].pos[1]) + "," + str(arg[0].pos[2]) + "," + str(arg[0].pos[3]) + ");")
			for x in arg[0].stops:
				js.eval("gradient.addColorStop(" + str(x[0]) + ",'" + x[1] + "');")
			js.eval(self.name + ".fillStyle = gradient;")
		
		def set_source_rgb(self,*arg):
			#print "API CALL: Context.set_source_rgb"
			js.eval(self.name + ".fillStyle = " + self.name + ".strokeStyle = 'rgb(" + str(arg[0] * 255) + "," + str(arg[1] * 255) + "," + str(arg[2] * 255) + ")';")
		
		def set_source_rgba(self,*arg):
			#print "API CALL: Context.set_source_rgba"
			js.eval(self.name + ".fillStyle = " + self.name + ".strokeStyle = 'rgba(" + str(arg[0] * 255) + "," + str(arg[1] * 255) + "," + str(arg[2] * 255) + "," + str(arg[3]) + ")';")
		
		def set_source_surface(self,*arg):
			#print "API CALL: Context.set_source_surface"
			img = ""
			if arg[0] == surf_tile1: img = "window.tile1"
			if arg[0] == surf_tile2: img = "window.tile2"
			js.eval("var pattern = " + self.name + ".createPattern(" + img + ",'repeat');")
			js.eval(self.name + ".fillStyle = pattern;")
		
		def show_text(self,*arg):
			#print "API CALL: Context.show_text"
			js.eval(self.name + ".font = '" + str(self.size) + "px" + " " + self.font + "';")
			
			if self.font in {"mono-normal","mono-bold"}:
				x = self.pos[0]
				for c in arg[0]:
					js.eval(self.name + ".fillText('" + hexEncode(c) + "'," + str(x) + "," + str(self.pos[1]) + ");")
					x += 8
			else:
				js.eval(self.name + ".fillText('" + hexEncode(arg[0]) + "'," + str(self.pos[0]) + "," + str(self.pos[1]) + ");")
		
		def stroke(self,*arg):
			#print "API CALL: Context.stroke"
			self.path = 0
			js.eval(self.name + ".stroke();")
		
		def text_extents(self,*arg):
			#print "API CALL: Context.text_extents"
			js.eval(self.name + ".font = '" + str(self.size * self.scalee) + "px" + " " + self.font + "';")
			width = float(js.eval(self.name + ".measureText('" + hexEncode(arg[0]) + "').width;"))
			return (0,0,width)
		
		def translate(self,*arg):
			#print "API CALL: Context.translate"
			js.eval(self.name + ".translate(" + str(arg[0]) + "," + str(arg[1]) + ");")
	
	class FontOptions:
		
		def __init__(self,*arg):
			#print "API CALL: FontOptions.__init__"
			pass
		
		def set_antialias(self,*arg):
			#print "API CALL: FontOptions.set_antialias"
			pass
	
	class ImageSurface:
		
		def __init__(self,*arg):
			#print "API CALL: ImageSurface.__init__"
			pass
		
		def set_extend(self,*arg):
			#print "API CALL: ImageSurface.set_extend"
			pass
		
		def set_filter(self,*arg):
			#print "API CALL: ImageSurface.set_filter"
			pass
		
		def get_matrix(self,*arg):
			#print "API CALL: ImageSurface.get_matrix"
			return cairo.Matrix()
		
		def set_matrix(self,*arg):
			#print "API CALL: ImageSurface.set_matrix"
			pass
		
		@staticmethod
		def create_for_data(*arg):
			#print "API CALL: ImageSurface.create_for_data"
			return cairo.ImageSurface()
		
		@staticmethod
		def create_from_png(*arg):
			#print "API CALL: ImageSurface.create_from_png"
			return cairo.ImageSurface()
	
	class LinearGradient:
		
		def __init__(self,*arg):
			#print "API CALL: LinearGradient.__init__"
			self.pos = arg
			self.stops = []
		
		def add_color_stop_rgba(self,*arg):
			#print "API CALL: LinearGradient.add_color_stop_rgba"
			self.stops += [(arg[0],"rgba(" + str(arg[1] * 255) + "," + str(arg[2] * 255) + "," + str(arg[3] * 255) + "," + str(arg[4]) + ")")]
	
	class Matrix:
		
		def __init__(self,*arg):
			#print "API CALL: Matrix.__init__"
			pass
		
		def scale(self,*arg):
			#print "API CALL: Matrix.scale"
			pass


