# -*- coding: utf-8 -*-



try:
	
	
	
	import array
	import copy
	import math
	import re
	import types
	
	
	
	#sudo apt-get update; sudo apt-get install -y meld python2 python-cairo python-pygame python-wxgtk3.0; sudo apt-get clean
	#while python2 <(bash preprocess.bash gbps_proto.py); do sleep 1; done
	
	
	
	titlee = u"gbps-proto rev2108"
	welcome = u"--- Welcome to %s " % titlee
	print
	print
	print welcome + u"-" * (80 - len(welcome))
	print
	print
	
	
	
	#(width,height) = (800,600) # window dimensions
	
	settings = {}
	try:
		with open("settings.json") as f:
			settings = json.load(f)
	except: pass
	
	ct = 0 # current tab
	#ct = 1
	files = [
		# path, file, sha256
		
		## TODO:
		# compare sha256 on: open, tab select, save
		[None,None,None,{"no parse": True,"permanent": True}],
		[None,None,None,{}],
		[None,None,None,{}],
		[None,None,None,{}],
		[None,None,None,{}],
		[None,None,None,{}],
		[None,None,None,{}],
		[None,None,None,{}],
		[None,None,None,{}],
		[None,None,None,{}],
		[None,None,None,{"no parse": True}], # TODO
	]
	files_init = tuple(id(x) for x in files)
	#files_index = set() # TODO open same file only once NO!
	titles = [
		u"0. Willkommen",
		u"1. Abfrage",
		u"2. Umschreiben / Generieren",
		u"3. Merkmale",
		u"4. Konfigurieren",
		u"5. Richtlinien",
		u"6. Code-Schemata",
		u"7. Orthogonale Bereiche",
		u"8. Komplexitätsklassen",
		u"9. Datenfluss",
		u"10. Erläuterung des Programmcodes",
	]
	lines = [
		[
			[[("mark","text formatting",0),[],{"sc": (.1,.3,.6)}],u""],
			[u""],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"==========================================="],
			[u" Herzlich Willkommen und vielen Dank dafür,"],
			[u" dass Sie gbps-proto gestartet haben."],
			[u"==========================================="],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u""],
			[u""],
			[u""],
			[u"- Bitte maximieren Sie das Fenster, um diesen Text besser lesen zu können."],
			[u""],
			[u"- Gbps-proto ist ein Prototyp des graphgestützten Programmiersystems, einem Werkzeugkasten zur Herstellung leistungsfähigerer Software. Die enthaltenen Funktionen geben einen Ausblick auf die Möglichkeiten dieser neuen Technologie. Es handelt sich um kein vollständiges Produkt, insbesondere sind weder Datenbank noch Programmanalyse enthalten. Viele der enthaltenen Werkzeuge funktionieren nur im Rahmen dieser Demonstration."],
			[u""],
			[u"- Bitte verwenden Sie gbps-proto nicht im produktiven Umfeld."],
			[u""],
			[u""],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"==========================================="],
			[u" Einführung in die Benutzeroberfläche:"],
			[u"==========================================="],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u""],
			[u""],
			[u""],
			[u"- Am oberen Rand des Fensters sehen Sie die Tab-Leiste mit allen geöffneten Tabs (Dateien). Mit einem Klick oder durch drehen am Mausrad wechseln Sie in einen anderen Tab. Um die Reihenfolge zu ändern, ziehen Sie einen Tab an eine andere Position. Halten Sie die rechte Maustaste, um ein Kontextmenü zu sehen. Ziehen Sie mit gehaltener Maustaste und lassen Sie auf dem gewünschten Eintrag los."],
			[u""],
			[u"- Rechts sehen Sie die Werkzeug-Schnellwahl mit Kurz-Infos in den geschlossenen Reitern. Mit einem Klick auf einen Reiter öffnen Sie diesen. Nutzen Sie das Mausrad um zu scrollen. Halten Sie die rechte Maustaste, um alle Werkzeuge in einem Kontextmenü zu sehen. Ziehen Sie mit gehaltener Maustaste und lassen Sie auf dem gewünschten Eintrag los. Wählen Sie \"X. Alle Reiter schließen\", um alle anderen Reiter zu schließen und so alle Kurz-Infos im Blick zu haben."],
			[u""],
			[u"- Der übrige Bereich, der sich von links unten über die Fenstermitte erstreckt, ist der Editor. Dort wird der Inhalt des gewählten Tabs gezeigt. Bei Tabs mit Programmcode befindet sich am rechten Rand des Editors eine verkleinerte Darstellung des Tabinhalts (Minimap). Nutzen Sie die Minimap wie eine Scroll-Leiste. Halten Sie die \"STRG\"-Taste für ein modifiziertes Verhalten bei Mausklicks und drehen des Mausrads. Verkleinern Sie dazu ggf. das Fenster. Der Editor unterstützt Copy-and-Paste sowie alle gängigen Maus- bzw. Tastaturbefehle."],
			[u""],
			[u"- Im Reiter \"0. Dateien / Tabs\" in der Werkzeug-Schnellwahl finden Sie Möglichkeiten für das Öffnen und Speichern von Dateien. Der Zustand von gbps-proto sowie alle geöffneten Tabs werden beim Beenden des Programms intern gespeichert und beim nächsten Programmstart wiederhergestellt. Nutzen Sie die Funktionen \"Tab speichern\", \"Tab speichern unter\" und \"Alle Tabs speichern\", um geöffnete Tabs in das Dateisystem der Festplatte zu schreiben. Außerhalb dieses Programms können jedoch keine Änderungen rückgängig gemacht werden."],
			[u""],
			[u""],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"==========================================="],
			[u" Einführung in die einzelnen Werkzeuge:"],
			[u"==========================================="],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u""],
			[u""],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"1. Abfrage.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"1. Abfrage\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Abfragen eine Auswahl. Die Ergebnisse werden im Editor hervorgehoben und in einer Tabelle aufgelistet. Mit einem Klick auf die Tabelle springen Sie im Editor zum entsprechenden Eintrag."],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"2. Umschreiben / Generieren.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"2. Umschreiben / Generieren\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Abfragen eine Auswahl. Die zur Auswahl passenden Stellen werden im Editor umgeschrieben. In dieser Demonstration können umgeschriebene Bereiche nicht bearbeitet werden. Deaktivieren Sie das Werkzeug, um den ursprünglichen Programmtext zu bearbeiten. Die umgeschriebenen Bereiche werden im Editor hervorgehoben und in Tabellen aufgelistet. Mit einem Klick auf eine Tabelle springen Sie im Editor zum entsprechenden Eintrag."],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"3. Merkmale.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"3. Merkmale\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Der Programmcode im Editor wird auf Vorkommen von Schleifen, Arithmetik und Datenzugriffen hin untersucht. Die Ergebnisse werden in Tabellen aufgelistet. In den Tabellen werden auch die Eigenschaften der gefundenen Vorkommen verglichen. Mit einem Klick auf eine Tabelle springen Sie im Editor zum entsprechenden Eintrag."],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"4. Konfigurieren.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"4. Konfigurieren\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Konfigurationen eine Auswahl. Die zur Auswahl passenden Stellen werden intern umgeschrieben und im Editor hervorgehoben. Es wird allerdings weiterhin der ursprüngliche Programmtext gezeigt. Die resultierende Umsetzung sehen Sie mit \"Resultierenden Programmtext einblenden\". Bei \"Tab speichern\", \"Tab speichern unter\" und \"Alle Tabs speichern\" wird ebenfalls der resultierende Programmtext verwendet."],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"5. Richtlinien.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"5. Richtlinien\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Richtlinien eine Auswahl. Der Programmcode im Editor wird auf Konformität zu den ausgewählten Richtlinien hin untersucht. Nicht-konforme Stellen werden im Editor deutlich hervorgehoben. In Kreisdiagrammen wird das Maß der Konformität des Programmcodes zu den ausgewählten Richtlinien dargestellt."],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"6. Code-Schemata.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"6. Code-Schemata\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Treffen Sie unter den Beispiel-Schemata eine Auswahl. Die zur Auswahl passenden Stellen werden im Editor vereinfacht dargestellt. Nicht-konforme Stellen werden im Editor deutlich hervorgehoben. In Kreisdiagrammen wird das Maß der Konformität des Programmcodes zu den ausgewählten Schemata dargestellt."],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"7. Orthogonale Bereiche.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"7. Orthogonale Bereiche\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Markieren Sie einen Bereich im Editor und klicken Sie auf \"Auswahl zu Gruppe ... hinzufügen\". Die gruppierten Bereiche werden in einer Tabelle katalogisiert. Mit \"Platzhalter für Gruppe ... aktivieren\" werden sie im Editor vereinfacht dargestellt."],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"8. Komplexitätsklassen.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"8. Komplexitätsklassen\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Der Programmcode im Editor wird auf Zeitkomplexität hin untersucht. Setzen Sie den Tastaturcursor (Caret) im Editor auf eine Position innerhalb einer Schleife, um die Ergebnisse zu sehen."],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"9. Datenfluss.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"9. Datenfluss\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Der Programmcode im Editor wird auf Datenfluss hin untersucht. Setzen Sie den Tastaturcursor (Caret) im Editor auf eine Variable, um die Ergebnisse zu sehen."],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"10. Erläuterung des Programmcodes.  ",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}],u"Wählen Sie den Reiter \"10. Erläuterung des Programmcodes\" in der Werkzeug-Schnellwahl und den entsprechenden Tab in der Tab-Leiste. Der Programmcode im Editor wird auf Zweck und Funktionsweise hin untersucht. Setzen Sie den Tastaturcursor (Caret) im Editor auf eine Position innerhalb einer Funktion, um die Ergebnisse zu sehen."],
			[u""],
			[u""],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],u"- Nochmals vielen Dank dafür, dass Sie sich für gbps-proto interessieren. Im Reiter \"12. Über dieses Programm\" in der Werkzeug-Schnellwahl finden Sie einen Link zu weiterem Informationsmaterial sowie eine Möglichkeit, mir eine E-Mail zu schreiben."],
			[u""],
			[u""],
			[u""],
		],
		[
			[u""],
			[u""],
			[u"# Division durch eine Variable"],
			[u""],
			[u""],
			[u"quotient = dividend / 10"],
			[u""],
			[u"quotient = dividend / divisor"],
			[u""],
			[u"rest = dividend % (10 * divisor)"],
			[u""],
			[u""],
			[u"# Datenzugriff mit einer Variablen als Schlüssel"],
			[u""],
			[u""],
			[u"eintrag = datenstruktur[\"graphbased\"]"],
			[u""],
			[u"eintrag = datenstruktur[schlüssel]"],
			[u""],
			[u"eintrag = datenstruktur[10 * schlüssel]"],
			[u""],
			[u""],
			[u"# Vergleich mit einem Literal"],
			[u""],
			[u""],
			[u"if variable1 == variable2:"],
			[u"\tpass"],
			[u""],
			[u"if variable1 > 10:"],
			[u"\tpass"],
			[u""],
			[u"if variable1 in {\"graph\",\"based\"}:"],
			[u"\tpass"],
			[u""],
			[u""],
			[u"# Funktionsaufruf innerhalb einer Schleife"],
			[u""],
			[u""],
			[u"rückgabewert = funktion(parameter)"],
			[u""],
			[u"for (zähler,eintrag) in enumerate(liste):"],
			[u"\tfunktion(zähler)"],
			[u""],
			[u"while True:"],
			[u"\tif funktion(parameter): pass"],
			[u""],
			[u""],
		],
		[
			[u""],
			[u""],
			[u"# Einfaches Beispiel"],
			[u""],
			[u""],
			[u"xxx"], # 
			[u""],
			[u""],
			[u"# Code mit orthogonalen Bestandteilen erzeugen"],
			[u""],
			[u""],
			[u"code erzeugen {"], # switch-case mit orthogonalen Bestandteilen
			[u"\t(a,z),"],
			[u"\t(a,b,x,y),"],
			[u"\t(c,z),"],
			[u"\t(x,y),"],
			[u"}"],
			[u""],
			[u""],
			[u"# Domänenspezifische Befehle umsetzen"],
			[u""],
			[u""],
			[u"physikalische_größen {"],
			[u"\tgeschwindigkeit = 10 m / 2 s"], # geschwindigkeit = größe(wert = 10,einheit = "meter").div(größe(wert = 2,einheit = "sekunden"))
			[u"}"],
			[u""],
			#[u"geldbeträge {"],
			#[u"\tkontostand += 100.00 Euro"], # kontostand = kontostand.add(betrag = 10000,währung = "Euro")
			#[u"}"],
			[u""],
			[u"geschäftslogik {"],
			[u"\tif kurs >= mindestkurs:"], # geschäftslogik.add(regel(bedingung = lambda: kurs >= mindestkurs,signale = (kurs,mindestkurs),aktion = aktion))
			[u"\t\tverkaufen()"], # def aktion(): verkaufen()
			[u"}"],
			[u""],
			[u""],
		],
		[
			[u""],
			[u""], # immer die äußerste Schleife
			[u"# Schleifen"], # Iterierung einer Liste, Enthält Funktionsaufruf, Keine polynomielle Laufzeit
			[u""],
			[u""],
			[u""],
			[u""], # immer die äußerste Operation
			[u"# Arithmetik"], # Enthält Konstante, Enthält Addition, Enthält Multiplikation
			[u""],
			[u""],
			[u""],
			[u""], # pro [; =,+=,++  # x[y].z = 123 gilt als schreiben
			[u"# Datenzugriffe"], # Datenstruktur lesen, Datenstruktur schreiben, Bedingte Ausführung
			[u""],
			[u""],
			[u""],
			[u""],
		],
		[
			[u""],
			[u""],
			[u"# Einfaches Beispiel"],
			[u""],
			[u""],
			[u"xxx"], # 
			[u""],
			[u""],
			[u"# Fehlertoleranz für Datenzugriffe einfügen"], # spezifischer identifier, nur dict
			[u""],
			[u""],
			[u"eintrag = datenstruktur[\"graphbased\"]"],
			[u""],
			[u"eintrag = datenstruktur[-10]"],
			[u""],
			[u"eintrag = datenstruktur[index][schlüssel].eigenschaft"],
			[u""],
			[u""],
			[u"# Unbehandelte Fälle aufzeichnen"], # betrifft nur ifs mit elif und ohne else
			[u""],
			[u""],
			[u" "],
			[u""],
			[u""],
		],
		[
			[u""],
			[u""],
			[u"# Einfaches Beispiel"],
			[u""],
			[u""],
			[u""],
			[u""],
			[u"# Laufzeit explizit begrenzen"],
			[u""],
			[u""],
			[u""],
			[u""],
			[u"# Daten konsistent halten"],
			[u""],
			[u""],
			[u""],
			[u""],
		],
		[
			[u""],
			[u""],
			[u"# Einfaches Beispiel"],
			[u""],
			[u""],
			[u""],
			[u""],
			[u"# Entwurfsmuster"],
			[u""],
			[u""],
			[u""],
			[u""],
			[u"# Duplikate mit Variationen"],
			[u""],
			[u""],
			[u""],
			[u""],
		],
		[
			[u""],
			[u""],
			[u""], # einzelnes Beispiel
			[u""], # Switch-Case mit orthogonalen Inhalten
			[u""],
			[u""],
			[u""],
		],
		[
			[u""],
			[u""],
			[u"# Einfaches Beispiel"],
			[u""],
			[u""],
			[u""],
			[u""],
			[u"# Polynomielle Laufzeit"],
			[u""],
			[u""],
			[u""],
			[u""],
			[u"# Rekursion und exponentielle Laufzeit"],
			[u""],
			[u""],
			[u""],
			[u""],
		],
		[
			[u""],
			[u""],
			[u""], # einzelnes Beispiel
			[u""], # Daten-Konsistenz
			[u""],
			[u""],
			[u""],
		],
		[ ##### Ereignisbehandlung, Graphsuche, Zustandsautomat
		# Idiome, mehrfache Verwendung von z.B. if
		# Eigenschaften und Nuancen
			[u"for (key,val) in enumerate(list):"],
			[u"\t",[("mark","draw anchor",1,"a1"),[]],u"if val == \"string\":"],
			[u"\t\tlist[key] += [(123,)]",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,1,.5),"stroke": (1,1,.75)}]],
			[u""],
			[u"a + b * c + d"],
			[u""],
			[u""],
			[u""],
			[u"for (key,val) in enumerate(list):"],
			[u"\tif ",[("mark","card static",0),[],{"mode": "paragraph","content": u"Dies ist ein Fließtext. Dies ist ein Fließtext. Dies ist ein Fließtext."}],u"val ",[("mark","card static",0),[],{"mode": "typewriter","content": [
				[u"for (key,val) in enumerate(list):"],
				[u"\tif val == \"string\":"],
				[u"\t\tlist[key] += [(123,)]"],
			]}],u"== ",[("mark","draw anchor",1,"a1"),[]],u"\"string\"",[("mark","card static",0),[],{"mode": "paragraph","content": u"Dies ist auch ein Fließtext. Dies ist auch ein Fließtext. Dies ist auch ein Fließtext."}],u":"],
			[u"\t\tlist[key]",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,.5,0),"stroke": (1,0,0)}],u" += [(123,)]"],
			[u""],
			[u"a + b * c + d"],
			[u""],
			[u""],
			[u""],
			[u"\t\tctx.set_source_rgb(0,00,00.,.00,00.00)"],
			[u"\t\tctx.set_source_rgb(uu0,uu00,uu00.,uu.00,uu00.00)"],
			[u"\t\tctx.set_source_rgb(.,..,...,00..,..00,00..00)"],
			[u"\t\tctx.select_font_face(\"Monospace\")"],
			[u"\t\tctx.select_font_face(uu\"Monospace\")"],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],[("mark","text formatting",0),[],{"sc": (.4,.2,.2)}],u"\t\tfo = ctx.get_font_options22() keyword",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}]],
			[[("mark","text formatting",0),[],{"sc": (.1,.4,.4)}],u"\t\tfo = ctx.get_font_options22() call"],
			[[("mark","text formatting",0),[],{"sc": (.1,.25,.1)}],u"\t\tfo = ctx.get_font_options22() identifier"],
			[[("mark","text formatting",0),[],{"sc": (.5,.4,.5)}],u"\t\tfo = ctx.get_font_options22() literal"],
			[[("mark","text formatting",0),[],{"sc": (.1,.3,.6)}],u"\t\tfo = ctx.get_font_options22() comment"],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],[("mark","text formatting",0),[],{"sc": (.4,.2,.2)}],u"\t\t",[("mark","draw anchor",1,"a1"),[]],u"a11 fo = ctx.get_font_options22() keyword",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}]],
			[[("mark","text formatting",0),[],{"sc": (.1,.4,.4)}],u"\t\tfo = ctx.get_font_options22() call"],
			[[("mark","text formatting",0),[],{"sc": (.1,.25,.1)}],u"\t\tfo = ctx.get_font_options22() identifier",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,1,.5),"stroke": (1,1,.75)}]],
			[[("mark","text formatting",0),[],{"sc": (.5,.4,.5)}],u"\t\t",[("mark","draw anchor",1,"a1"),[]],u"fo = ctx.get_font_options22() literal"],
			[[("mark","text formatting",0),[],{"sc": (.1,.3,.6)}],u"\t\tfo = ctx.get_font_options22() comment a12",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,1,.5),"stroke": (1,1,.75)}]],
			[u""],
			[[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)}],[("mark","text formatting",0),[],{"sc": (.4,.2,.2)}],u"\t\t",[("mark","draw anchor",1,"a1"),[]],u"a21 fo = ctx.get_font_options22() keyword",[("mark","text formatting",0),[],{"sf": ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)}]],
			[[("mark","text formatting",0),[],{"sc": (.1,.4,.4)}],u"\t\tfo = ctx.get_font_options22() call"],
			[[("mark","text formatting",0),[],{"sc": (.1,.25,.1)}],u"\t\tfo = ctx.get_font_options22() identifier",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,.5,0),"stroke": (1,0,0)}],[("mark","text formatting",0),[],{"sc": (.5,.4,.5)}],u"",[("mark","draw anchor",1,"a1"),[]],u"fo = ctx.get_font_options22() literal"],
			[[("mark","text formatting",0),[],{"sc": (.1,.3,.6)}],u"\t\tfo = ctx.get_font_options22() comment a22",[("mark","draw anchor",-1,"a1"),[],{"type": "bounding box","fill": (1,.5,0),"stroke": (1,0,0)}]],
			[u""],
			[u""],
			[u""],
			[u"ctx.set_source_rgb(0,0,0)"],
			[u"ctx.select_font_face(\"Monospace\")"],
			[u"fo = ctx.get_font_options()"],
			[u"fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)"],
			[u"ctx.set_font_options(fo)đſðđł€ŋŧłł³¹¼¼½{¼¬”«"],
			[u"ctx.set_source_rgb(0,0,0)"],
			[u"ctx.select_font_face(\"Monospace\")"],
			[u"fo = ctx.get_font_options()"],
			[u"fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)"],
			[u"ctx.set_font_options(fo)đſðđł€ŋŧłł³¹¼¼½{¼¬”«"],
		],
	]
	
	lines[-2] = lines[-1] #TODO
	
	selects = [[[0,0,0],[0,0,0]] for i in xrange(11)]
	scrolls1 = [[0,-1,0] for i in xrange(11)]
	histories = [None] * 11 #TODO
	
	ctx1 = [None] * 11  #  TODO access only via parse_query
	
	try:
	#if True:
		#with open("tabs.pkl","rb") as f:
		if True:
			
			#print "loading..."
			
			pkl = cPickle.load(f)
			
			for n,filee in reversed(tuple(enumerate(files))):
				if "permanent" in filee[3] and filee[3]["permanent"]:
					pkl["files"] = [files[n]] + pkl["files"]
					pkl["titles"] = [titles[n]] + pkl["titles"]
					pkl["lines"] = [lines[n]] + pkl["lines"]
					pkl["selects"] = [selects[n]] + pkl["selects"]
					pkl["scrolls1"] = [scrolls1[n]] + pkl["scrolls1"]
					pkl["histories"] = [histories[n]] + pkl["histories"]
			
			ct = pkl["ct"]
			files = pkl["files"]
			titles = pkl["titles"]
			lines = pkl["lines"]
			selects = pkl["selects"]
			scrolls1 = pkl["scrolls1"]
			histories = pkl["histories"]
			ctx1 = [None] * len(titles)
			
			#print "loaded"
	except: pass
	
	def save_tabs():
		try:
		#if True:
			#with open("tabs.pkl","wb") as f:
			if True:
				
				#print "saving..."
				
				pkl = {}
				pkl["files"] = list(files)
				pkl["titles"] = list(titles)
				pkl["lines"] = list(lines)
				pkl["selects"] = list(selects)
				pkl["scrolls1"] = list(scrolls1)
				pkl["histories"] = list(histories)
				
				sel = lines[ct]
				
				i = 0
				for n,filee in enumerate(files):
					if "permanent" in filee[3] and filee[3]["permanent"]:
						
						del pkl["files"][n - i]
						del pkl["titles"][n - i]
						del pkl["lines"][n - i]
						del pkl["selects"][n - i]
						del pkl["scrolls1"][n - i]
						del pkl["histories"][n - i]
						
						if ct == n:
							pkl["ct"] = files_init.index(id(filee))
						
						i += 1
				
				for n,filee in enumerate(pkl["files"]):
					if pkl["lines"][n] is sel:
						pkl["ct"] = n + i
				
				cPickle.dump(pkl,f)
				
				#print "saved"
		
		except: pass
	
	closed_tabs = [] # TODO renamed
	closed_tabs_index = {}
	
	scroll1_resize = True
	ast_selected1 = [None,None] # lowest node containing selection
	ast_selected2 = [] # nodes in ast_selected1, overlapping selection
	
	
	ca = 13 # current accordion
	tools = [
		u"0. Dateien / Tabs",
		u"1. Abfrage",
		u"2. Umschreiben / Generieren",
		u"3. Merkmale",
		u"4. Konfigurieren",
		u"5. Richtlinien",
		u"6. Code-Schemata",
		u"7. Orthogonale Bereiche",
		u"8. Komplexitätsklassen",
		u"9. Datenfluss",
		u"10. Erläuterung des Programmcodes",
		u"11. Knoten-Eigenschaften",
		u"12. Über dieses Programm",
		u"X. Alle Reiter schließen",
	]
	
	if "advanced mode" in settings and settings["advanced mode"]:
		tools += [
			u"A. Interaktive Ausführung",
			u"A. Datei-Verlauf",
			u"A. Knoten-Navigator",
			u"A. Knoten-Liste",
		]
	
	tool_ctrs = [0] * 18
	scroll2 = [0,-1]
	scroll3 = 1.
	
	
	mousedown1 = None
	buttons1 = [[] for i in xrange(5)]
	buttons3_1 = []
	buttons3_2 = []
	
	rearrange1 = -1
	rearrange2 = -1
	rearrange_pos1 = [0,0]
	rearrange_pos2 = [0,0]
	
	menu_actions = None
	menu_pos1 = [0,0]
	menu_pos2 = [0,0]
	menu_mousedown = None
	
	anim_ctrs = [0,0,0,0,3]
	anim_tick = -1
	
	state = {}
	
	try:
		#with open("state.pkl","rb") as f:
		if True:
			pkl = cPickle.load(f)
			ca = pkl["ca"]
			tool_ctrs = pkl["tool_ctrs"]
			scroll2 = pkl["scroll2"]
			state = pkl["state"]
			#width = pkl["width"]
			#height = pkl["height"]
			closed_tabs = pkl["closed_tabs"]
	except: pass
	
	def save_state():
		try:
			#with open("state.pkl","wb") as f:
			if True:
				pkl = {}
				pkl["ca"] = ca
				pkl["tool_ctrs"] = tool_ctrs
				pkl["scroll2"] = scroll2
				pkl["state"] = state
				#pkl["width"] = width
				#pkl["height"] = height
				pkl["closed_tabs"] = closed_tabs
				cPickle.dump(pkl,f)
		except: pass
	
	tab_history = [] # store tab history permanently # TODO
	tab_history_index = [] # TODO
	
	
	
	
	
	
	def resize():
		global surf,surf_mask
		
		if ctx1[ct]\
		and not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
			ctx1[ct]["minimap arranged"] = False
		
		pygame.display.set_mode((width,height),pygame.DOUBLEBUF | pygame.RESIZABLE,32)
		
		data = pygame.surfarray.pixels2d(pygame.display.get_surface()).data
		surf = cairo.ImageSurface.create_for_data(data,cairo.FORMAT_RGB24,width,height)
		
		data = array.array("c",chr(0) * width * height * 4)
		surf_mask = cairo.ImageSurface.create_for_data(data,cairo.FORMAT_ARGB32,width,height,width * 4)
	
	
	
	def validate_editor1():
		global inds
		
		inds = [None] * len(lines[ct]) # indents
		for i,line in enumerate(lines[ct]):
			textt = "".join(get_text(line))
			match = re.search("(\\t*)",textt)
			ind = len(match.group(1))
			inds[i] = (textt[ind:],ind)
	
	
	
	def validate_editor2():
		global ec,er
		
		ec = max(1,(xs[3] - xs[2] - 2) // f3[2][0]) # editor cols
		er = 0
		for i,line in enumerate(lines[ct]):
			
			ind = inds[i]
			
			er += max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1 # editor rows
	
	
	
	def validate_editor3():
		global eh,caret_line,caret_row,cards_row
		
		caret_line = [None] * len(lines[ct])
		caret_row = [None] * er
		cards_row = [None] * er
		
		j = 0
		eh = 0
		for i,line in enumerate(lines[ct]):
			
			
			marks = get_marks(i,line)
			
			ind = inds[i]
			
			
			caret_line[i] = [j,ind[1],eh,0] # starting at row, indent, y, total height of rows + cards
			
			for n in xrange(max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1):
				
				
				caret_row[j] = [i,ind[1],eh,0,n] # line, indent, y, total height of row + cards, row of line
				cards_row[j] = [0,[]] # total height of cards without row, cards
				
				
				h = 0
				markss = [[list(mark)] for mark in marks if max(1,mark[0]) > n * ec and mark[0] <= (n + 1) * ec]
				for mark in markss:
					
					mark[0][0] -= n * ec
					
					if mark[0][1]\
					and mark[0][1][0][1] == "card static":
						
						pos = (xs[2] + f3[2][0] * mark[0][0],0)
						
						if not h: h += iys[0]
						
						c1 = (mark[0][1][2],get_card_size(mark[0][1][2]),pos)
						cards_row[j][1] += [c1]
						
						h += c1[1][3]
						h += iys[0]
				
				
				eh += h
				eh += f3[2][1]
				
				caret_row[j][3] = eh - caret_row[j][2]
				cards_row[j][0] = h
				
				j += 1
			
			caret_line[i][3] = eh - caret_line[i][2]
	
	
	
	def validate_minimap():
		global minimap,mc
		
		minimap = []
		mc = 16 # minimap cols
		
		if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
			
			def traverse_recur(ast,minimap,depth = 0):
				global mc
				
				mc = min(32,max(mc,depth + 1))
				
				for x in ast:
					if isinstance(x,list):
						
						
						typee = None
						if x[0][:3] in set(("statement","expression",statement) for statement in ("(","await","for","if"))\
						or x[0][:3] in set(("statement","control",statement) for statement in ("assert","break","case","continue","for","goto","if","raise","return","switch","throw","try","while","yield"))\
						or x[0][:4] == ("statement","expression","op","new"):
							typee = "control"
						elif x[0][:3] in set(("statement","expression",statement) for statement in ("[",".","op"))\
						or x[0][:3] == ("statement","control","del")\
						or x[0][:1] == ("identifier",):
							typee = "data"
						elif x[0][:1] in {("literal",),("brackets",),("statement",)}:
							typee = "structure"
						
						if typee:
							minimap += [[depth,0,x,typee]]
							len_prev = len(minimap)
							
							traverse_recur(x[1],minimap,depth + 1)
							
							if x[0][:1] == ("brackets",)\
							and len_prev == len(minimap):
								del minimap[-1]
			
			
			traverse_recur(ctx1[ct]["ast"],minimap)
	
	
	
	# "text formatting"  state
	# "text decoration"  state
	
	# "draw inline"      pos, extent, nocaret
	# "draw spacing"     pos, extent, nocaret
	# "draw anchor"      anchors, nocaret
	
	# "card active"      range, nested, extern
	# "card static"      pos, extern
	
	# "readonly"         range, nested, readonly
	# "rewrite"          range, nested, readonly
	# "configure"        range, nested
	
	
	
	def get_flags(selectt):
		flags = set()
		
		for i,x in enumerate(line):
			if isinstance(x,list):
				if x[0][1] == "text formatting":
					flags |= {"state"}
				elif x[0][1] == "text decoration":
					flags |= {"state"}
				elif x[0][1] == "draw inline":
					flags |= {"pos","extent","nocaret"}
				elif x[0][1] == "draw spacing":
					flags |= {"pos","extent","nocaret"}
				elif x[0][1] == "draw anchor":
					flags |= {"anchors","nocaret"}
				elif x[0][1] == "card active":
					flags |= {"range","nested","extern"}
				elif x[0][1] == "card static":
					flags |= {"pos","extern"}
				elif x[0][1] == "readonly":
					flags |= {"range","nested","readonly"}
				elif x[0][1] == "rewrite":
					flags |= {"range","nested","readonly"}
				elif x[0][1] == "configure":
					flags |= {"range","nested"}
		
		return flags
	
	
	
	def get_text(line,rewrite = True,configure = False):
		textt = []
		
		depth_rewrite = 0
		depth_configure = 0
		
		for x in line:
			if isinstance(x,list):
				if x[0][1] == "text formatting":
					pass
				elif x[0][1] == "text decoration":
					pass
				elif x[0][1] == "draw inline":
					pass
				elif x[0][1] == "draw spacing":
					pass
				elif x[0][1] == "draw anchor":
					pass
				elif x[0][1] == "card active":
					pass
				elif x[0][1] == "card static":
					pass
				elif x[0][1] == "readonly":
					pass
				elif x[0][1] == "rewrite":
					depth_rewrite += x[0][2]
					
					if not rewrite\
					and x[0][2] > 0:
						textt += get_text(x[1],rewrite,configure)
				elif x[0][1] == "configure":
					depth_configure += x[0][2]
					
					if configure\
					and x[0][2] > 0:
						textt += get_text(x[1],rewrite,configure)
			else:
				if not rewrite\
				and depth_rewrite > 0:
					pass
				if configure\
				and depth_configure > 0:
					pass
				else:
					textt += [x]
		
		return textt
	
	
	
	def get_segment(line,off):
		end = (0,0)
		
		for i,x in enumerate(line):
			if isinstance(x,list):
				if x[0][1] == "text formatting":
					pass
				elif x[0][1] == "text decoration":
					pass
				elif x[0][1] == "draw inline":
					pass
				elif x[0][1] == "draw spacing":
					pass
				elif x[0][1] == "draw anchor":
					pass
				elif x[0][1] == "card active":
					pass
				elif x[0][1] == "card static":
					pass
				elif x[0][1] == "readonly":
					pass
				elif x[0][1] == "rewrite":
					pass
				elif x[0][1] == "configure":
					pass
			else:
				end = (i,len(x))
				
				if off < len(x):
					return (i,off)
				
				off -= len(x)
		
		return end
	
	
	
	def get_marks(i,line):
		marks = []
		
		ind = inds[i]
		
		i = min(ec,ind[1] * 8) - ind[1]
		for x in line:
			if isinstance(x,list):
				marks += [[i,x]]
				
				if x[0][1] == "text formatting":
					pass
				elif x[0][1] == "text decoration":
					pass
				elif x[0][1] == "draw inline":
					pass
				elif x[0][1] == "draw spacing":
					pass
				elif x[0][1] == "draw anchor":
					pass
				elif x[0][1] == "card active":
					pass
				elif x[0][1] == "card static":
					pass
				elif x[0][1] == "readonly":
					pass
				elif x[0][1] == "rewrite":
					pass
				elif x[0][1] == "configure":
					pass
			else:
				i += len(x)
		
		return marks
	
	
	
	blk = (0,0,0)
	wht = (1,1,1)
	gr1 = (.92,.92,.92)
	gr2 = (.92,.92,.92)
	gr3 = (.98,.98,.98)
	grn = (.4,.8,.4)
	org = (1,.5,0)
	red = (1,0,0)
	yl1 = (1,1,.5)
	yl2 = (1,1,.75)
	bl1 = (.1,.3,.6)
	bl2 = (.25,.5,1)
	bl3 = (.7,.8,1)
	bl4 = (.85,.9,.95)
	
	def ccn(n):
		return [(.8,0,.2),(.9,.6,0),(0,.6,0),(.25,.5,1)][n % 4]
	
	
	# font face, size, (char width, line height, char height), weight
	
	f1 = ("Droid Sans",13,(0,16,9),cairo.FONT_WEIGHT_NORMAL)
	f2 = ("Droid Sans",13,(0,16,9),cairo.FONT_WEIGHT_BOLD)
	f3 = ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_NORMAL)
	f4 = ("DejaVu Sans Mono",14,(8,17,13),cairo.FONT_WEIGHT_BOLD)
	
	def blank(n): # blank line, shortcut for (line height - char height) + n * line height
		return (n + 1) * f1[2][1] - f1[2][2]
	
	
	fmt = {
		"default": [blk,f3],
		#"keyword": [(.4,.2,.2),f4],
		#"call": [(.1,.4,.4),None],
		"identifier": [(.1,.25,.1),None],
		#"literal": [(.5,.4,.5),None],
		"comment": [(.1,.3,.6),None],
		
		"keyword": [blk,f4],
		"call": [(.09,.53,.62),None],
		"literal": [(.67,.48,.13),None],
		
		"structure": [(.85,.8,.72),None],
		#"control": [(1,0,0),None],
		#"data": [(.6,.8,.3),None],
		"control": [(.25,.5,1),None],
		"data": [(.7,.8,1),None],
	}
	
	
	
	measure = {}
	
	
	
	def layout(update = {0}): ## TODO caret_line,caret_row,inds inconsistency
		global scroll2,buttons1,buttons3_1,buttons3_2
		global txs,tys,ixs,iys,xs,ys
		
		
		measure["layout"] = pygame.time.Clock()
		
		measure["layout toplevel 1"] = pygame.time.Clock()
		
		
		ctx = cairo.Context(surf)
		ctx_mask = cairo.Context(surf_mask)
		
		
		
	
	
		global get_card_size
		
		def get_card_size(card):
			global disable_draw_prs
			
			
			#TODO move ( le -> l -> le )
			
			
			if card["mode"] == "paragraph":
				w1 = ixs[2] + 2 * ixs[1] # fixed size
			elif card["mode"] == "typewriter":
				#w1 = xs[3] - xs[2] - 2 * ixs[0] # maximum size
				w1 = ixs[2] + 2 * ixs[1] # fixed size
			
			
			disable_draw_prs = True
			(x1,y1,w1,h1) = layout_card(card,0,0,w1,0,0,0)
			disable_draw_prs = False
			
			
			x1 = xs[3] - ixs[0] - w1
			h1 = y1
			
			
			return (x1,0,w1,h1)
	
	
	
		def layout_card(card,x1,y1,w1,h1,x2,y2):
			
			
			#TODO clip
			
			
			frect(x1,x1 + w1,y1,y1 + h1 - 1,wht)
			srect(x1,x1 + w1,y1,y1 + h1 - 1,bl1)
			if x1 + w1 - 1 <= x2:
				frect(x1 + w1 - 1,x2 + 2,y1 + h1 - 3,y1 + h1 - 1,bl1)
			else:
				frect(x2,x1 + w1,y1 + h1 - 2,y1 + h1,bl1)
			frect(x2 + .5,x2 + 1.5,y1 + h1 - 1,y2 - 5,bl1)
			ftri(x2 + 1,y2 - 5,5,1./2 * math.pi,bl1)
			
			
			y = y1
			x = x1
			w = w1
			
			y += iys[1]
			x += ixs[1]
			w -= 2 * ixs[1]
			
			## TODO:
			if card["mode"] == "paragraph":
				y += 150
			elif card["mode"] == "typewriter":
				y += 50
			
			
			y += iys[1]
			y += 2
			
			return (x1,y,w1,h1)
	
	
	
		def layout_editor():
			global scroll1_resize,ast_selected1,ast_selected2,buttons1
			
			
			measure["layout_editor"] = pygame.time.Clock()
			
			
			# decorate lines
			
			
			if not ("no parse" in files[ct][3] and files[ct][3]["no parse"])\
			and not ("highlighted" in ctx1[ct] and ctx1[ct]["highlighted"]):
				ctx1[ct]["highlighted"] = True
				
				
				lines[ct] = copy.deepcopy(ctx1[ct]["lines"])
				
				
				def highlight(x,name):
					pos = ctx1[ct]["positions"][id(x)]
					seg1 = get_segment(lines[ct][pos[0][0]],pos[0][1])
					lines[ct][pos[0][0]][seg1[0]:seg1[0] + 1] = [
						lines[ct][pos[0][0]][seg1[0]][:seg1[1]],
						[("mark","draw anchor",1,name),[]], # type, tokens
						lines[ct][pos[0][0]][seg1[0]][seg1[1]:],
					]
					seg2 = get_segment(lines[ct][pos[1][0]],pos[1][1])
					lines[ct][pos[1][0]][seg2[0]:seg2[0] + 1] = [
						lines[ct][pos[1][0]][seg2[0]][:seg2[1]],
						[("mark","draw anchor",-1,name),[],{"type": "bounding box","fill": yl1,"stroke": yl2}], # type, tokens, props
						lines[ct][pos[1][0]][seg2[0]][seg2[1]:],
					]
				
				
				if u"Abfrage.choose" in state:
					if state[u"Abfrage.choose"][0]: # Division durch eine Variable
						statements = tuple(("statement","expression","op",statement,"binary") for statement in ("/","//","%",))
						ast = tuple(
							x for statement in statements\
							for x in parse_query(ctx1[ct]).type(statement).sel
						)
						for i,x in enumerate(ast):
							if ast_descendants(x[2]["x"][1:],("identifier",),ctx1[ct]):
								highlight(x,"%s-%d-%d" % (u"Abfrage.choose",0,i))
					
					if state[u"Abfrage.choose"][1]: # Datenzugriff mit einer Variablen als Schlüssel
						statements = tuple(("statement","expression",statement) for statement in ("[",))
						ast = tuple(
							x for statement in statements\
							for x in parse_query(ctx1[ct]).type(statement).sel
						)
						for i,x in enumerate(ast):
							if ast_descendants(x[2]["x"][1:],("identifier",),ctx1[ct]):
								highlight(x,"%s-%d-%d" % (u"Abfrage.choose",1,i))
					
					if state[u"Abfrage.choose"][2]: # Vergleich mit einem Literal
						
						statements = tuple(("statement","expression","op",statement,"binary") for statement in ("in","not in","<","<=",">",">=","!=","==",))
						
						q = parse_query(ctx1[ct])
						q.sel = list(
							x for statement in statements\
							for x in q.type(statement).sel
						)
						q = q.unique()
						
						def f1(arg1):
							
							x = arg1["x"]
							q1 = arg1["s"].nodes([x])
							
							if q1.isNodePropertyOf(q1.parent().type(("statement","control","for")),("x",)).count():
								return False
							
							q1 = q1.descendants(include_original_selection = True)
							q1 = q1.setMinus(q.init().type(("statement","expression","[")).descendants(include_original_selection = True))
							q1 = q1.setMinus(q.init().type(("statement","expression","(")).descendants(include_original_selection = True))
							q1 = q1.type(("literal",))
							
							return q1.count()
						
						q = q.filter(f1)
						
						for i,x in enumerate(q.sel):
							highlight(x,"%s-%d-%d" % (u"Abfrage.choose",2,i))
					
					if state[u"Abfrage.choose"][3]: # Funktionsaufruf innerhalb einer Schleife
						statements = tuple(("statement","expression",statement) for statement in ("(",))
						ast = tuple(
							x for statement in statements\
							for x in parse_query(ctx1[ct]).type(statement).sel
						)
						for i,x in enumerate(ast):
							ancestor = [None,x]
							while ancestor:
								ancestor = ast_ancestor(ancestor[-1],(("statement","control","for"),("statement","control","while")),ctx1[ct])
								
								if ancestor\
								and (
									ancestor[-1][0][:3] == ("statement","control","while")\
									or id(ancestor[-2]) in (id(z) for z in ancestor[-1][2]["do"])\
								):
									highlight(x,"%s-%d-%d" % (u"Abfrage.choose",3,i))
									break
				
				for i in xrange(3):
					if u"Richtlinien.choose" in state\
					and state[u"Richtlinien.choose"][i]:
						pass
				
				for i in xrange(3):
					if u"Schemata.choose" in state\
					and state[u"Schemata.choose"][i]:
						pass
				
				for i in xrange(2):
					if u"Orthogonale Bereiche.placeholder %d" % i in state\
					and state[u"Orthogonale Bereiche.placeholder %d" % i]:
						pass
					else:
						pass
				
				if u"Komplexitätsklassen.show cards" in state\
				and state[u"Komplexitätsklassen.show cards"]:
					pass
				
				if u"Datenfluss.show cards" in state\
				and state[u"Datenfluss.show cards"]:
					pass
				
				if u"Erläuterung.show cards" in state\
				and state[u"Erläuterung.show cards"]:
					pass
			
			
			# find nodes in selection
			
			
			ast_selected1[0] = None
			ast_selected2 = []
			
			
			# TODO request for accordion
			
			
			if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
				
				
				select = selects[ct]
				
				select_i1 = 0
				if select[0][0] > select[1][0]\
				or (\
					select[0][0] == select[1][0]\
					and select[0][1] > select[1][1]\
				):
					select_i1 = 1
				select_i2 = (select_i1 + 1) % 2
				
				
				for x in ctx1[ct]["nodes"]:
					pos = ctx1[ct]["positions"][id(x)]
					if pos[1][0] < select[select_i2][0]: # end < end
						pass
					elif pos[1][0] == select[select_i2][0]\
					and pos[1][1] < select[select_i2][1]:
						pass
					elif pos[0][0] > select[select_i1][0]: # start > start
						break
					elif pos[0][0] == select[select_i1][0]\
					and pos[0][1] > select[select_i1][1]:
						break
					elif x[0][0] not in {"comment","whitespace","keyword"}:
						ast_selected1[0] = x
				
				for x in ast_selected1[0][1] if ast_selected1[0] else ctx1[ct]["ast"]:
					if not isinstance(x,list):
						continue
					
					pos = ctx1[ct]["positions"][id(x)]
					if pos[1][0] < select[select_i1][0]: # end <= start
						pass
					elif pos[1][0] == select[select_i1][0]\
					and pos[1][1] <= select[select_i1][1]:
						pass
					elif pos[0][0] > select[select_i2][0]: # start >= end
						break
					elif pos[0][0] == select[select_i2][0]\
					and pos[0][1] >= select[select_i2][1]:
						break
					elif x[0][0] not in {"comment","whitespace","keyword"}:
						ast_selected2 += [x]
				
				if not ast_selected1[1]:
					ast_selected1[1] = ast_selected1[0]
			
			
			#print ast_selected1[0] if ast_selected1 else None
			#print [x[0] for x in ast_selected2]
			
			
			# layout editor
			
			
			clip(xs[2],xs[3],ys[5],ys[6])
			
			select = selects[ct]
			
			select_i1 = 0
			if select[0][0] > select[1][0]\
			or (\
				select[0][0] == select[1][0]\
				and select[0][1] > select[1][1]\
			):
				select_i1 = 1
			select_i2 = (select_i1 + 1) % 2
			
			select_s = False
			
			
			scrolls1[ct][0] = min(eh - f3[2][1],scrolls1[ct][0])
			scrolls1[ct][0] = max(0,scrolls1[ct][0])
			
			if not scroll1_resize:
				scrolls1[ct][1] = -1
			
			if not scroll1_resize\
			and scrolls1[ct][1] < 0:
				for i,line in enumerate(lines[ct]):
					cl = caret_line[i]
					
					if cl[2] >= scrolls1[ct][0]:
						scrolls1[ct][1] = i
						scrolls1[ct][2] = cl[2] - scrolls1[ct][0]
						break
			
			if not scroll1_resize\
			and scrolls1[ct][1] < 0:
				scrolls1[ct][1] = len(lines[ct]) - 1
				scrolls1[ct][2] = 0
			
			if scroll1_resize\
			and scrolls1[ct][1] >= 0:
				i = scrolls1[ct][1]
				cl = caret_line[i]
				
				scrolls1[ct][0] = cl[2] - scrolls1[ct][2]
			
			scroll1_resize = False
			
			if scroll_to_caret:
				cl = caret_line[select[1][0]]
				cr = cl[0] + (select[1][1] + min(ec,cl[1] * 8) - cl[1]) // ec
				cr = caret_row[cr]
				scrolls1[ct][0] = max(cr[2] + cr[3] + ys[5] - ys[6],scrolls1[ct][0])
				
				cl = caret_line[select[1][0]]
				cr = cl[0] + max(0,select[1][1] + min(ec,cl[1] * 8) - cl[1] - 1) // ec
				cs = cards_row[cr]
				cr = caret_row[cr]
				scrolls1[ct][0] = min(cr[2] + cs[0],scrolls1[ct][0])
			
			
			# draw decorations
			
			
			measure["draw decorations"] = pygame.time.Clock()
			
			
			sa = {}
			
			draw_sorted = []
			sort_index = 0
			
			clipping = 0
			for i,line in enumerate(lines[ct]):
				
				cl = caret_line[i]
				y = ys[5] - scrolls1[ct][0] + cl[2]
				
				if clipping == 0\
				and y + cl[3] >= ys[5]:
					clipping = 1
				
				if y >= ys[6]\
				and not sa:
					clipping = 2
				
				
				textt = "".join(get_text(line))
				marks = get_marks(i,line)
				
				ind = inds[i]
				
				for n in xrange(max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1):
					
					cs = cards_row[cl[0] + n]
					cr = caret_row[cl[0] + n]
					y = ys[5] - scrolls1[ct][0] + cr[2] + cs[0]
					
					
					# bounding box
					
					
					if not n:
						for j in sa:
							pos = (xs[2] + f3[2][0] * min(ec,ind[1] * 8),)
							sa[j][0] = min(pos[0],sa[j][0])
							sa[j][1] = max(pos[0],sa[j][1])
					else:
						for j in sa:
							sa[j][5] = True
					
					
					# iterate segments
					
					
					markss = [[list(mark)] for mark in marks if max(1,mark[0]) > n * ec and mark[0] <= (n + 1) * ec]
					for mark in markss:
						
						mark[0][0] -= n * ec
						
						if mark[0][1]\
						and mark[0][1][0][1] == "draw anchor":
							
							j = mark[0][1][0][3]
							pos = (xs[2] + f3[2][0] * mark[0][0],y)
							if mark[0][1][0][2] == 1\
							and y < ys[6]:
								sa[j] = [pos[0],pos[0],pos[1],pos[1],[pos],False,None,sort_index]
								sort_index += 1
							
							if j in sa:
								if mark[0][1][0][2] == 2\
								or mark[0][1][0][2] == -1:
									sa[j][0] = min(pos[0],sa[j][0])
									sa[j][1] = max(pos[0],sa[j][1])
									sa[j][3] = pos[1]
									sa[j][4] += [pos]
								if mark[0][1][0][2] == -1:
									
									if clipping == 1:
										sa[j][6] = mark[0][1][2]
										draw_sorted += [sa[j]]
									
									del sa[j]
					
					
					# bounding box
					
					
					if n == max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1 - 1:
						for j in sa:
							texttt = (u" " * min(ec,ind[1] * 8) + ind[0])[n * ec:(n + 1) * ec]
							pos = (xs[2] + f3[2][0] * len(texttt),)
							sa[j][0] = min(pos[0],sa[j][0])
							sa[j][1] = max(pos[0],sa[j][1])
					else:
						for j in sa:
							sa[j][5] = True
			
			draw_sorted.sort(key = lambda x: x[7])
			for sa_j in draw_sorted:
				if sa_j[5]:
					rect1 = (sa_j[4][0][0],xs[3],sa_j[2],sa_j[2] + f3[2][1])
					rect2 = (xs[2],xs[3],sa_j[2] + f3[2][1],sa_j[3])
					rect3 = (xs[2],sa_j[4][-1][0],sa_j[3],sa_j[3] + f3[2][1])
				else:
					rect1 = (sa_j[4][0][0],sa_j[1],sa_j[2],sa_j[2] + f3[2][1])
					rect2 = (sa_j[0],sa_j[1],sa_j[2] + f3[2][1],sa_j[3])
					rect3 = (sa_j[0],sa_j[4][-1][0],sa_j[3],sa_j[3] + f3[2][1])
				
				if "stroke" in sa_j[6]:
					srect(*(rect1 + (sa_j[6]["stroke"],)),lw = 5)
					srect(*(rect2 + (sa_j[6]["stroke"],)),lw = 5 if rect2[2] < rect2[3] else 3)
					srect(*(rect3 + (sa_j[6]["stroke"],)),lw = 5)
				
				if "fill" in sa_j[6]:
					frect(*(rect1 + (sa_j[6]["fill"],)))
					frect(*(rect2 + (sa_j[6]["fill"],)))
					frect(*(rect3 + (sa_j[6]["fill"],)))
			
			
			measure["draw decorations"] = measure["draw decorations"].tick()
			
			
			# draw text content
			
			
			measure["draw text content"] = pygame.time.Clock()
			
			
			sc = [blk]
			sf = [f3]
			
			clipping = 0
			for i,line in enumerate(lines[ct]):
				
				cl = caret_line[i]
				y = ys[5] - scrolls1[ct][0] + cl[2]
				
				if clipping == 0\
				and y + cl[3] >= ys[5]:
					clipping = 1
				
				if y >= ys[6]:
					break
				
				
				textt = "".join(get_text(line))
				marks = get_marks(i,line)
				
				ind = inds[i]
				
				select_c1 = -1
				if i == select[select_i1][0]:
					select_c1 = select[select_i1][1] + min(ec,ind[1] * 8) - ind[1]
				
				select_c2 = -1
				if i == select[select_i2][0]:
					select_c2 = select[select_i2][1] + min(ec,ind[1] * 8) - ind[1]
				
				for n in xrange(max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1):
					
					cs = cards_row[cl[0] + n]
					cr = caret_row[cl[0] + n]
					y = ys[5] - scrolls1[ct][0] + cr[2] + cs[0]
					
					if clipping == 1:
						
						
						# draw selection bg
						
						
						if select_c1 != select_c2 or select_s:
							if select_c1 >= 0 and select_c1 <= ec\
							and select_c2 >= 0 and select_c2 <= ec:
								frect(xs[2] + f3[2][0] * select_c1,xs[3] if select_c2 == ec else xs[2] + f3[2][0] * select_c2,y,y + f3[2][1],bl1)
							elif select_c1 >= 0 and select_c1 <= ec:
								frect(xs[2] + f3[2][0] * select_c1,xs[3],y,y + f3[2][1],bl1)
							elif select_c2 >= 0 and select_c2 <= ec:
								frect(xs[2],xs[3] if select_c2 == ec else xs[2] + f3[2][0] * select_c2,y,y + f3[2][1],bl1)
							elif select_s:
								frect(xs[2],xs[3],y,y + f3[2][1],bl1)
						
						
						# draw caret bg
						
						
						if select_c1 >= 0 and select_c1 <= ec:
							frect(xs[2] + f3[2][0] * select_c1,xs[2] + f3[2][0] * select_c1 + 2,y,y + f3[2][1],bl1)
						if select_c2 >= 0 and select_c2 <= ec:
							frect(xs[2] + f3[2][0] * select_c2,xs[2] + f3[2][0] * select_c2 + 2,y,y + f3[2][1],bl1)
					
					
					# iterate segments
					
					
					markss = [[max(min(ec,ind[1] * 8),n * ec),None]]
					markss += [mark for mark in marks if max(1,mark[0]) > n * ec and mark[0] <= (n + 1) * ec]
					markss += [[(n + 1) * ec,None]]
					markss = [[list(markss[j]),list(markss[j + 1])] for j in xrange(len(markss) - 1)]
					for mark in markss:
						
						if mark[0][1]\
						and mark[0][1][0][1] == "text formatting":
							
							if mark[0][1][0][2] == 1:
								sc += [sc[-1]]
								sf += [sf[-1]]
							elif mark[0][1][0][2] == -1\
							and 1 < len(sc)\
							and 1 < len(sf):
								del sc[-1]
								del sf[-1]
							
							if 2 < len(mark[0][1])\
							and "sc" in mark[0][1][2]:
								sc[-1] = mark[0][1][2]["sc"]
							if 2 < len(mark[0][1])\
							and "sf" in mark[0][1][2]:
								sf[-1] = mark[0][1][2]["sf"]
						
						if mark[0][0] != mark[1][0]:
							
							
							# draw text segments
							
							
							texttt = (u" " * min(ec,ind[1] * 8) + ind[0])[n * ec:(n + 1) * ec]
							
							mark[0][0] -= n * ec
							mark[1][0] -= n * ec
							mark[1][0] = min(len(texttt),mark[1][0])
							
							if clipping == 1:
								if select_c1 >= mark[0][0] and select_c1 <= mark[1][0]\
								and select_c2 >= mark[0][0] and select_c2 <= mark[1][0]:
									text(xs[2] + f3[2][0] * mark[0][0],y,sc[-1],sf[-1],texttt[mark[0][0]:select_c1])
									text(xs[2] + f3[2][0] * select_c1,y,wht,sf[-1],texttt[select_c1:select_c2])
									text(xs[2] + f3[2][0] * select_c2,y,sc[-1],sf[-1],texttt[select_c2:mark[1][0]])
								elif select_c1 >= mark[0][0] and select_c1 <= mark[1][0]:
									text(xs[2] + f3[2][0] * mark[0][0],y,sc[-1],sf[-1],texttt[mark[0][0]:select_c1])
									text(xs[2] + f3[2][0] * select_c1,y,wht,sf[-1],texttt[select_c1:mark[1][0]])
								elif select_c2 >= mark[0][0] and select_c2 <= mark[1][0]:
									text(xs[2] + f3[2][0] * mark[0][0],y,wht,sf[-1],texttt[mark[0][0]:select_c2])
									text(xs[2] + f3[2][0] * select_c2,y,sc[-1],sf[-1],texttt[select_c2:mark[1][0]])
								elif select_s:
									text(xs[2] + f3[2][0] * mark[0][0],y,wht,sf[-1],texttt[mark[0][0]:mark[1][0]])
								else:
									text(xs[2] + f3[2][0] * mark[0][0],y,sc[-1],sf[-1],texttt[mark[0][0]:mark[1][0]])
							
							
							# update select state
							
							
							if select_c1 >= mark[0][0] and select_c1 <= mark[1][0]\
							and select_c2 >= mark[0][0] and select_c2 <= mark[1][0]:
								select_s = False
							elif select_c1 >= mark[0][0] and select_c1 <= mark[1][0]:
								select_s = True
							elif select_c2 >= mark[0][0] and select_c2 <= mark[1][0]:
								select_s = False
					
					
					select_c1 -= ec
					select_c2 -= ec
			
			
			measure["draw text content"] = measure["draw text content"].tick()
			
			
			# draw cards
			
			
			clipping = 0
			for i,line in enumerate(lines[ct]):
				
				cl = caret_line[i]
				y = ys[5] - scrolls1[ct][0] + cl[2]
				
				if clipping == 0\
				and y + cl[3] >= ys[5]:
					clipping = 1
				
				if y >= ys[6]:
					break
				
				if clipping != 1:
					continue
				
				
				ind = inds[i]
				
				for n in xrange(max(0,len(ind[0]) + min(ec,ind[1] * 8) - 1) // ec + 1):
					
					cs = cards_row[cl[0] + n]
					cr = caret_row[cl[0] + n]
					y1 = ys[5] - scrolls1[ct][0] + cr[2]
					y2 = y1 + cs[0]
					
					
					# iterate cards
					
					
					for i,c in enumerate(cs[1]):
						
						if not i: y1 += iys[0]
						
						layout_card(c[0],c[1][0],y1,c[1][2],c[1][3],c[2][0],y2)
						
						y1 += c[1][3]
						y1 += iys[0]
			
			
			measure["layout_editor"] = measure["layout_editor"].tick()
		
		
		
		def layout_editor_sides():
			global buttons1
			
			
			measure["layout_editor_sides"] = pygame.time.Clock()
			
			
			clip(xs[1],xs[4],ys[5],ys[6])
			
			select = selects[ct]
			
			frect(xs[1],xs[2] - 2,ys[5],ys[6],gr1)
			
			for i,line in enumerate(lines[ct]):
				
				cl = caret_line[i]
				y = ys[5] - scrolls1[ct][0] + cl[2]
				
				if y >= ys[6]:
					break
				
				ind = inds[i]
				
				if y + cl[3] >= ys[5]:
					text(xs[1],y,blk,f4 if i == select[1][0] else f3,unicode(i + 1).rjust(lc + 1))
			
			
			frect(xs[3] + 2,xs[4],ys[5],ys[6],gr3)
			
			
			h_screen = ys[6] - ys[5]
			h_doc = eh
			h_bar = max(5,int(round(min(1,float(h_screen) / h_doc) * h_screen)))
			h_sp_doc = max(1,h_doc - h_screen)
			h_sp_bar = max(0,h_screen - h_bar)
			
			y_bar = int(round(min(1,float(scrolls1[ct][0]) / h_sp_doc) * h_sp_bar))
			y_mid = min(1,float(scrolls1[ct][0]) / h_sp_doc) * h_screen
			
			frect(xs[3] + 2,xs[4] + 1,ys[5] + y_bar - 1,ys[5] + y_bar + h_bar + 1,gr1)
			
			
			if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
				
				
				minimap_lines = set(id(line[2]) for line in minimap)
				
				ast_selected = ast_selected2 or [ast_selected1[1]]
				ast_selected_ids = set()
				for x in ast_selected:
					ancestors = ast_ancestor(x,(),ctx1[ct])
					ancestors = (z for z in ancestors if id(z) in minimap_lines)
					try: ast_selected_ids |= {id(next(ancestors))}
					except: pass
				
				
				if not ("minimap arranged" in ctx1[ct] and ctx1[ct]["minimap arranged"]):
					ctx1[ct]["minimap arranged"] = True
					
					
					y = 0
					for i,line in enumerate(minimap):
						pos = ctx1[ct]["positions"][id(line[2])]
						cl = caret_line[pos[0][0]]
						cr = cl[0] + (pos[0][1] + min(ec,cl[1] * 8) - cl[1]) // ec
						cs = cards_row[cr]
						cr = caret_row[cr]
						pos = cr[2] + cs[0] + f3[2][1] // 2
						pos = float(pos) / h_doc * h_screen
						
						y = max(pos,y)
						y = min(h_screen,y)
						
						line[1] = y
					
					
					def arrange(j1,j2):
						if j1 >= j2: return
						
						indent = minimap[j1][0] + 1 if j1 >= 0 else 0
						y1 = minimap[j1][1] if j1 >= 0 else 0
						y2 = minimap[j2][1] if j2 < len(minimap) else h_screen
						children = list(j1 + 1 + i for i,line in enumerate(minimap[j1 + 1:j2]) if line[0] == indent)
						extended = [j1] + children + [j2]
						#mindist = min(
						#	(minimap[extended[i + 1]][1] if extended[i + 1] < len(minimap) else h_screen) -\
						#	(minimap[extended[i]][1] if extended[i] >= 0 else 0)\
						#	for i in xrange(len(extended) - 1)
						#)
						
						#if (y2 - y1) <= h_bar\
						#and (float(mindist) * 4 < h_bar or not h_sp_bar): # distribute equally
						if (y2 - y1) <= h_bar: # distribute equally
							
							n = max(1,(j2 - j1) - sum(1 for i in xrange(j1 + 1,j2) if i and minimap[i - 1][0] < minimap[i][0]))
							h = max(0,float(y2 - y1) / n)
							k = 0
							for i in xrange(j1 + 1,j2):
								if not (i and minimap[i - 1][0] < minimap[i][0]):
									k += 1
								minimap[i][1] = y1 + h * k
						
						else: # descend tree
							
							for i in xrange(1,len(extended) - 1):
								arrange(extended[i],extended[i + 1])
					
					
					arrange(-1,len(minimap))
				
				
				n = max(1,len(minimap) - sum(1 for i in xrange(len(minimap) - 1) if minimap[i][0] < minimap[i + 1][0]))
				alpha = min(1,float(h_screen) / n * scroll3)
				
				
				x = xs[3] + 8
				for i in xrange(mc + 1):
					frect(x,x + 1,ys[5],ys[6],wht)
					x += 6
				
				
				indent = 0
				y = ys[5] + minimap[0][1] if minimap else 0
				y = (y - ys[5] - y_mid) * scroll3 + ys[5] + y_mid
				y_start = {}
				y_line = {0:[None,None,None,"structure"]}
				y_end = {}
				selected_y1 = ys[6]
				selected_y2 = ys[5]
				selected_indent = -1
				for i,line in enumerate(minimap):
					
					x_prev = xs[3] + 8
					
					if line[0] not in y_start:
						indent = line[0]
						y_start[line[0]] = y
					else:
						y = ys[5] + line[1]
						y = (y - ys[5] - y_mid) * scroll3 + ys[5] + y_mid
					
					y_end[line[0]] = y
					
					if selected_y2 == ys[6]\
					and line[0] <= selected_indent:
						selected_y2 = y
					
					
					if id(line[2]) in ast_selected_ids:
						if selected_y1 == ys[6]:
							selected_y1 = y
						selected_y2 = ys[6]
						selected_indent = line[0]
					
					x = x_prev + line[0] * 6
					bold = 1 if selected_y1 <= y and y < selected_y2 else 0
					if alpha < .5 and bold:
						frect(x + 1,x + 7,int(round(y)),int(round(y)) + 1,fmt[line[3]][0],alpha * 2)
					else:
						frect(x + 1,x + 7 + bold,int(round(y)),int(round(y)) + 1 + bold,fmt[line[3]][0],alpha)
					
					
					if i == len(minimap) - 1:
						line = list(line)
						line[0] = -1
					
					if line[0] < indent:
						#print line[0], indent, y_start, y_end
						for j in xrange(line[0] + 1,indent + 1):
							
							if j in y_start\
							and j in y_line\
							and y_start[j] != y_end[j]:
								x = x_prev + j * 6
								bold1 = 1 if selected_y1 <= y_start[j] and y_end[j] < selected_y2 else 0
								bold2 = bold1 if alpha >= .5 else 0
								frect(x,x + 1 + bold1,int(round(y_start[j])),int(round(y_end[j])) + 1 + bold2,fmt[y_line[j][3]][0])
							
							if j in y_start: del y_start[j]
						
						indent = line[0]
					
					y_line[line[0] + 1] = line
			
			
			srect(xs[3] + 2,xs[4] + 1,ys[5] + y_bar - 1,ys[5] + y_bar + h_bar + 1,bl1)
			
			
			buttons1[3] += [((xs[1],xs[3],ys[5],ys[6]),("editor",))]
			buttons1[3] += [((xs[3],xs[4],ys[5],ys[6]),("minimap",))]
			
			
			measure["layout_editor_sides"] = measure["layout_editor_sides"].tick()
		
	
	
		def get_accordion_height(n,current):
			global disable_draw_prs
			
			disable_draw_prs = True
			y = layout_accordion(n,current,0)
			disable_draw_prs = False
			
			return y
	
	
	
		def layout_accordion(n,current,y):
			global state
			
			x = xs[7] + axs[0]
			w = xs[8] - xs[7] - 2 * axs[0]
			
			def action_parse(arg):
				global request_parse,request_layout
				
				request_parse = True
				
				request_layout |= {0}
			
			def action_layout(arg):
				global request_layout
				
				if ctx1[ct]\
				and not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
					ctx1[ct]["highlighted"] = False
				
				request_layout |= {0}
			
			if n == 0: # Dateien / Tabs
				if current:
					y += ays[0]
					
					y += text_paragraph((x,y,w),blk,f1,u"Titel: %s" % titles[ct])
					
					y += blank(1)
					
					y += text_paragraph((x,y,w),blk,f1,u"Dateipfad: keiner")
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					
					def cont1((x,y,w),i,j):
						y_prev = y
						
						if j: y += ays[0] * 2
						
						# Tab speichern unter -> Tab öffnen + sha256 lookup
						# Tab zurücksetzen -> Änderung automatisch erkennen + Rückfrage
						
						if j == 0 and i == 0:
							y += button((x,y,w),u"Neuer Tab",actionNew)
						elif j == 0 and i == 1:
							y += button((x,y,w),u"Tab öffnen",actionOpen)
						elif j == 1 and i == 0:
							y += button((x,y,w),u"Tab speichern",actionSave)
						elif j == 1 and i == 1:
							y += button((x,y,w),u"Alle Tabs speichern",actionSaveAll)
						elif j == 2 and i == 0:
							y += button((x,y,w),u"Tab schließen",actionClose)
						elif j == 2 and i == 1:
							y += button((x,y,w),u"Tab wiederherstellen",actionRestore)
						elif j == 3 and i == 0:
							y += button((x,y,w),u"Titel festlegen",actionTitle)
						elif j == 3 and i == 1:
							y += button((x,y,w),u"Einstellungen",actionSettings)
						
						return y - y_prev
					
					
					y += table((x,y,w),(-1,-1),((cont1,) * 2,) * 4)
					
					y += ays[0]
				else:
					y += text_paragraph((x,y,w),blk,f1,u"Dateipfad: keiner")
			
			elif n == 1: # Abfrage
				if current:
					y += ays[0]
					
					labels = (
						u"Division durch eine Variable",
						u"Datenzugriff mit einer Variablen als Schlüssel",
						u"Vergleich mit einem Literal",
						u"Funktionsaufruf innerhalb einer Schleife",
					)
					
					name = u"Abfrage.choose"
					if name not in state: state[name] = {}
					
					for i,label in enumerate(labels):
						if i: y += blank(1)
						
						if i not in state[name]: state[name][i] = 0
						y += choose((x,y,w),name,i,label,False,action = action_layout)
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					y += text_paragraph((x,y,w),blk,f1,u"Tabelle mit Ergebnissen")
					
					y += ays[0]
				else:
					y += text_paragraph((x,y,w),gr1,f1,u"Keine Abfrage ausgewählt")
			
			elif n == 2: # Umschreiben / Generieren
				if current:
					y += ays[0]
					
					name = u"Umschreiben.active"
					if name not in state: state[name] = 1
					y += switch((x,y,w),name,u"Umschreiben aktivieren",action = action_parse)
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					labels = (
						u"Spezielle Formatierung anwenden",
						u"Fehlertoleranz für bestimmte Konstrukte einfügen",
						u"Domänenspezifische Befehle umsetzen",
					)
					
					name = u"Umschreiben.choose"
					if name not in state: state[name] = {}
					
					for i,label in enumerate(labels):
						if i: y += blank(1)
						
						if i not in state[name]: state[name][i] = 0
						y += choose((x,y,w),name,i,label,action = action_parse,enabled = state[u"Umschreiben.active"])
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					y += text_paragraph((x,y,w),blk,f1,u"Tabellen mit umgeschriebenen Stellen")
					
					y += ays[0]
				else:
					y += text_paragraph((x,y,w),gr1,f1,u"Umschreiben deaktiviert")
			
			elif n == 3: # Merkmale
				if current:
					y += ays[0]
					
					features = [None] * 3
					
					for i in xrange(len(features)):
						features[i] = u"Merkmal %d %s" % (i + 1,u" Extra" * i) #TODO
					
					occurrences = (
						[[0,0,1,],None],
						[[1,0,1,],None],
						[[1,1,0,],None],
					)
					
					for j,occurrence in enumerate(occurrences):
						occurrence[1] = u"Vorkommen %d %s" % (j + 1,u" Extra" * j) #TODO
					
					y += feature_table((x,y,w),features,occurrences)
					
					y += ays[0]
				else:
					
					
					def cont1((x,y,w),i,j):
						y_prev = y
						
						if j: y += blank(0)
						
						if j == 0 and i == 0:
							y += text_paragraph((x,y,w),blk,f1,u"Schleifen:")
						elif j == 0 and i == 1:
							y += text_paragraph((x,y,w),blk,f1,u"Arithmetik:")
						elif j == 0 and i == 2:
							y += text_paragraph((x,y,w),blk,f1,u"Datenzugriffe:")
						elif j == 1 and i == 0:
							y += text_paragraph((x,y,w),blk,f1,u"777")
						elif j == 1 and i == 1:
							y += text_paragraph((x,y,w),blk,f1,u"888")
						elif j == 1 and i == 2:
							y += text_paragraph((x,y,w),blk,f1,u"999")
						
						return y - y_prev
					
					
					y += table((x,y,w),(-1,-1,-1),((cont1,) * 3,) * 2)
			
			elif n == 4: # Konfigurieren
				if current:
					y += ays[0]
					
					name = u"Konfigurieren.active"
					if name not in state: state[name] = 1
					y += switch((x,y,w),name,u"Konfigurationen anwenden",action = action_parse)
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					labels = (
						u"Fehlertoleranz für Datenzugriffe einfügen",
						u"Toleranz bei Vergleich von Gleitkommazahlen",
						u"Algorithmen unterbrechbar machen",
					)
					
					name = u"Konfigurieren.choose"
					if name not in state: state[name] = {}
					
					for i,label in enumerate(labels):
						if i: y += blank(1)
						
						if i not in state[name]: state[name][i] = 0
						y += choose((x,y,w),name,i,label,action = action_parse,enabled = state[u"Konfigurieren.active"])
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					y += text_paragraph((x,y,w),blk,f1,u"Tabellen mit Anwendungen der Konfigurationen")
					
					y += ays[0]
				else:
					y += text_paragraph((x,y,w),gr1,f1,u"Konfiguration deaktiviert")
			
			elif n == 5: # Richtlinien
				
				values = (
					[5,6,1],
					[2,5,1],
					[1,200,1],
				)
				
				if current:
					y += ays[0]
					
					name = u"Richtlinien.choose"
					if name not in state: state[name] = {}
					
					for i in xrange(3):
						if i: y += blank(1)
						
						if i not in state[name]: state[name][i] = 0
						y += choose((x,y,w),name,i,u"Richtlinie %d" % (i + 1),action = action_layout) #TODO
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					for i,value in enumerate(values):
						if i: y += blank(2)
						
						y += pie_chart((x,y,w),u"Vorkommen nach Richtlinie %d" % (i + 1),value) #TODO
					
					y += ays[0]
				else:
					y += hbar_chart((x,y,w),[0,0,sum(float(n) / d if d > 0 else v for (n,d,v) in values) / len(values)])
			
			elif n == 6: # Code-Schemata
				
				values = (
					[5,6,1],
					[2,5,1],
					[1,200,1],
				)
				
				if current:
					y += ays[0]
					
					name = u"Schemata.choose"
					if name not in state: state[name] = {}
					
					for i in xrange(3):
						if i: y += blank(1)
						
						if i not in state[name]: state[name][i] = 0
						y += choose((x,y,w),name,i,u"Schema %d" % (i + 1),action = action_layout) #TODO
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					for i,value in enumerate(values):
						if i: y += blank(2)
						
						y += pie_chart((x,y,w),u"Vorkommen nach Schema %d" % (i + 1),value) #TODO
					
					y += ays[0]
				else:
					y += hbar_chart((x,y,w),[0,0,sum(float(n) / d if d > 0 else v for (n,d,v) in values) / len(values)])
			
			elif n == 7: # Orthogonale Bereiche
				if current:
					y += ays[0]
					
					
					def action1(arg):
						pass # TODO
					
					
					for i in xrange(2):
						if i: y += blank(1)
						
						#TODO Auswahl aus Gruppe %d entfernen
						y += button((x,y,w),u"Auswahl zu Gruppe %d hinzufügen" % (i + 1),action1,i)
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					for i in xrange(2):
						if i: y += blank(1)
						
						name = u"Orthogonale Bereiche.placeholder %d" % i
						if name not in state: state[name] = 0
						y += switch((x,y,w),name,u"Platzhalter für Gruppe %d aktivieren" % (i + 1),action = action_layout)
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					y += text_paragraph((x,y,w),blk,f1,u"Tabelle mit Vorkommen von Bereichen in Gruppe 1")
					y += blank(1)
					y += text_paragraph((x,y,w),blk,f1,u"Tabelle mit Vorkommen von Bereichen in Gruppe 2")
					
					y += ays[0]
				else:
					
					
					def cont1((x,y,w),i,j):
						y_prev = y
						
						if j: y += blank(0)
						
						if j == 0 and i == 0:
							y += text_paragraph((x,y,w),blk,f1,u"Gruppe 1:")
						elif j == 0 and i == 1:
							y += text_paragraph((x,y,w),blk,f1,u"Gruppe 2:")
						elif j == 1 and i == 0:
							y += text_paragraph((x,y,w),blk,f1,u"0")
						elif j == 1 and i == 1:
							y += text_paragraph((x,y,w),blk,f1,u"0 (Platzhalter)")
						
						return y - y_prev
					
					
					y += table((x,y,w),(-1,-1),((cont1,) * 2,) * 2)
			
			elif n == 8: # Komplexitätsklassen
				if current:
					y += ays[0]
					
					y += text_paragraph((x,y,w),blk,f1,u"Keine Schleife ausgewählt")
					
					y += ays[0]
				else:
					y += text_paragraph((x,y,w),gr1,f1,u"Keine Schleife ausgewählt")
			
			elif n == 9: # Datenfluss
				if current:
					y += ays[0]
					
					y += text_paragraph((x,y,w),blk,f1,u"Keine Variable ausgewählt")
					
					y += ays[0]
				else:
					y += text_paragraph((x,y,w),gr1,f1,u"Keine Variable ausgewählt")
			
			elif n == 10: # Erläuterung des Programmcodes
				if current:
					y += ays[0]
					
					y += text_paragraph((x,y,w),blk,f1,u"Keine Beschreibung verfügbar")
					
					y += ays[0]
				else:
					y += text_paragraph((x,y,w),gr1,f1,u"Keine Beschreibung verfügbar")
			
			elif n == 11: # Knoten-Eigenschaften
				y += ays[0]
				
				x_prev = x
				
				colorss = [[blk,gr1,gr2,gr3,grn,org,red,yl1,yl2,bl1,bl2,bl3,bl4],[(.8,0,.2),(.9,.6,0),(0,.6,0),(.25,.5,1),blk,(.09,.53,.62),(.67,.48,.13),(.1,.3,.6),(.85,.8,.72),(.25,.5,1),(.7,.8,1)]]
				for colors in colorss:
					for color in colors:
						fcirc(x + 5,y + 5,5,color)
						x += 20
					
					x = x_prev
					y += 20
				
				y -= 10
				
				y += ays[0]
			
			elif n == 12: # Über dieses Programm
				if current:
					y += ays[0]
					
					y += text_paragraph((x,y,w),blk,f1,u"Gbps-proto ist ein Prototyp des graphgestützten Programmiersystems, einem Werkzeugkasten zur Herstellung leistungsfähigerer Software. Die enthaltenen Funktionen geben einen Ausblick auf die Möglichkeiten dieser neuen Technologie. Es handelt sich um kein vollständiges Produkt, insbesondere sind weder Datenbank noch Programmanalyse enthalten. Viele der enthaltenen Werkzeuge funktionieren nur im Rahmen dieser Demonstration.")
					
					y += blank(1)
					
					y += text_paragraph((x,y,w),blk,f1,u"Bitte verwenden Sie gbps-proto nicht im produktiven Umfeld.")
					
					y += ays[0] * 2
					y += hline((x,y,w))
					y += ays[0] * 2
					
					
					def action1(arg):
						webbrowser.open("mailto:aaron.lorey@gmail.com",new = 2)
					
					
					y += button((x,y,w),u"E-Mail an den Autor senden",action1)
					
					y += ays[0] * 2
					
					
					def action1(arg):
						webbrowser.open("https://drive.google.com/drive/folders/1Hpoeb2pOeTGcAD1XpKgKiNcjKz2dut8P",new = 2)
					
					
					y += button((x,y,w),u"Informationsmaterial zur Geschäftsidee",action1)
					
					y += ays[0]
				else:
					pass
			
			elif n == 13: # Alle Reiter schließen
				y += ays[0]
				
				y += text_paragraph((x,y,w),blk,f1,u"Wählen Sie diesen Reiter, um alle anderen Reiter zu schließen und so alle Kurz-Infos im Blick zu haben.")
				
				y += ays[0]
			
			elif n == 14: # Interaktive Ausführung
				if current:
					pass
				else:
					pass
			
			elif n == 15: # Datei-Verlauf
				
				# Dateien (Pfad, akt. sha256, anzahl Snapshots, Speicherplatz) -> Snapshots (Datum, Speicherplatz)
				
				# Snapshot löschen
				# alle Snapshots löschen  1x confirm
				
				if current:
					pass
				else:
					pass
			
			elif n == 16: # Knoten-Navigator
				if current:
					y += ays[0]
					
					if "no parse" in files[ct][3] and files[ct][3]["no parse"]\
					or not ast_selected1[1]:
						
						y += text_paragraph((x,y,w),blk,f1,u"Kein Knoten ausgewählt")
					
					else:
						
						
						def action1(line):
							global request_layout
							
							if 4 < len(line)\
							and line[4]:
								
								ast_selected1[1] = line[4]
								pos = ctx1[ct]["positions"][id(line[4])][0]
								
								select = selects[ct]
								
								select[:] = [[pos[0],pos[1],pos[1]],[pos[0],pos[1],pos[1]]]
								
								validate_caret(0)
								validate_caret(1)
								
								request_layout |= {0}
						
						
						siblings = ((),())
						if id(ast_selected1[1]) in ctx1[ct]["parents"]:
							parent = ctx1[ct]["parents"][id(ast_selected1[1])]
							index = tuple(i for i,z in enumerate(parent[1]) if z is ast_selected1[1]) or (0,)
							siblings = (
								tuple(node for node in parent[1][:index[0]] if isinstance(node,list) and node[0][0] not in {"comment","whitespace","keyword"}),
								tuple(node for node in parent[1][index[0] + 1:] if isinstance(node,list) and node[0][0] not in {"comment","whitespace","keyword"}),
							)
						children = tuple(node for node in ast_selected1[1][1] if isinstance(node,list) and node[0][0] not in {"comment","whitespace","keyword"})
						
						path = list(reversed(ast_ancestor(ast_selected1[1],(),ctx1[ct])))
						if path and path[0] == ctx1[ct]["pars"][0]: path = path[1:]
						path = path[:-1]
						
						
						liness = []
						
						for i,node in enumerate(path):
							liness += [[0,1,f1,ast_name(node,ctx1[ct]),node]]
						
						if path:
							liness += [[-1]]
						
						for node in siblings[0]:
							liness += [[1,1,f1,ast_name(node,ctx1[ct]),node]]
						
						if siblings[0]:
							liness += [[-1]]
						
						liness += [[1,1,f2,ast_name(ast_selected1[1],ctx1[ct]),ast_selected1[1]]]
						
						if children:
							liness += [[-1]]
						
						for node in children:
							liness += [[2,1,f1,ast_name(node,ctx1[ct]),node]]
						
						if siblings[1]:
							liness += [[-1]]
						
						for node in siblings[1]:
							liness += [[1,1,f1,ast_name(node,ctx1[ct]),node]]
						
						y += indent((x,y,w),liness,True,action = action1)
					
					y += ays[0]
				else:
					pass
			
			elif n == 17: # Knoten-Liste
				if current:
					pass
				else:
					pass
			
			return y
		
		
		
	
	
		global disable_draw_prs
		disable_draw_prs = False
		
		def clip(x1,x2,y1,y2):
			if disable_draw_prs: return
			
			ctx.reset_clip()
			ctx.rectangle(x1,y1,max(0,x2 - x1),max(0,y2 - y1))
			ctx.clip()
		
		def frect(x1,x2,y1,y2,(r,g,b),a = 1):
			if disable_draw_prs: return
			
			ctx.set_source_rgba(r,g,b,a)
			ctx.rectangle(x1,y1,x2 - x1,y2 - y1)
			ctx.fill()
		
		def srect(x1,x2,y1,y2,(r,g,b),a = 1,lw = 1):
			if disable_draw_prs: return
			
			ctx.set_line_width(lw)
			ctx.set_source_rgba(r,g,b,a)
			ctx.rectangle(x1 + .5,y1 + .5,x2 - x1 - 1,y2 - y1 - 1)
			ctx.stroke()
		
		def fcirc(x,y,rad,(r,g,b)):
			if disable_draw_prs: return
			
			ctx.set_source_rgb(r,g,b)
			ctx.arc(x,y,rad,0,2 * math.pi)
			ctx.fill()
		
		def ftri(x,y,rho,phi,(r,g,b)):
			if disable_draw_prs: return
			
			verts = [
				[rho * math.cos(phi),rho * math.sin(phi)],
				[rho * math.cos(phi + 2./3 * math.pi),rho * math.sin(phi + 2./3 * math.pi)],
				[rho * math.cos(phi + 4./3 * math.pi),rho * math.sin(phi + 4./3 * math.pi)],
			]
			
			ctx.set_source_rgb(r,g,b)
			ctx.move_to(x + verts[0][0],y + verts[0][1])
			ctx.line_to(x + verts[1][0],y + verts[1][1])
			ctx.line_to(x + verts[2][0],y + verts[2][1])
			ctx.fill()
		
		def text_extents((ff,fs,fe,fw),textt):
			ctx.select_font_face(ff,cairo.FONT_SLANT_NORMAL,fw)
			fo = ctx.get_font_options()
			fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
			ctx.set_font_options(fo)
			ctx.set_font_size(fs)
			
			return ctx.text_extents(textt)[2]
		
		def text(x,y,(r,g,b),(ff,fs,fe,fw),textt):
			if disable_draw_prs: return
			
			#srect(x,x + (len(textt) * fe[0] if fe[0] else text_extents((ff,fs,fe,fw),textt)),y,y + fe[2],red)
			
			ctx.set_source_rgb(r,g,b)
			ctx.select_font_face(ff,cairo.FONT_SLANT_NORMAL,fw)
			fo = ctx.get_font_options()
			fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
			ctx.set_font_options(fo)
			ctx.set_font_size(fs)
			ctx.move_to(x,y + fe[2])
			ctx.show_text(textt)
			ctx.fill()
		
		def text_fit(x,y,w,(r,g,b),(ff,fs,fe,fw),textt):
			if disable_draw_prs: return
			
			#srect(x,x + (len(textt) * fe[0] if fe[0] else text_extents((ff,fs,fe,fw),textt)),y,y + fe[2],red)
			
			ctx.set_source_rgb(r,g,b)
			ctx.select_font_face(ff,cairo.FONT_SLANT_NORMAL,fw)
			fo = ctx.get_font_options()
			fo.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
			ctx.set_font_options(fo)
			ctx.set_font_size(fs)
			
			extent = max(1,ctx.text_extents(textt)[2])
			
			for i in xrange(100):
				scale = min(1,float(w) / extent)
				
				ctx.save()
				ctx.translate(x,y + fe[2] // 2)
				ctx.scale(scale,scale)
				ctx.translate(-x,-(y + fe[2] // 2))
				
				if ctx.text_extents(textt)[2] * scale <= w:
					break
				
				ctx.restore()
				
				extent += 1
			
			ctx.move_to(x,y + fe[2])
			ctx.show_text(textt)
			ctx.fill()
			
			ctx.restore()
		
		def shadow_rect(x1,x2,y1,y2):
			if disable_draw_prs: return
			
			ctx.set_source_surface(surf_tile1)
			ctx.get_source().set_extend(cairo.EXTEND_REPEAT)
			ctx.get_source().set_filter(cairo.FILTER_NEAREST)
			matrix = ctx.get_source().get_matrix()
			matrix.scale(.125,.125)
			ctx.get_source().set_matrix(matrix)
			ctx.rectangle(x1,y1,x2 - x1,y2 - y1)
			ctx.fill()
		
		def shadow_in(x1,x2,y1,y2):
			if disable_draw_prs: return
			
			sb = sb_in
			rect = (x1 - sds_in[2] - sb,x2 + sds_in[0] + sb,y1 - sds_in[1] - sb,y2 + sds_in[3] + sb)
			
			ctx_mask.set_source_rgba(0,0,0,1)
			ctx_mask.rectangle(0,0,width,height)
			ctx_mask.fill()
			
			ctx_mask.save()
			ctx_mask.set_operator(cairo.OPERATOR_DEST_OUT)
			ctx_mask.set_source_rgba(0,0,0,1)
			ctx_mask.rectangle(rect[0],rect[2],rect[1] - rect[0],rect[3] - rect[2])
			ctx_mask.fill()
			ctx_mask.restore()
			
			pattern = cairo.LinearGradient(rect[0],0,rect[0] + sb,0)
			pattern.add_color_stop_rgba(0,0,0,0,1)
			pattern.add_color_stop_rgba(1,0,0,0,0)
			ctx_mask.set_source(pattern)
			ctx_mask.rectangle(rect[0],rect[2],sb,rect[3] - rect[2])
			ctx_mask.fill()
			
			pattern = cairo.LinearGradient(0,rect[3],0,rect[3] - sb)
			pattern.add_color_stop_rgba(0,0,0,0,1)
			pattern.add_color_stop_rgba(1,0,0,0,0)
			ctx_mask.set_source(pattern)
			ctx_mask.rectangle(rect[0],rect[3],rect[1] - rect[0],-sb)
			ctx_mask.fill()
			
			pattern = cairo.LinearGradient(rect[1],0,rect[1] - sb,0)
			pattern.add_color_stop_rgba(0,0,0,0,1)
			pattern.add_color_stop_rgba(1,0,0,0,0)
			ctx_mask.set_source(pattern)
			ctx_mask.rectangle(rect[1],rect[2],-sb,rect[3] - rect[2])
			ctx_mask.fill()
			
			pattern = cairo.LinearGradient(0,rect[2],0,rect[2] + sb)
			pattern.add_color_stop_rgba(0,0,0,0,1)
			pattern.add_color_stop_rgba(1,0,0,0,0)
			ctx_mask.set_source(pattern)
			ctx_mask.rectangle(rect[0],rect[2],rect[1] - rect[0],sb)
			ctx_mask.fill()
			
			ctx.set_source_surface(surf_tile2)
			ctx.get_source().set_extend(cairo.EXTEND_REPEAT)
			ctx.get_source().set_filter(cairo.FILTER_NEAREST)
			matrix = ctx.get_source().get_matrix()
			matrix.scale(.125,.125)
			ctx.get_source().set_matrix(matrix)
			ctx.mask_surface(surf_mask)
		
		def shadow_out(x1,x2,y1,y2):
			if disable_draw_prs: return
			
			sb = sb_out
			rect = (x1 - sds_out[0],x2 + sds_out[2],y1 - sds_out[3],y2 + sds_out[1])
			
			ctx_mask.save()
			ctx_mask.set_operator(cairo.OPERATOR_DEST_OUT)
			ctx_mask.set_source_rgba(0,0,0,1)
			ctx_mask.rectangle(0,0,width,height)
			ctx_mask.fill()
			ctx_mask.restore()
			
			ctx_mask.set_source_rgba(0,0,0,1)
			ctx_mask.rectangle(rect[0],rect[2],rect[1] - rect[0],rect[3] - rect[2])
			ctx_mask.fill()
			
			ctx_mask.save()
			ctx_mask.set_operator(cairo.OPERATOR_DEST_OUT)
			
			pattern = cairo.LinearGradient(rect[0],0,rect[0] + sb,0)
			pattern.add_color_stop_rgba(0,0,0,0,1)
			pattern.add_color_stop_rgba(1,0,0,0,0)
			ctx_mask.set_source(pattern)
			ctx_mask.rectangle(rect[0],rect[2],sb,rect[3] - rect[2])
			ctx_mask.fill()
			
			pattern = cairo.LinearGradient(0,rect[3],0,rect[3] - sb)
			pattern.add_color_stop_rgba(0,0,0,0,1)
			pattern.add_color_stop_rgba(1,0,0,0,0)
			ctx_mask.set_source(pattern)
			ctx_mask.rectangle(rect[0],rect[3],rect[1] - rect[0],-sb)
			ctx_mask.fill()
			
			pattern = cairo.LinearGradient(rect[1],0,rect[1] - sb,0)
			pattern.add_color_stop_rgba(0,0,0,0,1)
			pattern.add_color_stop_rgba(1,0,0,0,0)
			ctx_mask.set_source(pattern)
			ctx_mask.rectangle(rect[1],rect[2],-sb,rect[3] - rect[2])
			ctx_mask.fill()
			
			pattern = cairo.LinearGradient(0,rect[2],0,rect[2] + sb)
			pattern.add_color_stop_rgba(0,0,0,0,1)
			pattern.add_color_stop_rgba(1,0,0,0,0)
			ctx_mask.set_source(pattern)
			ctx_mask.rectangle(rect[0],rect[2],rect[1] - rect[0],sb)
			ctx_mask.fill()
			
			ctx_mask.restore()
			
			ctx.set_source_surface(surf_tile2)
			ctx.get_source().set_extend(cairo.EXTEND_REPEAT)
			ctx.get_source().set_filter(cairo.FILTER_NEAREST)
			matrix = ctx.get_source().get_matrix()
			matrix.scale(.125,.125)
			ctx.get_source().set_matrix(matrix)
			ctx.mask_surface(surf_mask)
		
	
	
		# text layout
		
		
		def text_paragraph((x,y,w),color,(ff,fs,fe,fw),textt,action = None,arg = None,usepadding = True,rightalign = False): # box, color, font, text, action, arg, usepadding, rightalign
			global buttons1
			
			y_prev = y
			
			textt = textt.split(" ")
			
			i = 0
			while i < len(textt):
				if i: y += f1[2][1]
				
				j = len(textt)
				while j - 1 > i:
					if text_extents((ff,fs,fe,fw)," ".join(textt[i:j])) <= w:
						break
					j -= 1
				
				align = w - text_extents((ff,fs,fe,fw)," ".join(textt[i:j])) if rightalign else 0
				
				text(x + align,y,color,(ff,fs,fe,fw)," ".join(textt[i:j]))
				
				i = j
			
			y += f1[2][2]
			
			if action:
				padding = blank(1) // 2 if usepadding else 0
				buttons1[4] += [((x,x + w,y_prev - padding,y + padding),("button",None,None,None,action,arg))]
			
			return y - y_prev
		
		
		def text_center((x,y,w),color,(ff,fs,fe,fw),textt): # box, color, font, text
			
			extent = text_extents((ff,fs,fe,fw),textt)
			
			if extent > w:
				text_fit(x,y,w,color,(ff,fs,fe,fw),textt)
			else:
				text(x + (w - extent) // 2,y,color,(ff,fs,fe,fw),textt)
			
			return f1[2][2]
		
		
		# simple objects
		
		
		def hline((x,y,w)): # box
			frect(x,x + w,y,y + 1,gr1)
			return 1
		
		
		def switch((x,y,w),name,label,action = None,arg = None,enabled = True): # box, name, label, action, arg, enabled
			global buttons1,state
			
			y_prev = y
			x_prev = x
			w_prev = w
			x += 10
			w -= 10
			
			if state[name]:
				frect(x + 10,x + 20,y,y + 9,bl1 if enabled else gr1)
				srect(x,x + 20,y,y + 9,bl1 if enabled else gr1)
				
				x += 40
				w -= 40
				y += text_paragraph((x,y,w),blk if enabled else gr1,f2,label + (u" (An)" if enabled else u" (Inaktiv)"))
			else:
				frect(x,x + 10,y,y + 9,gr1)
				srect(x,x + 20,y,y + 9,gr1)
				
				x += 40
				w -= 40
				y += text_paragraph((x,y,w),blk if enabled else gr1,f1,label + (u" (Aus)" if enabled else u" (Inaktiv)"))
			
			if enabled\
			and not disable_draw_prs:
				buttons1[4] += [((x_prev,x_prev + w_prev,y_prev - blank(1) // 2,y + blank(1) // 2),("switch",name,None,None,action,arg))]
			
			return y - y_prev
		
		
		def choose((x,y,w),name,value,label,ismulti = True,action = None,arg = None,enabled = True): # box, name, value, label, is-multi, action, arg, enabled
			global buttons1,state
			
			y_prev = y
			x_prev = x
			w_prev = w
			x += 10
			w -= 10
			
			if state[name][value]:
				frect(x + 10,x + 20,y,y + 9,bl1 if enabled else gr1)
				
				x += 40
				w -= 40
				y += text_paragraph((x,y,w),blk if enabled else gr1,f2,label + (u" (Ausgewählt)" if enabled else u" (Inaktiv)"))
			else:
				srect(x + 10,x + 20,y,y + 9,gr1)
				
				x += 40
				w -= 40
				y += text_paragraph((x,y,w),blk if enabled else gr1,f1,label + (u" (Nicht ausgewählt)" if enabled else u" (Inaktiv)"))
			
			if enabled\
			and not disable_draw_prs:
				buttons1[4] += [((x_prev,x_prev + w_prev,y_prev - blank(1) // 2,y + blank(1) // 2),("choose",name,value,ismulti,action,arg))]
			
			return y - y_prev
		
		
		def button((x,y,w),label,action,arg = None,size = 0,enabled = True): # box, label, action, arg, size, enabled
			global buttons1
			
			if not size:
				size = w - 20
			
			m = x + w // 2
			
			if enabled:
				frect(m - size // 2,m + size // 2,y,y + tys[1],gr1)
			srect(m - size // 2,m + size // 2,y,y + tys[1],blk if enabled else gr1)
			text_center((m - size // 2 + 10,y + (tys[1] - f1[2][2] + 1) // 2,size - 20),blk if enabled else gr1,f1,label)
			
			if enabled\
			and not disable_draw_prs:
				buttons1[4] += [((m - size // 2,m + size // 2,y,y + tys[1]),("button",None,None,None,action,arg))]
			
			return tys[1]
		
		
		def hbar_chart((x,y,w),(n,d,v)): # box, numerator, denominator, default-value
			if d > 0:
				v = float(n) / d
			if v != 0:
				v = max(.25 - math.atan(2) / 2 / math.pi,v)
			if v != 1:
				v = min(.75 + math.atan(2) / 2 / math.pi,v)
			
			if not disable_draw_prs:
				if v == 1:
					frect(x,x + w - 20,y + 6,y + 8,grn)
					
					ctx.set_line_width(2)
					ctx.set_source_rgb(*grn)
					ctx.move_to(x + w - 10,y + 3)
					ctx.line_to(x + w - 6,y + 7)
					ctx.line_to(x + w,y + 1)
					ctx.stroke()
				else:
					m = round(x + (w - 20) * v)
					if v != 0:
						m += 1
						frect(x,m - 2,y + 6,y + 8,grn)
					frect(m,x + w - 20,y,y + 8,red)
					frect(m + 1,x + w - 20 - 1,y + 1,y + 7,org)
					
					ctx.set_line_width(2)
					ctx.set_source_rgb(*red)
					ctx.move_to(x + w - 9,y)
					ctx.line_to(x + w - 1,y + 8)
					ctx.move_to(x + w - 1,y)
					ctx.line_to(x + w - 9,y + 8)
					ctx.stroke()
					
					ctx.set_line_width(1)
					ctx.set_source_rgb(*org)
					ctx.move_to(x + w - 9,y)
					ctx.line_to(x + w - 1,y + 8)
					ctx.move_to(x + w - 1,y)
					ctx.line_to(x + w - 9,y + 8)
					ctx.stroke()
			
			return 8
		
		
		# page layout
		
		
		def indent((x,y,w),liness,linetobottom = False,action = None): # box, lines, line-to-bottom, action
			y_prev = y
			
			indent = 0
			y_start = {}
			y_end = {}
			for i,line in enumerate(liness):
				if i: y += blank(1)
				
				if line[0] >= 0 and line[1] and line[0] not in y_start:
					indent = line[0]
					y_start[line[0]] = y
				
				if line[0] >= 0:
					y += text_paragraph((x + 20 * line[0],y,w - 20 * line[0]),blk,line[2],line[3],action = action,arg = line)
				
				if line[0] >= 0 and line[1]:
					y_end[line[0]] = y
				
				if i == len(liness) - 1:
					if linetobottom:
						for j in xrange(line[0] + 1):
							y_end[j] = y
					
					line = list(line)
					line[0] = 0
				
				if line[0] >= 0 and line[0] < indent:
					#print line[0], indent, y_start, y_end
					for j in xrange(line[0] + 1,indent + 1):
						
						if j in y_start:
							frect(x + 20 * (j - 1) + 4.5,x + 20 * (j - 1) + 5.5,y_start[j],y_end[j],blk)
						
						if j in y_start: del y_start[j]
					
					indent = line[0]
			
			return y - y_prev
		
		
		def column((x,y,w),cols,conts): # box, cols (-1 is fill), content
			fixedwidth = sum(col for col in cols if col >= 0)
			remaining = w - fixedwidth
			nfills = sum(1 for col in cols if col < 0)
			fillwidth = remaining // nfills if nfills else 0
			lastfill = (-i - 1 for i,col in enumerate(reversed(cols)) if col < 0)
			
			cols = [(col if col >= 0 else fillwidth) for col in cols]
			
			try: cols[next(lastfill)] = remaining - fillwidth * (nfills - 1)
			except: pass
			
			#print w, fixedwidth, remaining, nfills, fillwidth, remaining - fillwidth * (nfills - 1)
			#print cols
			
			h = [0] * len(conts)
			for i,cont in enumerate(conts):
				if cont:
					h[i] = cont((x,y,cols[i]),i)
				x += cols[i]
			
			#print h
			
			return max(h)
		
		
		def table((x,y,w),cols,contss): # box, cols (-1 is fill), content
			y_prev = y
			x_prev = x
			
			fixedwidth = sum(col for col in cols if col >= 0)
			remaining = w - fixedwidth
			nfills = sum(1 for col in cols if col < 0)
			fillwidth = remaining // nfills if nfills else 0
			lastfill = (-i - 1 for i,col in enumerate(reversed(cols)) if col < 0)
			
			cols = [(col if col >= 0 else fillwidth) for col in cols]
			
			try: cols[next(lastfill)] = remaining - fillwidth * (nfills - 1)
			except: pass
			
			#print w, fixedwidth, remaining, nfills, fillwidth, remaining - fillwidth * (nfills - 1)
			#print cols
			
			for j,conts in enumerate(contss):
				x = x_prev
				
				h = [0] * len(conts)
				for i,cont in enumerate(conts):
					if cont:
						h[i] = cont((x,y,cols[i]),i,j)
					x += cols[i]
				
				y += max(h)
			
			return y - y_prev
		
		
		# complex objects
		
		
		def pie_chart((x,y,w),label,(n,d,v)): # box, label, numerator, denominator, default-value
			y_prev = y
			
			if d > 0:
				v = float(n) / d
			if v != 0:
				v = max(.25 - math.atan(2) / 2 / math.pi,v)
			if v != 1:
				v = min(.75 + math.atan(2) / 2 / math.pi,v)
			
			y += text_paragraph((x,y,w),blk,f1,label)
			y += blank(1)
			
			
			def cont1((x,y,w),i):
				if not disable_draw_prs:
					
					if v == 1:
						ctx.set_line_width(2)
						ctx.set_source_rgb(*grn)
						ctx.arc(x + 30,y + 24,23,0,2 * math.pi)
						ctx.stroke()
						
						ctx.set_line_width(2)
						ctx.set_source_rgb(*grn)
						ctx.move_to(x + 18,y + 22)
						ctx.line_to(x + 28,y + 32)
						ctx.line_to(x + 44,y + 16)
						ctx.stroke()
					
					else:
						ctx.set_line_width(8)
						ctx.set_source_rgb(*red)
						ctx.arc(x + 30,y + 30,26,3./2 * math.pi,(3./2 + 2 * (1 - v)) * math.pi)
						ctx.stroke()
						
						ctx.set_line_width(6)
						ctx.set_source_rgb(*org)
						ctx.arc(x + 30,y + 30,26,3./2 * math.pi,(3./2 + 2 * (1 - v)) * math.pi)
						ctx.stroke()
						
						if v != 0:
							
							verts = [
								[22 * math.cos(3./2 * math.pi),22 * math.sin(3./2 * math.pi)],
								[22 * math.cos((3./2 + 2 * (1 - v)) * math.pi),22 * math.sin((3./2 + 2 * (1 - v)) * math.pi)],
								[30 * math.cos(3./2 * math.pi),30 * math.sin(3./2 * math.pi)],
								[30 * math.cos((3./2 + 2 * (1 - v)) * math.pi),30 * math.sin((3./2 + 2 * (1 - v)) * math.pi)],
							]
							
							ctx.set_line_width(4)
							ctx.set_source_rgb(*red)
							ctx.move_to(x + 30 + verts[0][0],y + 30 + verts[0][1])
							ctx.line_to(x + 30 + verts[2][0],y + 30 + verts[2][1])
							ctx.move_to(x + 30 + verts[1][0],y + 30 + verts[1][1])
							ctx.line_to(x + 30 + verts[3][0],y + 30 + verts[3][1])
							ctx.stroke()
							
							ctx.set_line_width(10)
							ctx.set_source_rgb(*wht)
							ctx.arc(x + 30,y + 30,26,(3./2 + 2 * (1 - v)) * math.pi,3./2 * math.pi)
							ctx.stroke()
							
							ctx.set_line_width(2)
							ctx.set_source_rgb(*grn)
							ctx.arc(x + 30,y + 30,23,(3./2 + 2 * (1 - v)) * math.pi,3./2 * math.pi)
							ctx.stroke()
							
							verts = [
								[21 * math.cos(3./2 * math.pi),21 * math.sin(3./2 * math.pi)],
								[21 * math.cos((3./2 + 2 * (1 - v)) * math.pi),21 * math.sin((3./2 + 2 * (1 - v)) * math.pi)],
								[31 * math.cos(3./2 * math.pi),31 * math.sin(3./2 * math.pi)],
								[31 * math.cos((3./2 + 2 * (1 - v)) * math.pi),31 * math.sin((3./2 + 2 * (1 - v)) * math.pi)],
							]
							
							ctx.set_line_width(2)
							ctx.set_source_rgb(*wht)
							ctx.move_to(x + 30 + verts[0][0],y + 30 + verts[0][1])
							ctx.line_to(x + 30 + verts[2][0],y + 30 + verts[2][1])
							ctx.move_to(x + 30 + verts[1][0],y + 30 + verts[1][1])
							ctx.line_to(x + 30 + verts[3][0],y + 30 + verts[3][1])
							ctx.stroke()
						
						ctx.set_line_width(2)
						ctx.set_source_rgb(*red)
						ctx.move_to(x + 20,y + 20)
						ctx.line_to(x + 40,y + 40)
						ctx.move_to(x + 40,y + 20)
						ctx.line_to(x + 20,y + 40)
						ctx.stroke()
						
						ctx.set_line_width(1)
						ctx.set_source_rgb(*org)
						ctx.move_to(x + 20,y + 20)
						ctx.line_to(x + 40,y + 40)
						ctx.move_to(x + 40,y + 20)
						ctx.line_to(x + 20,y + 40)
						ctx.stroke()
						
						ctx.set_line_width(3)
						ctx.set_source_rgba(*(wht + (.75,)))
						ctx.move_to(x + 19,y + 19)
						ctx.line_to(x + 41,y + 41)
						ctx.move_to(x + 41,y + 19)
						ctx.line_to(x + 19,y + 41)
						ctx.stroke()
				
				return 48 if v == 1 else 60
			
			
			def cont2((x,y,w),i):
				y_prev = y
				
				fcirc(x + 5,y + 5,5,grn)
				
				y += text_paragraph((x + 20,y,w - 20),blk,f1,u"%d/%d konform" % (n,d))
				
				if v != 1:
					y += blank(1)
					
					fcirc(x + 5,y + 5,5,red)
					fcirc(x + 5,y + 5,4,org)
					
					y += text_paragraph((x + 20,y,w - 20),blk,f2,u"%d/%d nicht konform" % (d - n,d))
				
				return y - y_prev
			
			
			y += column((x,y,w),(10,60,20,-1),(None,cont1,None,cont2))
			
			return y - y_prev
		
		
		def feature_table((x,y,w),features,occurrences): # box, features, occurrences #TODO use table()
			y_prev = y
			x_prev = x
			w_prev = w
			
			y_starts = [0] * len(features)
			
			for i,feature in enumerate(features):
				y += text_paragraph((x,y,w),blk,f2,feature)
				y += blank(1)
				
				y_starts[i] = y
				
				x += 20
				w -= 20
			
			y_end = y + 5
			x = x_prev
			w = w_prev
			
			for i,feature in enumerate(features):
				frect(x + 4,x + 6,y_starts[i],y_end,ccn(i))
				x += 20
			
			x = x_prev
			
			for i,feature in enumerate(features):
				ftri(x + 5,y + 5,5,1./2 * math.pi,ccn(i))
				x += 20
			
			y += 10
			
			for j,occurrence in enumerate(occurrences[:8]):
				x = x_prev
				y += blank(1)
				
				for i,feature in enumerate(occurrence[0]):
					if feature:
						fcirc(x + 5,y + 5,4,ccn(i))
					else:
						fcirc(x + 5,y + 5,.5,blk)
					x += 20
				
				x += 10
				
				y += text_paragraph((x,y,w - 130),blk,f1,occurrence[1])
			
			y += blank(1)
			x = x_prev
			
			for i,feature in enumerate(features):
				ftri(x + 5,y + 5,5,-1./2 * math.pi,ccn(i))
				x += 20
			
			y += 10
			x = x_prev
			
			if len(occurrences) > 8:
				y += blank(1)
				y += text_paragraph((x,y,w),blk,f1,u"Die Tabelle wurde auf 8 Einträge gekürzt, von zuvor %d Einträgen." % len(occurrences))
			
			return y - y_prev
		
		
		if 0 in update or 3 in update:
			validate_editor1()
		
		
		#lc = int(math.ceil(math.log10(len(lines[ct]) + 1))) # lnum cols
		lc = len(unicode(len(lines[ct]))) # lnum cols
		
		
		txs = (4,100) # tab xs
		tys = (4,30) # tab ys
		axs = (10,) # accordion xs
		ays = (10,22) # accordion ys
		cys = (4,22) # context menu ys
		ixs = (20,10,280) # info card xs
		iys = (20,10) # info card ys
		
		
		xs = (0,1,(lc + 2) * f3[2][0] + 2,width - txs[0] - 302 - (mc + 1) * 6 - 8,width - txs[0] - 302,width - txs[0] - 301,width - 301,width - 300,width - 1,width) # layout xs
		
		tc = max(1,(xs[6] - xs[1] - txs[0]) // (txs[0] + txs[1])) # tab cols
		tr = (len(titles) - 1) // tc + 1 # tab rows
		th = tys[0] + (tys[0] + tys[1]) * tr # tab height
		
		ys = (0,1,th + 1,th + 2,th + tys[1],th + tys[1] + 1,height - 2,height - 1,height) # layout ys
		
		sds_in = (3,-5,-5,3) # shadow dists in
		sds_out = (-3,5,5,-3) # shadow dists out
		sb_in = 6 # shadow blur size in
		sb_out = 6 # shadow blur size out
		
		
		titles[:] = [title or files[n][1] or u"Neuer Tab" for n,title in enumerate(titles)] # this has effect on ah !
		
		
		measure["layout toplevel 1"] = measure["layout toplevel 1"].tick()
		
		measure["layout toplevel 2"] = pygame.time.Clock()
		
		
		if 0 in update or 3 in update:
			validate_editor2()
		
		
		if 0 in update or 3 in update:
			validate_editor3()
		
		
		measure["layout toplevel 2"] = measure["layout toplevel 2"].tick()
		
		measure["layout toplevel 3"] = pygame.time.Clock()
		
		
		if anim_tick >= 0\
		and ca == 13:
			scroll2 = [0,-1]
		
		if scroll2[1] >= 0:
			scroll2[0] = scroll2[1]
		
		ah = -1 # accordion height
		ahs = [None] * len(tools)
		for n,tool in enumerate(tools):
			ah_prev = ah
			
			if anim_tick >= 0:
				if n == ca:
					tool_ctrs[n] += anim_tick
					tool_ctrs[n] = min(150,tool_ctrs[n])
				else:
					tool_ctrs[n] -= anim_tick
					tool_ctrs[n] = max(0,tool_ctrs[n])
			
			ahs[n] = (
				0 if tool_ctrs[n] == 150 else get_accordion_height(n,False),
				0 if tool_ctrs[n] == 0 else get_accordion_height(n,True),
			)
			
			ah += ays[1]
			ah += ays[0]
			ah += (ahs[n][1] * tool_ctrs[n] + ahs[n][0] * (150 - tool_ctrs[n])) // 150
			ah += ays[0]
			
			if anim_tick >= 0\
			and n == ca:
				if ca != 13: scroll2[0] = max(ah + ys[1] - ys[6],scroll2[0])
				scroll2[0] = min(ah_prev + 1,scroll2[0])
		
		scroll2[0] = min(ah + ys[1] - ys[6],scroll2[0])
		scroll2[0] = max(0,scroll2[0])
		
		if scroll2[1] < 0\
		or anim_tick >= 0:
			scroll2[1] = scroll2[0]
		
		
		#print update, anim_tick,tool_ctrs, ca, scroll2
		
		
		measure["layout toplevel 3"] = measure["layout toplevel 3"].tick()
		
		
		# clear
		
		
		if 0 in update: # complete redraw
			clip(xs[0],xs[9],ys[0],ys[8])
			
			frect(xs[0],xs[9],ys[0],ys[8],wht)
			
			buttons1 = [[] for i in xrange(5)]
		
		if 1 in update: # update tabs
			
			buttons1[1] = []
		
		if 2 in update: # update active tab
			pass
		
		if 3 in update: # update editor
			clip(xs[1],xs[4],ys[5],ys[6])
			
			frect(xs[1],xs[4],ys[5],ys[6],wht)
			
			buttons1[3] = []
		
		if 4 in update: # update accordion
			clip(xs[7],xs[8],ys[1],ys[6])
			
			frect(xs[7],xs[8],ys[1],ys[6],wht)
			
			buttons1[4] = []
		
		if 5 in update: # update context menu
			pass
		
		buttons3_1 = []
		buttons3_2 = []
		
		
		# tabs
		
		
		if 0 in update or 1 in update:
			
			
			clip(xs[1],xs[6],ys[1],ys[2])
			
			shadow_rect(xs[1],xs[6],ys[1],ys[2])
			shadow_in(xs[1],xs[6],ys[1],ys[2])
			
			
			clip(xs[5],xs[6],ys[2],ys[6])
			
			shadow_rect(xs[5],xs[6],ys[2],ys[6])
			shadow_out(xs[5] + sds_out[0] - sb_out,xs[5],ys[2],ys[6])
			
			
			m = 0
			for n,title in enumerate(titles):
				
				if m == rearrange2:
					m += 1
				
				if n == rearrange1:
					continue
				
				
				clip(xs[1],xs[6],ys[1],ys[2])
				
				i = m % tc
				j = m // tc
				(x,y) = (xs[1] + txs[0] + (txs[0] + txs[1]) * i,ys[1] + tys[0] + (tys[0] + tys[1]) * j)
				
				shadow_out(x,x + txs[1],y,y + tys[1])
				
				srect(x,x + txs[1],y,y + tys[1],blk)
				frect(x + 1,x + txs[1] - 1,y + 1,y + tys[1] - 1,gr1 if n == ct else wht)
				
				
				# tab label
				
				
				clip(x + 1,min(xs[6],x + txs[1] - 1),y + 1,min(ys[2],y + tys[1] - 1))
				
				text_fit(x + 10,y + (tys[1] - f1[2][2] + 1) // 2,txs[1] - 20,blk,f1,title)
				
				
				m += 1
			
			if rearrange1 >= 0:
				
				
				clip(xs[1],xs[6],ys[1],ys[2] + tys[1] // 2)
				
				n = rearrange1
				
				(x,y) = rearrange_pos2
				
				srect(x,x + txs[1],y,y + tys[1],blk,.75)
				frect(x + 1,x + txs[1] - 1,y + 1,y + tys[1] - 1,gr1 if n == ct else wht,.75)
			
			
			for n,title in enumerate(titles):
				i = n % tc
				j = n // tc
				(x,y) = (xs[1] + txs[0] + (txs[0] + txs[1]) * i,ys[1] + tys[0] + (tys[0] + tys[1]) * j)
				
				buttons1[1] += [((x - txs[0] // 2,x + txs[1] + txs[0] // 2,y - tys[0] // 2,y + tys[1] + tys[0] // 2),(actionTab,n))]
		
		
		# active tab
		
		
		if 0 in update or 1 in update or 2 in update:
			
			
			clip(xs[1],xs[4],ys[3],ys[4])
			
			frect(xs[1],xs[4],ys[3],ys[4],gr1)
			text(xs[1] + 9,ys[3] + (tys[1] - f1[2][2] + 1) // 2 - 1,blk,f1,titles[ct])
		
		
		# editor text
		
		
		if 0 in update or 3 in update:
			
			
			layout_editor()
		
		
		# editor sides
		
		
		if 0 in update or 3 in update:
			
			
			layout_editor_sides()
		
		
		# accordion
		
		
		if 0 in update or 4 in update:
			
			
			y = ys[1] - scroll2[0] - 1
			for n,tool in enumerate(tools):
				
				
				clip(xs[7],xs[8],ys[1],ys[6])
				
				frect(xs[7],xs[8],y,y + ays[1],gr1)
				text(xs[7] + 9,y + (ays[1] - f1[2][2] + 1) // 2,blk,f2 if n == ca else f1,tool)
				frect(xs[7],xs[8],y,y + 1,blk)
				frect(xs[7],xs[8],y + ays[1] - 1,y + ays[1],blk)
				
				y += ays[1]
				y_prev = y
				y += ays[0]
				y += (ahs[n][1] * tool_ctrs[n] + ahs[n][0] * (150 - tool_ctrs[n])) // 150
				y += ays[0]
				
				
				clip(xs[7],xs[8],y_prev,y)
				
				y_prev += ays[0]
				layout_accordion(n,tool_ctrs[n] > 75,y_prev)
				y_prev -= ays[0]
				frect(xs[7],xs[8],y_prev,y,wht,1 - abs(tool_ctrs[n] - 75) / 75.)
			
			
			y = ys[1] - 1
			for n,tool in enumerate(tools):
				y_prev = y
				
				y += ays[1]
				y += ays[0]
				y += (ahs[n][1] * tool_ctrs[n] + ahs[n][0] * (150 - tool_ctrs[n])) // 150
				y += ays[0]
				
				if n == len(tools) - 1:
					y = max(ys[6],y)
				
				if n != ca:
					buttons1[4] += [((xs[7],xs[8],y_prev - scroll2[0],y - scroll2[0]),(actionAccordion,n))]
		
		
		# main layout
		
		
		if 0 in update or True:
			
			
			clip(xs[0],xs[9],ys[0],ys[8])
			
			srect(xs[0],xs[9],ys[0],ys[7],blk)
			frect(xs[4],xs[4] + 1,ys[2],ys[6],blk)
			frect(xs[6],xs[6] + 1,ys[1],ys[6],blk)
			frect(xs[1],xs[4],ys[2],ys[2] + 1,blk)
			frect(xs[1],xs[4],ys[4],ys[4] + 1,blk)
			srect(xs[0],xs[9],ys[7],ys[8],wht)
		
		
		# context menu
		
		
		buttons3_1 += [((xs[1],xs[6],ys[1],ys[4]),[
			(u"1. Tab speichern",(actionSave,{"set_ct": True})), #TODO set ct
			(u"2. Alle Tabs speichern",(actionSaveAll,None)),
			(u"3. Tab schließen",(actionClose,{"set_ct": True})), #TODO set ct
			(u"4. Tab wiederherstellen",(actionRestore,None)),
			(u"5. Neuer Tab",(actionNew,None)),
			(u"6. Tab öffnen",(actionOpen,None)),
			(u"7. Titel festlegen",(actionTitle,{"set_ct": True})), #TODO set ct
		])]
		
		buttons3_1 += [((xs[1],xs[4],ys[5],ys[6]),[
			(u"1. Rückgängig",(actionUndo,None)),
			(u"2. Wiederholen",(actionRedo,None)),
			(u"3. Ausschneiden",(actionCut,None)),
			(u"4. Kopieren",(actionCopy,None)),
			(u"5. Einfügen",(actionPaste,None)),
			(u"6. Alles markieren",(actionSelectAll,None)),
		])]
		
		button = ((xs[7],xs[8],ys[1],ys[6]),[
			(u"X. Alle Reiter schließen",(actionAccordion,13)),
			(u"0. Dateien / Tabs",(actionAccordion,0)),
			(u"1. Abfrage",(actionAccordion,1)),
			(u"2. Umschreiben",(actionAccordion,2)),
			(u"3. Merkmale",(actionAccordion,3)),
			(u"4. Konfigurieren",(actionAccordion,4)),
			(u"5. Richtlinien",(actionAccordion,5)),
			(u"6. Code-Schemata",(actionAccordion,6)),
			(u"7. Orthogonale Bereiche",(actionAccordion,7)),
			(u"8. Komplexitätsklassen",(actionAccordion,8)),
			(u"9. Datenfluss",(actionAccordion,9)),
			(u"10. Erläuterung",(actionAccordion,10)),
			(u"11. Knoten-Eigenschaften",(actionAccordion,11)),
			(u"12. Über das Programm",(actionAccordion,12)),
		])
		buttons3_1 += [button]
		
		if "advanced mode" in settings and settings["advanced mode"]:
			button = button[1]
			button += [
				(u"A. Interaktive Ausführung",(actionAccordion,14)),
				(u"A. Datei-Verlauf",(actionAccordion,15)),
				(u"A. Knoten-Navigator",(actionAccordion,16)),
				(u"A. Knoten-Liste",(actionAccordion,17)),
			]
		
		
		if 0 in update or 5 in update or True:
			
			
			if menu_actions:
				
				buttons1 = [[] for i in xrange(5)]
				buttons3_1 = []
				buttons3_2 = []
				
				y = cys[0] + len(menu_actions) * (cys[0] + cys[1])
				off = [
					100,
					-100,
					cys[0] + cys[1] // 2,
					cys[0] + cys[1] // 2 - y,
				]
				pos = list(menu_pos1)
				pos[0] = min(xs[9] + off[1],pos[0])
				pos[0] = max(off[0],pos[0])
				pos[1] = min(ys[7] + off[3],pos[1])
				pos[1] = max(off[2],pos[1])
				pos = [
					pos[0] - off[0],
					pos[0] - off[1],
					pos[1] - off[2],
					pos[1] - off[3],
				]
				
				
				clip(xs[0],xs[9],ys[0],ys[8])
				
				frect(pos[0],pos[1],pos[2],pos[3],wht)
				srect(pos[0],pos[1],pos[2],pos[3],blk)
				
				y = pos[2]
				for menu_action in menu_actions:
					hover = get_button(menu_pos2,[((pos[0],pos[1],y,y + cys[0] + cys[1]),)])
					text(pos[0] + 10,y + cys[0] + (cys[1] - f1[2][2] + 1) // 2,blk,f2 if hover else f1,menu_action[0])
					buttons3_2 += [((pos[0],pos[1],y,y + cys[0] + cys[1]),menu_action[1])]
					
					y += cys[0] + cys[1]
		
		
		# flip buffers
		
		
		pygame.display.flip()
		
		
		measure["layout"] = measure["layout"].tick()
		
		
		print "---"
		for m in measure.items(): print "%5d %s" % (m[1],m[0])
	
	
	
	# token types:
	#
	# comment
	# whitespace
	# literal
	# keyword
	# identifier
	# brackets
	# statement
	# mark
	# prop group
	#
	
	
	# statement types:
	#
	# expression
	# control (contains python structure)
	# structure (java only)
	#
	
	
	
	match_statement = lambda statement,k: ("t",statement,k)
	
	match_list = lambda pattern,k: ("p",(
		pattern,
		("*",(
			",",
			pattern,
		)),
	),k)
	
	match_expression = lambda k: ("|",(
		(
			match_statement(("statement","expression"),k),
		),
		(
			match_statement(("literal",),k),
		),
		(
			match_statement(("identifier",),k),
		),
		(
			match_statement(("brackets","("),k),
		),
		(
			match_statement(("brackets","{"),k),
		),
		(
			match_statement(("brackets","indentation"),k),
		),
	))
	
	
	
	def op_unary_pre(op):
		return [("op",op,"pre"),(op,match_expression("x")),{"no newline","no preceding expression"}]
	
	
	
	def ops_unary_pre(ops,extra = []):
		statements = []
		for op in ops:
			statements += [op_unary_pre(op)]
		
		return ["rtl",statements + extra]
	
	
	
	def op_unary_post(op):
		return [("op",op,"post"),(match_expression("x"),op),{"no newline","no succeeding expression"}]
	
	
	
	def ops_unary_post(ops,extra = []):
		statements = []
		for op in ops:
			statements += [op_unary_post(op)]
		
		return ["ltr",statements + extra]
	
	
	
	def op_binary(op):
		return [("op",op,"binary"),(match_expression("x"),op,match_expression("x"))]
	
	
	
	def ops_binary(ops,extra = []):
		statements = []
		for op in ops:
			statements += [op_binary(op)]
		
		return ["ltr",statements + extra]
	
	
	
	def in_1(ins):
		return [(ins,),(ins,match_expression("x"))]
	
	
	
	def in_01(ins):
		return [(ins,),(ins,("?",(match_expression("x"),))),{"no newline"}]
	
	
	
	def java_declaration(visibilities,modifiers,types):
		statements = []
		
		pattern1 = []
		pattern1 += [("*",(("k",visibilities,"visibilities"),))]
		pattern1 += [("*",(("k",modifiers,"modifiers"),))]
		
		# variable
		
		pattern2 = []
		pattern2 += pattern1
		pattern2 += [("|",((("k",types,"type"),),(match_statement(("identifier",),"type"),)))]
		pattern2 += [match_list(("p",(("|",((match_statement(("statement","expression","op","="),"x"),),(match_statement(("identifier",),"x"),))),),None),None)]
		
		statements += [[("declaration","variable"),tuple(pattern2),{"no lookahead"}]]
		
		# method
		
		pattern2 = []
		pattern2 += pattern1
		pattern2 += [("?",(("|",((("k",types,"type"),),(match_statement(("identifier",),"type"),))),))]
		pattern2 += [match_statement(("statement","expression","("),"signature"),("?",("throws",match_list(("p",(match_statement(("identifier",),"throws"),),None),None))),match_statement(("brackets","{"),"{")]
		
		statements += [[("declaration","method"),tuple(pattern2),{"no lookahead"}]]
		
		# class
		
		pattern2 = []
		pattern2 += pattern1
		pattern2 += ["class",match_statement(("identifier",),"identifier"),("?",(match_statement(("brackets","<"),"generic"),)),("?",("extends",match_list(("p",(match_statement(("identifier",),"extends"),),None),None))),("?",("implements",match_list(("p",(match_statement(("identifier",),"implements"),),None),None))),match_statement(("brackets","{"),"{")]
		
		statements += [[("declaration","class"),tuple(pattern2)]]
		
		# interface
		
		pattern2 = []
		pattern2 += pattern1
		pattern2 += ["interface",match_statement(("identifier",),"identifier"),("?",("extends",match_list(("p",(match_statement(("identifier",),"extends"),),None),None))),match_statement(("brackets","{"),"{")]
		
		statements += [[("declaration","interface"),tuple(pattern2)]]
		
		# enum
		
		pattern2 = []
		pattern2 += pattern1
		pattern2 += ["enum",match_statement(("identifier",),"identifier"),match_statement(("brackets","{"),"{")]
		
		statements += [[("declaration","enum"),tuple(pattern2)]]
		
		return ["ltr",statements]
	
	
	
	def parse(ctx,rewrite = False):
		global statement_names
		
		
		#ctx["text"]
		ctx["ast"] = None
		ctx["pars"] = [None]
		ctx["nodes"] = []
		ctx["types"] = {}
		ctx["parents"] = {}
		ctx["tplparams"] = {}
		ctx["positions"] = {}
		ctx["lines"] = None
		
		
		ast = ctx["text"]
		ast = re.split("([\\W\\d])",ast)
		ast = [x for x in ast if x]
		
		
		# reduce comments
		# reduce string literals (count escape)
		
		
		ast2 = []
		i = 0
		while i < len(ast):
			x = ast[i]
			
			if (\
				x == "#"\
			): # single line comment
			#or (\
			#	x == "/"\
			#	and i < len(ast) - 1\
			#	and ast[i + 1] == "/"\
			#):
				
				j = i + 1
				while j < len(ast):
					y = ast[j]
					
					if y == "\n":
						break
					
					j += 1
				
				ast2 += [[("comment",),["".join(ast[i:j])]]] # type, tokens
				i = j
			elif x == "/"\
			and i < len(ast) - 1\
			and ast[i + 1] == "*": # multi line comment
				
				j = i + 1
				while j < len(ast):
					y = ast[j]
					
					if y == "*"\
					and j < len(ast) - 1\
					and ast[j + 1] == "/":
						break
					
					j += 1
				
				ast2 += [[("comment",),["".join(ast[i:j + 2])]]] # type, tokens
				i = j + 2
			elif x == "\""\
			or x == "'": # string literal
				
				j = i + 1
				while j < len(ast):
					y = ast[j]
					
					if y == x:
						break
					
					if y == "\\":
						j += 1
					
					j += 1
				
				ast2 += [[("literal","string"),["".join(ast[i:j + 1])]]] # type, tokens
				i = j + 1
			#elif x == "\\"\
			#and i < len(ast) - 1\
			#and ast[i + 1] == "\n": # escape before newline
			#	i += 1
			else:
				ast2 += [x]
				i += 1
		
		ast = ast2
		
		
		# reduce whitespace
		# read escape character as whitespace
		
		
		ast2 = []
		i = 0
		while i < len(ast):
			x = ast[i]
			
			if not isinstance(x,list)\
			and re.match("[\\s\\\\]",x):
				
				j = i + 1
				while j < len(ast):
					y = ast[j]
					
					if isinstance(y,list)\
					or not re.match("[\\s\\\\]",y):
						break
					
					j += 1
				
				ast2 += [[("whitespace",),["".join(ast[i:j])],0]] # type, tokens, relative-indentation
				i = j
			else:
				ast2 += [x]
				i += 1
		
		ast = ast2
		
		
		# reduce number literals
		# reduce identifiers with digits
		
		
		ast2 = []
		i = 0
		while i < len(ast):
			x = ast[i]
			
			if not isinstance(x,list)\
			and re.match("[\\w\\.]",x,re.UNICODE):
				dots = [i - 1]
				is_number = False
				is_exp = False ## TODO NIY
				
				j = i
				k = 0
				while j < len(ast):
					y = ast[j]
					
					if isinstance(y,list)\
					or not re.match("[\\w\\.]",y,re.UNICODE):
						break
					
					if y == ".":
						dots += [j]
						k = -1
					elif not k\
					and not isinstance(y,list)\
					and re.match("\\d",y):
						is_number = True
					
					j += 1
					k += 1
				
				dots += [j]
				
				if is_number:
					ast2 += [[("literal","number"),["".join(ast[i:j])]]] # type, tokens
				else:
					for k in xrange(len(dots) - 1):
						if dots[k] + 1 < dots[k + 1]:
							ast2 += ["".join(ast[dots[k] + 1:dots[k + 1]])]
						
						ast2 += ["."]
					
					ast2.pop()
				
				i = j
			else:
				ast2 += [x]
				i += 1
		
		ast = ast2
		
		
		# reduce string literal prefixes
		
		
		ast2 = []
		i = 0
		while i < len(ast):
			x = ast[i]
			
			if not isinstance(x,list)\
			and re.match("\\w",x,re.UNICODE)\
			and i < len(ast) - 1\
			and isinstance(ast[i + 1],list)\
			and ast[i + 1][0][:2] == ("literal","string"):
				ast2 += [[ast[i + 1][0],[x + ast[i + 1][1][0]]]] # type, tokens
				i += 2
			else:
				ast2 += [x]
				i += 1
		
		ast = ast2
		
		
		# reduce operators (ltr)
		
		
		op_chrs2 = {
			"+": {"=","+"},
			"-": {"=","-",">"},
			"*": {"=","*"},
			"/": {"=","/"},
			"<": {"=","<"},
			">": {"=",">"},
			"&": {"=","&"},
			"|": {"=","|"},
			"!": {"="},
			"=": {"="},
			":": {"=",":"},
			"@": {"="},
			"%": {"="},
			"^": {"="},
		}
		op_chrs3 = {
			">": {
				">": {"=",">"},
			},
			"*": {
				"*": {"="},
			},
			"/": {
				"/": {"="},
			},
			"<": {
				"<": {"="},
			},
			"=": {
				"=": {"="},
			},
			".": {
				".": {"."},
			},
		}
		op_chrs4 = {
			">": {
				">": {
					">": {"="},
				},
			},
		}
		
		
		ast2 = []
		i = 0
		while i < len(ast):
			x = ast[i]
			
			if i < len(ast) - 3\
			and not isinstance(x,list)\
			and not isinstance(ast[i + 1],list)\
			and not isinstance(ast[i + 2],list)\
			and not isinstance(ast[i + 3],list)\
			and x in op_chrs4\
			and ast[i + 1] in op_chrs4[x]\
			and ast[i + 2] in op_chrs4[x][ast[i + 1]]\
			and ast[i + 3] in op_chrs4[x][ast[i + 1]][ast[i + 2]]:
				
				ast2 += ["".join(ast[i:i + 4])]
				i += 4
			elif i < len(ast) - 2\
			and not isinstance(x,list)\
			and not isinstance(ast[i + 1],list)\
			and not isinstance(ast[i + 2],list)\
			and x in op_chrs3\
			and ast[i + 1] in op_chrs3[x]\
			and ast[i + 2] in op_chrs3[x][ast[i + 1]]:
				
				ast2 += ["".join(ast[i:i + 3])]
				i += 3
			elif i < len(ast) - 1\
			and not isinstance(x,list)\
			and not isinstance(ast[i + 1],list)\
			and x in op_chrs2\
			and ast[i + 1] in op_chrs2[x]:
				
				ast2 += ["".join(ast[i:i + 2])]
				i += 2
			else:
				ast2 += [x]
				i += 1
		
		ast = ast2
		
		
		# reduce identifiers and other literals
		# reduce multiword keywords
		
		
		lits = {
			"false","none","true",
			"false","null","true",
		}
		keywords = {
			"and","as","assert","async","await","break","class",
			"continue","def","del","elif","else","except",
			"finally","for","from","global","if","import","in",
			"is","lambda","nonlocal","not","or","pass",
			"raise","return","try","while","with","yield",
			"abstract","assert","boolean","break","byte","case","catch","char","class","const",
			"continue","default","do","double","else","enum","extends","final","finally","float",
			"for","if","goto","implements","import","instanceof","int","interface","long","native",
			"new","package","private","protected","public","return","short","static","strictfp","super",
			"switch","synchronized","this","throw","throws","transient","try","void","volatile","while",
			"_","var","yield"
		}
		
		
		ast2 = []
		i = 0
		while i < len(ast):
			x = ast[i]
			
			if not isinstance(x,list)\
			and x.lower() in lits:
				ast2 += [[("literal","other"),[x]]] # type, tokens
				i += 1
			elif not isinstance(x,list)\
			and x.lower() in keywords:
				ast2 += [x]
				i += 1
			elif not isinstance(x,list)\
			and re.match("\\w",x,re.UNICODE):
				ast2 += [[("identifier",),[x],None]] # type, tokens, indentation
				i += 1
			else:
				ast2 += [x]
				i += 1
		
		ast = ast2
		
		
		keywords2 = {
			"not": {"in"},
			"is": {"not"},
		}
		
		
		ast2 = []
		i = 0
		while i < len(ast):
			x = ast[i]
			
			if i < len(ast) - 2\
			and not isinstance(x,list)\
			and isinstance(ast[i + 1],list)\
			and not isinstance(ast[i + 2],list)\
			and ast[i + 1][0][:1] == ("whitespace",)\
			and x.lower() in keywords2\
			and ast[i + 2].lower() in keywords2[x.lower()]:
				
				ast2 += [[("keyword"," ".join((x,ast[i + 2]))),ast[i:i + 3]]] # type, tokens
				i += 3
			else:
				ast2 += [x]
				i += 1
		
		ast = ast2
		
		
		# reduce matching brackets and indentation (ltr, top-down)
		
		
		i = 0
		curr_ind = 0
		while i < len(ast):
			x = ast[i]
			
			if isinstance(x,list)\
			and x[0][:1] == ("whitespace",)\
			and (not i or x[1][0].count("\n")):
				match = re.search("(\\t*)[^\\n\\t]*\\Z",x[1][0])
				ind = len(match.group(1))
				
				if ind - curr_ind:
					ast[i][2] = ind - curr_ind
				
				curr_ind = ind
			
			i += 1
		
		
		par_chrs = {
			"<": ">",
			"{": "}",
			"(": ")",
			"[": "]",
		}
		
		
		def reduce_pars(ast,ctx_pars):
			
			ast2 = []
			i = 0
			while i < len(ast):
				x = ast[i]
				
				if x == "<": # TODO  f(g<a,b>(c)) vs f((g<a),(b>c))
					
					j = i + 1
					depth = 1
					while j < len(ast):
						y = ast[j]
						
						if y == x:
							depth += 1
						elif y == par_chrs[x]:
							depth -= 1
						elif y != ","\
						and not isinstance(y,list)\
						and y[0][:1] != ("whitespace",)\
						and y[0][:1] != ("identifier",):
							break
						
						j += 1
						
						if depth <= 0:
							break
					
					if depth <= 0:
						ast3 = [("brackets",x),[ast[i]] + reduce_pars(ast[i + 1:j],ctx_pars)] # type, tokens
						ctx_pars += [ast3]
						ast2 += [ast3]
						i = j
					else:
						ast2 += [x]
						i += 1
				elif not isinstance(x,list)\
				and x in par_chrs:
					
					j = i + 1
					depth = 1
					while j < len(ast):
						y = ast[j]
						
						if y == x:
							depth += 1
						elif y == par_chrs[x]:
							depth -= 1
						
						j += 1
						
						if depth <= 0:
							break
					
					ast3 = [("brackets",x),[ast[i]] + reduce_pars(ast[i + 1:j],ctx_pars)] # type, tokens
					ctx_pars += [ast3]
					ast2 += [ast3]
					i = j
				elif isinstance(x,list)\
				and x[0][:1] == ("whitespace",)\
				and x[2] > 0:
					
					j = i + 1
					depth = x[2]
					while j < len(ast):
						y = ast[j]
						
						if isinstance(y,list)\
						and y[0][:1] == ("whitespace",):
							depth += y[2]
						
						j += 1
						
						if depth <= 0:
							break
					
					ast3 = [("brackets","indentation"),[ast[i]] + reduce_pars(ast[i + 1:j],ctx_pars)] # type, tokens
					ctx_pars += [ast3]
					ast2 += [ast3]
					i = j
				else:
					ast2 += [x]
					i += 1
			
			return ast2
		
		
		ast = reduce_pars(ast,ctx["pars"])
		ctx["pars"][0] = [("brackets","{"),ast] # type, tokens
		
		
		# reduce statements (ltr and rtl, with precedence, bottom-up)
		
		
		# TODO performance: keyword look ahead before statement -> match_pattern()
		# TODO use "statement in no_keyword or statement in next_keyword"
		
		
		statementsss = [
			["expression",[
				["ltr",[
					[("[",),(match_expression("x"),match_statement(("brackets","["),"x")),{"no lookahead"}],
					[("(",),(match_expression("x"),match_statement(("brackets","("),"x")),{"no lookahead"}], # TODO (a)(b)c
					[(".",),(match_expression("x"),".",match_statement(("identifier",),"identifier"))],
				]],
				ops_unary_pre([
					"await",
				]),
				ops_unary_post([
					"++","--", # special case
				]),
				ops_unary_pre([
					"++","--",
				]),
				ops_binary([
					"**",
				]),
				ops_unary_pre([
					"+","-", #TODO TEST
					"~","!",
					"new",
				],[
					[("cast",),(match_statement(("brackets","("),"type"),match_expression("x")),{"no newline","no lookahead"}], # special case
				]),
				ops_binary([
					"*","@","/","//","%",
				]),
				ops_binary([
					"+","-",
				]),
				ops_binary([
					"<<",">>",">>>",
				]),
				ops_binary([ #TODO TEST
					#"<","<=",">",">=",
					#"instanceof",
				]),
				ops_binary([ #TODO TEST
					"!=","==",
				]),
				ops_binary([
					"&",
				]),
				ops_binary([
					"^",
				]),
				ops_binary([
					"|",
				]),
				ops_binary([
					"in","not in","is","is not",
					"<","<=",">",">=","!=","==",
					"instanceof",
				]),
				ops_unary_pre([
					"not",
				]),
				ops_binary([
					"and",
					"&&",
				]),
				ops_binary([
					"or",
					"||",
				]),
				["ltr",[
					[("lambda","py"),("lambda",match_expression("signature"),":",match_expression("x"))],
				]],
				["rtl",[
					[("if","java"),(match_expression("if"),"?",match_expression("x"),":",match_expression("x"))],
				]],
				["ltr",[
					[("if","py"),(match_expression("x"),("+",("if",match_expression("if"),"else",match_expression("x")))),{"no newline"}], # special case
				]],
				["ltr",[
					[("for",),(match_expression("x"),("+",(("p",("for",match_statement(("statement","expression","op","in"),"x"),("*",("if",match_expression("if")))),"for"),))),{"no newline"}], # special case
				]],
				ops_binary([
					":=",
				]),
				ops_binary([
					"=",
					"**=",
					"*=","@=","/=","//=","%=",
					"+=","-=",
					"<<=",">>=",">>>=",
					"&=",
					"^=",
					"|=",
				]),
				["ltr",[
					[("lambda","java"),(match_expression("signature"),"->",match_expression("x"))],
				]],
			]],
			["control",[
				["ltr",[
					[("import",),(("?",("from",match_expression("from"))),"import",("|",((match_list(("p",(match_expression("x"),("?",("as",match_expression("as")))),"x"),None),),("*",))))],
				]],
				["rtl",[
					# as
					[("assert",),("assert",match_list(("p",(match_expression("x"),("?",(":",match_expression(":")))),"x"),None))],
					in_1("async"),
					in_01("break"), # special case
					[("declaration","class"),("class",match_expression("x"),":",match_expression(":"))],
					in_01("continue"), # special case
					[("declaration","method"),("def",match_statement(("statement","expression","("),"signature"),("?",("->",match_expression("->"))),":",match_expression(":"))],
					in_1("del"),
					# elif
					# else
					# except
					# finally
					[("for","py"),("for",match_statement(("statement","expression","op","in"),"x"),":",match_expression("do"),("?",("else",":",match_expression("do"))))],
					[("global",),("global",match_list(("p",(match_expression("x"),),None),None))],
					[("if","py"),("if",match_expression("x"),":",match_expression("do"),("*",("elif",match_expression("x"),":",match_expression("do"))),("?",("else",":",match_expression("do"))))],
					[("nonlocal",),("nonlocal",match_list(("p",(match_expression("x"),),None),None))],
					[("pass",),("pass",)],
					[("raise",),("raise",match_expression("x"),("?",("from",match_expression("from"))))],
					in_01("return"), # special case
					[("try","py"),("try",":",match_expression("do"),("*",(("p",("except",("?",(match_expression("x"),("?",("as",match_expression("as"))))),":",match_expression("do")),"except"),)),("?",("else",":",match_expression("else"))),("?",("finally",":",match_expression("finally"))))],
					[("while","py"),("while",match_expression("x"),":",match_expression("do"),("?",("else",":",match_expression("do"))))],
					[("with",),("with",match_list(("p",(match_expression("x"),("?",("as",match_expression("as")))),"x"),None),":",match_expression("do"))],
					in_1("yield"),
					[("decorated",),(("+",(match_statement(("decorator",),"decorator"),)),match_expression("x")),{"no lookahead"}],
					#[("labeled",),(match_statement(("identifier",),"identifier"),":",match_expression(":"))],
					
					
					# assert
					# break
					#[("case",),("case",match_list(("p",(match_expression("x"),),None),None),":",match_expression("do"))], #TODO
					#[("case","default"),("default",":",match_expression("do"))], #TODO
					# catch
					# class
					# continue
					# else
					# enum
					# finally
					[("for","java"),("for",match_statement(("brackets","("),"x"),match_expression("do"))],
					in_1("goto"),
					[("if","java"),("if",match_statement(("brackets","("),"x"),match_expression("do"),("*",("elif",match_statement(("brackets","("),"x"),match_expression("do"))),("?",("else",match_expression("do"))))],
					# import
					# interface
					in_1("package"), #TODO
					# return
					# super
					[("switch",),("switch",match_statement(("brackets","("),"x"),match_statement(("brackets","{"),"do"))],
					[("synchronized",),("synchronized",match_statement(("brackets","("),"x"),match_statement(("brackets","{"),"do"))],
					# this
					in_1("throw"),
					#[("try","java"),("try",("?",(match_statement(("statement","structure","declaration","variable"),"x"),("*",(";",match_statement(("statement","structure","declaration","variable"),"x"))))),match_statement(("brackets","{"),"do"),("*",("catch",match_statement(("brackets","("),"x"),match_statement(("brackets","{"),"do"))),("?",("finally",match_statement(("brackets","{"),"do"))))], #TODO
					[("while","java"),("while",match_statement(("brackets","("),"x"),match_expression("do"))],
					[("while","do"),("do",match_expression("do"),"while",match_statement(("brackets","("),"x"))],
					# _
					# decorated
					# labeled
				]],
			]],
			["structure",[
				java_declaration(
				
				# extends
				# implements
				# throws
				
				{
					#"default",
					"private",
					"protected",
					"public",
				},
				{
					"abstract",
					"const",
					"final",
					"native",
					"static",
					"strictfp",
					"synchronized",
					"transient",
					"volatile",
				},
				{
					"boolean",
					"byte",
					"char",
					"double",
					"float",
					"int",
					"long",
					"short",
					"var",
					"void",
				}),
			]],
		]
		
		statementsss_properties = {
		}
		
		
		statement_names = {}
		
		for statementss in statementsss:
			for statements in statementss[1]:
				for statement in statements[1]:
					
					statement_names[statement[0]] = statement
					
					assert len(statement) >= 2
					if len(statement) <= 2:
						statement += [set()]
					statement += [set()]
					
					
					def traverse_recur(statement,pattern):
						
						for y in pattern:
							if isinstance(y,tuple):
								if y[0] in {"p","?","*","+"}:
									traverse_recur(statement,y[1])
								elif y[0] == "|":
									for z in y[1]:
										traverse_recur(statement,z)
								elif y[0] == "k":
									statement[3] |= y[1]
							else:
								statement[3] |= {y}
					
					traverse_recur(statement,statement[1])
		
		
		#print statement_names
		
		
		def accept_props(prop,prop2):
			for k in prop2:
				if k not in prop:
					prop[k] = []
				prop[k] += prop2[k]
			prop2.clear()
		
		
		def append_props(prop,prop2,k):
			if not k:
				accept_props(prop,prop2)
			else:
				if k not in prop:
					prop[k] = []
				prop[k] += [[("prop group",),None,prop2]] # type, None, props
				#prop2.clear()
		
		
		def match_pattern(ast,i,statement,pattern,prop):
			
			
			if "no preceding expression" in statement[2]:
				j = i - 1
				while j >= 0\
				and isinstance(ast[j],list)\
				and ast[j][0][0] in {"comment","whitespace"}: # no preceding expression: skip comment and whitespace
					
					if "no newline" in statement[2]\
					and ast[j][0][:1] == ("whitespace",)\
					and ast[j][1][0].count("\n")\
					and (ast[j][1][0][:1] == "\n" or re.search("[^\\\\]\n",ast[j][1][0])): # no preceding expression: do not skip newline
						break
					
					j -= 1
				
				if j >= 0\
				and isinstance(ast[j],list)\
				and ast[j][0][0] in {"statement","literal","identifier","brackets"}:
					return -1
			
			
			i_noskip = i
			j = 0
			while i < len(ast)\
			and j < len(pattern):
				
				x = ast[i]
				y = pattern[j]
				
				
				if "no newline" in statement[2]\
				and isinstance(x,list)\
				and x[0][:1] == ("whitespace",)\
				and x[1][0].count("\n")\
				and (x[1][0][:1] == "\n" or re.search("[^\\\\]\n",x[1][0])): # skip optional or reject
					break
				
				
				if isinstance(x,list)\
				and x[0][0] in {"comment","whitespace"}: # skip comment and whitespace
					i += 1
					continue
				
				
				if isinstance(x,list)\
				and x[0][:1] == ("keyword",):
					x = x[0][1]
				
				
				if isinstance(y,tuple):
					prop2 = {}
					
					if y[0] == "p":
						k = match_pattern(ast,i,statement,y[1],prop2)
						if k < 0:
							return -1
						i = k
						append_props(prop,prop2,y[2])
						i -= 1
					elif y[0] == "?":
						k = match_pattern(ast,i,statement,y[1],prop2)
						if k >= 0:
							i = k
							accept_props(prop,prop2)
						i -= 1
					elif y[0] == "|":
						k = 0
						for z in y[1]:
							k = match_pattern(ast,i,statement,z,prop2)
							if k >= 0:
								i = k
								accept_props(prop,prop2)
								break
						if k < 0:
							return -1
						i -= 1
					elif y[0] == "*":
						k = i
						while k >= 0:
							i = k
							accept_props(prop,prop2)
							k = match_pattern(ast,i,statement,y[1],prop2)
						i -= 1
					elif y[0] == "+":
						k = match_pattern(ast,i,statement,y[1],prop2)
						if k < 0:
							return -1
						while k >= 0:
							i = k
							accept_props(prop,prop2)
							k = match_pattern(ast,i,statement,y[1],prop2)
						i -= 1
					elif y[0] == "t":
						if not isinstance(x,list)\
						or y[1] != x[0][:len(y[1])]:
							return -1
						
						
						if "no newline" in statement[2]\
						and x[0][:2] == ("brackets","indentation"): # skip optional or reject
							break
						
						
						if y[2] not in prop:
							prop[y[2]] = []
						prop[y[2]] += [x]
					elif y[0] == "k":
						if isinstance(x,list)\
						or x.lower() not in y[1]:
							return -1
						
						if y[2]:
							if y[2] not in prop:
								prop[y[2]] = []
							prop[y[2]] += [x]
				
				elif not isinstance(x,list)\
				and y == x.lower():
					pass
				else:
					return -1
				
				i += 1
				j += 1
				i_noskip = i
			
			
			while j < len(pattern): # skip optional or reject
				
				y = pattern[j]
				
				if isinstance(y,tuple)\
				and y[0] in {"?","*"}:
					j += 1
				else:
					return -1
			
			
			if "no succeeding expression" in statement[2]:
				j = i
				while j < len(ast)\
				and isinstance(ast[j],list)\
				and ast[j][0][0] in {"comment","whitespace"}: # no succeeding expression: skip comment and whitespace
					
					if "no newline" in statement[2]\
					and ast[j][0][:1] == ("whitespace",)\
					and ast[j][1][0].count("\n")\
					and (ast[j][1][0][:1] == "\n" or re.search("[^\\\\]\n",ast[j][1][0])): # no succeeding expression: do not skip newline
						break
					
					j += 1
				
				if j < len(ast)\
				and isinstance(ast[j],list)\
				and ast[j][0][0] in {"statement","literal","identifier","brackets"}:
					return -1
			
			
			return i_noskip
		
		
		def keyword_lookahead(ast,i):
			if not isinstance(ast[i],list):
				return ast[i].lower()
			
			for j in xrange(2):
				while i < len(ast)\
				and isinstance(ast[i],list)\
				and ast[i][0][0] in {"comment","whitespace"}: # skip comment and whitespace
					i += 1
				
				if i == len(ast):
					break
				
				x = ast[i]
				
				if isinstance(x,list)\
				and x[0][:1] == ("keyword",):
					x = x[0][1]
				
				if not isinstance(x,list):
					return x.lower()
				
				i += 1
			
			return None
		
		
		extract_properties = []
		
		for ctx_par in ctx["pars"]: # time in O(ast*statements)
			
			for statementss in statementsss:
				
				for statements in statementss[1]:
					ast = ctx_par[1]
					
					if statements[0] == "ltr":
						
						i = 0
						while i < len(ast):
							
							if isinstance(ast[i],list)\
							and ast[i][0][0] in {"comment","whitespace"}: # skip comment and whitespace
								i += 1
								continue
							
							lookahead = keyword_lookahead(ast,i)
							#print lookahead
							
							j = -1
							for statement in statements[1]:
								if "no lookahead" not in statement[2]\
								and lookahead not in statement[3]:
									continue
								
								prop = {}
								j = match_pattern(ast,i,statement,statement[1],prop)
								
								if j > i:
									break
							
							if j > i:
								ast[i:j] = [[("statement",statementss[0]) + statement[0],ast[i:j],prop]] # type, tokens, props
							else:
								i += 1
					
					elif statements[0] == "rtl":
						
						i = len(ast) - 1
						while i >= 0:
							
							if isinstance(ast[i],list)\
							and ast[i][0][0] in {"comment","whitespace"}: # skip comment and whitespace
								i -= 1
								continue
							
							lookahead = keyword_lookahead(ast,i)
							#print lookahead
							
							j = -1
							for statement in statements[1]:
								if "no lookahead" not in statement[2]\
								and lookahead not in statement[3]:
									continue
								
								prop = {}
								j = match_pattern(ast,i,statement,statement[1],prop)
								
								if j > i:
									break
							
							if j > i:
								ast[i:j] = [[("statement",statementss[0]) + statement[0],ast[i:j],prop]] # type, tokens, props
							else:
								i -= 1
		
		
		# extract properties
		
		#TODO ...
		
		
		# create index dictionaries
		
		
		def update_indices():
			ctx["pars"][1:] = []
			ctx["nodes"][:] = []
			ctx["types"].clear()
			ctx["parents"].clear()
			
			
			def build_index(curr,path):
				nodes = None
				for x in path:
					if x not in curr:
						curr[x] = [{},[]]
					nodes = curr[x][1]
					curr = curr[x][0]
				
				return nodes
			
			
			indent = [0]
			def traverse_recur(ast,parent,ctx):
				
				for x in ast:
					if isinstance(x,list):
						
						if x[0][:1] == ("brackets",):
							ctx["pars"] += [x]
						
						ctx["nodes"] += [x]
						nodes = build_index(ctx["types"],x[0])
						nodes += [x]
						ctx["parents"][id(x)] = parent
						
						if x[0][:1] == ("identifier",):
							x[2] = indent[0]
						
						if x[0][:1] == ("whitespace",):
							indent[0] += x[2]
						
						traverse_recur(x[1],x,ctx)
			
			traverse_recur(ctx["pars"][0][1],ctx["pars"][0],ctx)
		
		update_indices()
		
		if not rewrite\
		and "identifier" in ctx["types"]:
			for x in ctx["types"]["identifier"][1]:
				if x[1][0] not in ctx["tplparams"]:
					ctx["tplparams"][x[1][0]] = []
				ctx["tplparams"][x[1][0]] += [x]
		
		#print ctx["nodes"]
		#print ctx["types"]["literal"]
		#print ctx["parents"]
		#print ctx["tplparams"]
		
		
		# rewrite ast
		
		
		if rewrite: #TODO
			
			
			if u"Umschreiben.choose" in state:
				if state[u"Umschreiben.choose"][0]: # Spezielle Formatierung anwenden
					pass
				
				if state[u"Umschreiben.choose"][1]: # Fehlertoleranz für bestimmte Konstrukte einfügen
					pass
				
				if state[u"Umschreiben.choose"][2]: # Domänenspezifische Befehle umsetzen
					pass
			
			
			if u"Konfigurieren.active" in state\
			and state[u"Konfigurieren.active"]\
			and u"Konfigurieren.choose" in state:
				if state[u"Konfigurieren.choose"][0]: # Fehlertoleranz für Datenzugriffe einfügen
					statements = tuple(("statement","expression",statement) for statement in ("[",".",))
					ast = tuple(
						x for statement in statements\
						for x in parse_query(ctx).type(statement).sel
					)
					for i,x in enumerate(ast):
						tpl = copy.deepcopy(tpls[{("statement","expression","["): "config 0-1",("statement","expression","."): "config 0-2"}[x[0][:3]]])
						tpl["parents"] = {id(y):y for y in tpl["parents"].values()}
						tpl["positions"] = {id(y):y for y in tpl["positions"].values()}
						for y in tpl["tplparams"]["a"]:
							y[:] = x
						for y in tpl["tplparams"]["b"]:
							y[:] = copy.deepcopy(x[2][0][0])
						for y in tpl["tplparams"]["c"]:
							ast = tpl["parents"][id(y)][1]
							index = next(i for i,z in enumerate(ast) if z is y)
							ast[index:index + 1] = copy.deepcopy(x[2][1][0][1])
						x[:] = tpl["ast"][0]
						
						update_indices()
				
				if state[u"Konfigurieren.choose"][1]: # Toleranz bei Vergleich von Gleitkommazahlen
					pass
				if state[u"Konfigurieren.choose"][2]: # Algorithmen unterbrechbar machen
					pass
			
			
			
			
			#parent = ctx["parents"][id(x)]
			#parent[1][index:index + 1] = ast2
			
			#for i in xrange(3):
			#	if u"Umschreiben.choose" in state\
			#	and state[u"Umschreiben.choose"][i]:
			#		tpl = copy.deepcopy(tpls["rewrite %d" % i])
			
			#for i in xrange(3):
			#	if u"Konfigurieren.choose" in state\
			#	and state[u"Konfigurieren.choose"][i]:
			#		tpl = copy.deepcopy(tpls["config %d" % i])
		
		
		# log ast
		
		
		def prop_name(props,x):
			for y in props:
				if id(x) in (id(z) for z in props[y]):
					return " $" + unicode(y)
				else:
					for z in props[y]:
						if isinstance(z,list)\
						and z[0][:1] == ("prop group",):
							path = prop_name(z[2],x)
							if path:
								return " $" + unicode(y) + path
			return ""
		
		
		def log_recur(ast,props = None,depth = 0):
			
			indent = "  " * depth
			for x in ast:
				
				name = ""
				if props:
					name = prop_name(props,x)
				
				if isinstance(x,list)\
				and x[0][:1] == ("brackets",):
					print indent + repr(x[0]) + name + ":"
					log_recur(x[1],None,depth + 1)
				elif isinstance(x,list)\
				and x[0][:1] == ("statement",):
					print indent + repr(x[0]) + name + ":"
					log_recur(x[1],x[2],depth + 1)
				else:
					print indent + repr(x) + name
		
		#log_recur(ctx["pars"][0][1])
		
		
		# return ast
		
		
		ctx["ast"] = ctx["pars"][0][1]
	
	
	
	def ast_descendants(ast,typee,ctx,ignore_types = None):
		
		nodes = parse_query(ctx).nodes([x for x in ast if isinstance(x,list)])
		descendants = nodes.descendants(include_original_selection = True)
		
		if ignore_types:
			for ignore_type in ignore_types:
				ignore_desc = descendants.type(ignore_type).descendants(include_original_selection = True)
				descendants = descendants.setMinus(ignore_desc)
		
		descendants = descendants.type(typee)
		
		return descendants.sel
	
	
	
	def ast_ancestor(x,types,ctx):
		
		path = parse_query(ctx).nodes([x]).ancestors(include_original_selection = True).sel
		
		if not types:
			return path
		
		for i,x in tuple(enumerate(path))[1:]:
			for typee in types:
				if x[0][:len(typee)] == typee:
					return path[:i + 1]
		
		return []
	
	
	
	def ast_isconform(tpl):
		pass
	
	
	
	def ast_substitute(tpl,subs): #TODO indent
		pass
	
	
	
	def ast_name(x,ctx):
		name = ""
		
		
		def prop_name(props,x):
			for y in props:
				if id(x) in (id(z) for z in props[y]):
					index = tuple(i for i,z in enumerate(props[y]) if z is x) or ("?",)
					return unicode(y) + "[" + unicode(index[0]) + "] "
				else:
					for z in props[y]:
						if isinstance(z,list)\
						and z[0][:1] == ("prop group",):
							path = prop_name(z[2],x)
							if path:
								index = tuple(i for i,zz in enumerate(props[y]) if zz is z) or ("?",)
								return unicode(y) + "[" + unicode(index[0]) + "] " + path
			return ""
		
		
		if id(x) in ctx["parents"]:
			parent = ctx["parents"][id(x)]
			
			name = ""
			if 2 < len(parent):
				name = prop_name(parent[2],x)
			
			else:
				if id(x) in (id(z) for z in parent[1]):
					ast = tuple(y for y in parent[1] if isinstance(y,list) and y[0][0] not in {"comment","whitespace","keyword"})
					index = tuple(i for i,z in enumerate(ast) if z is x) or ("?",)
					name += "[" + unicode(index[0]) + "] "
		
		name += "-".join(x[0])
		
		return name
	
	
	
	def printt(ctx):
		ast = ctx["ast"]
		
		
		# print ast, insert marks, track positions
		
		
		ast2 = []
		pos = [0,0]
		def print_recur(ast,ast2,formatting):
			
			for x in ast:
				if isinstance(x,list)\
				and x[0][:1] == ("mark",):
					ast2 += [x]
				elif isinstance(x,list)\
				and x[0][:1] == ("keyword",):
					pos_prev = tuple(pos)
					
					print_recur(x[1],ast2,formatting)
					
					ctx["positions"][id(x)] = (pos_prev,tuple(pos))
				elif isinstance(x,list):
					pos_prev = tuple(pos)
					
					if x[0][:3] == ("statement","expression","("):
						ast2 += [[("mark","text formatting",1),[],{"sc": fmt["call"][0]}]] # type, tokens, props
						print_recur(x[1],ast2,x[0])
						ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
					elif formatting[:2] == ("brackets","("):
						ast2 += [[("mark","text formatting",1),[],{"sc": fmt["default"][0]}]] # type, tokens, props
						print_recur(x[1],ast2,x[0])
						ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
					else:
						print_recur(x[1],ast2,x[0])
					
					ctx["positions"][id(x)] = (pos_prev,tuple(pos))
				else:
					if formatting[:1] == ("statement",):
						if not re.search("[^\\W\\d]",x):
							ast2 += [x]
						else:
							ast2 += [[("mark","text formatting",1),[],{"sc": fmt["keyword"][0],"sf": fmt["keyword"][1]}]] # type, tokens, props
							ast2 += [x]
							ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
					#elif formatting[:1] == ("identifier",):
					#	ast2 += [[("mark","text formatting",1),[],{"sc": fmt["identifier"][0]}]] # type, tokens, props
					#	ast2 += [x]
					#	ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
					elif formatting[:1] == ("literal",):
						ast2 += [[("mark","text formatting",1),[],{"sc": fmt["literal"][0]}]] # type, tokens, props
						ast2 += [x]
						ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
					elif formatting[:1] == ("comment",):
						ast2 += [[("mark","text formatting",1),[],{"sc": fmt["comment"][0]}]] # type, tokens, props
						ast2 += [x]
						ast2 += [[("mark","text formatting",-1),[]]] # type, tokens
					else:
						ast2 += [x]
					
					
					if x.count("\n"):
						pos[0] += x.count("\n")
						match = re.search("[^\\n]*\\Z",x)
						pos[1] = len(match.group(0))
					else:
						pos[1] += len(x)
		
		print_recur(ast,ast2,("brackets","{"))
		
		ast = ast2
		
		
		ast2 = [[u""]]
		i = 0
		while i < len(ast):
			x = ast[i]
			
			if not isinstance(x,list):
				
				j = i + 1
				while j < len(ast)\
				and not isinstance(ast[j],list):
					j += 1
				
				ast3 = "".join(ast[i:j])
				ast3 = ast3.split("\n")
				
				ast2[-1] += [ast3[0]]
				ast2 += [[x] for x in ast3[1:]]
				
				i = j
			else:
				ast2[-1] += [x]
				i += 1
		
		
		#if lines[ct] != ast2: raise Exception()
		ctx["lines"] = ast2
	
	
	
	## TODO:
	
	
	
	# reduce unary + and - with implicit parentheses
	# (NO!) later pull ** out of unary + and -
	# reduce different chained operators (eg. < <= ==)
	# reduce sign of number literals
	# reduce range access
	# reduce initializers: tuple, list and dictionary
	# reduce implicit tuple
	# expand/reduce for-in
	# short names for statements
	# assign comments to line (from right or top) or block (from top with space)
	# track inline-for props
	# rearrange inline-if props
	# expand/reduce from-import
	# expand/reduce type-parmeters < >
	
	
	
	# remember original tokens & formatting !
	# remember selection
	# (NO!) skip comments and whitespace ( token(n) )
	# ast, ctx_pars for compile, ctx_tokens for complete traversal, ctx_types for selecting
	# named children and parent for adjacent traversal
	
	# rewrite: sync ast,x[1],ctx_pars,ctx_tokens,ctx_types,ctx_parents; format x[1]
	# rewrite pre & post: track range -> caret diff, update on print
	
	
	
	# Fluid Graph Query Interface
	# 
	# 
	# 
	# Constructors:
	# 
	# I1: parse_query(ctx) (select all nodes)
	# 
	# I2: other.copy() (duplicate query object), other.empty() (without selection), other.init() (select all nodes)
	# 
	# I3: other.root() (select root node), other.nodes(nodes) (create selection of specific nodes)
	# 
	# 
	# Selectors:
	# 
	# S1: type(type) (grammar statement hierarchy, also matches prefixes, extended type hierarchy for documentation, recorded data, queries, templates, configurations, guidelines, schemata, models, recipes and additional data)
	# 
	# S2: clazz(clazz) (e.g. structure declaration, function declaration, variable declaration, parameter declaration, instruction, affects control flow, invokes code, loop, condition, exception handling, expression, accesses data, writes data, reads data, specific language)
	# 
	# S3: flag(flag) (analyzed features, behavior, purposes and functioning)
	# 
	# S4: group(group) (orthogonal grouping, variants)
	# 
	# 
	# Properties (Navigate and Filter):
	# 
	# P1: nodeProperties(properties = *,property_class = None) (navigate along named edges to child nodes; contains results of abstract analysis, e.g. control and data flow)
	# 
	# Property classes are e.g. identifier, other identifier, declaration, condition, data read, data write, control flow, property source (see below)
	# 
	# P2: hasNodeProperties(properties = *,property_class = None), hasDataProperties(#) -- (filter by occurrence of named edges)
	# 
	# P3: isNodePropertyOf(other,properties = *,property_class = None), reverseIsNodePropertyOf(#) (has property with) -- (filter by relations along named edges)
	# 
	# P4: dataPropertiesTextMatch(pattern,properties = *,property_class = None), dataPropertiesTextRegex(#) -- (filter by attribute values)
	# 
	# 
	# There are 2 types of properties and 4 sources of properties:
	# 
	# Type 1: Node Properties (child nodes)
	# 
	# Type 2: Data Properties (attributes)
	# 
	# Source 1: grammar (syntax tree)
	# 
	# Source 2: annotations (assigned comments and attributes assigned by the programmer)
	# 
	# Source 3: semantic (resolved type, variable and function identifiers, scopes of variables)
	# 
	# Source 4: abstract analysis (control and data flow, features, behavior, purposes and functioning)
	# 
	# 
	# Navigation and Selection: -- (N3, N4, N6, N7 syntax tree only, ignore results of abstract analysis)
	# 
	# N1: pick(order = Preorder,indices) (indices can be negative), slice(order = Preorder,index,count = +Infinity) (count can be negative) -- (select nodes within previous selection)
	# 
	# N2: next(order = Preorder,reverse = False,distance = 1,all_following = False,include_original_selection = False) (nodes within specific distance XOR all following nodes in direction) -- (applied simultaneously to all nodes in selection; result is not necessarily contained in previous selection)
	# 
	# N3: convenience aliases: parent(distance = 1), children(#), leftSibling(#), rightSibling(#), adjacentSibling(#) -- (0 to include original selection, negative for other direction)
	# 
	# N4: convenience aliases: ancestors(include_original_selection = False), descendants(#), leftSiblings(#), rightSiblings(#), allSiblings(#)
	# 
	# N5: search(predicate(i,x,y),order = Preorder,count = 1,start = None,search_in_other = None,abort_before_miss = False,abort_after_miss = False) (count can be +Infinity or negative) -- (result is not necessarily contained in previous selection)
	# 
	# N6: topNodes(), leftmostNode(), rightmostNode(), bottomNodes() -- (select nodes within previous selection)
	# 
	# N7: lowestCommonAncestor(), leftmostDescendant() (includes previous selection), rightmostDescendant(), bottomDescendants() -- (result is not necessarily contained in previous selection)
	# 
	# N8: indices(order = Preorder) (dictionary of node -> index) -- (does not return a selection)
	# 
	# 
	# Built-In Orders: -- (O1, O2 syntax tree only, ignore results of abstract analysis)
	# 
	# O1: Preorder(reverse = False,wrap_at_end = False), Postorder(#), PrintedTokens(#)
	# 
	# O2: Ancestor(reverse = False), Descendant(#), Sibling(#,wrap_at_end = False) -- (not suitable for pick() and slice())
	# 
	# O3: Incoming(reverse = False,depth_first = False), Outgoing(#) -- (graph search, consider results of abstract analysis; not suitable for pick() and slice())
	# 
	# 
	# Set Algebra, Filter and Query:
	# 
	# A1: unique() (remove duplicate entries in selection; necessary, if the selection was manually changed)
	# 
	# A2: setAnd(other,...) (intersection), setOr(#) (union), setXor(#) (only contained once), setMinus(#) (only contained in first), setNot() (not contained, all minus contained)
	# 
	# s1.and(s2)....and(sn) == parse_query.and(s1,s2,...,sn)
	# 
	# A3: filter(predicate(i,x)), map(mapping(i,x)) (mapping returns list of nodes)
	# 
	# Q1: contains(other,...), containsNotEqual(#), reverseContains(#) (is contained in), reverseContainsNotEqual(#), isEqualTo(#), isDisjointTo(#) -- (do not return a selection)
	# 
	# Q2: exists(predicate(i,x)), forall(#) -- (do not return a selection)
	# 
	# Q3: count() (number of nodes in selection) -- (does not return a selection)
	# 
	# 
	# Built-In Predicates:
	# 
	# B1: PredicateAnd(), PredicateOr(), PredicateXor(), PredicateImplication(), PredicateReverseImplication(), PredicateEquivalence(), PredicateNot()
	# 
	# B2: ContainedIn(other) (True, if x is contained in the other selection, e.g. if x satisfies the predicates of the other selection; useful for operations with a result, which is not necessarily contained in previous selection)
	# 
	# B3: HasContainedNodeProperties(other,properties = *,property_class = None) (True, if one of the specified properties of x is contained in the other selection)
	# 
	# B4: HasContainedNext(other,order = Preorder,distance = 1,all_following = False) (True, if one of the specified relatives of x is contained in the other selection)
	# 
	# B5: SameType() (True, if x has the same type as y)
	# 
	# B6: SameProperties(properties = *,property_class = None) (True, if the specified properties of x and y are equal)
	# 
	# 
	# Searching Constructs:
	# 
	# C1: occurrencesOfConstruct(specification) (matches nodes with the specified structure of descendants)
	# 
	# C2: occurrencesOfPattern(specification) (matches nodes, which contain the specified relations of descendants)
	# 
	# C3: occurrencesOfIdiom(specification) (matches nodes, which contain an implementation of the specified purpose at a small scale)
	# 
	# C4: occurrencesOfModel(specification) (matches groups of nodes, which implement the specified purpose at a larger scale)
	# 
	# 
	# Traversal and Reading the Result:
	# 
	# R1: foreach(iteration(i,x)) (alias for filter())
	# 
	# R2: result() (returns all nodes in selection as a list)
	# 
	# 
	# Utility Functions:
	# 
	# U1: parse_query.path(x,y) (get a path from x to y)
	# 
	# 
	
	
	
	callPredicate = lambda predicate,i,s,x,o,y: predicate({"i": i,"s": s,"x": x,"o": o,"y": y})
	
	class parse_query(object):
		
		
		# Constructors:
		
		
		def __init__(s,ctx = None,sel = None):
			s.ctx = ctx if ctx is not None else ctx1[ct]
			s.sel = sel if sel is not None else s.ctx["nodes"] # do not modify s.sel
		
		copy = lambda o: parse_query(ctx = o.ctx,sel = list(o.sel)) # allowed to modify s.sel
		empty = lambda o: parse_query(ctx = o.ctx,sel = [])
		init = lambda o: parse_query(ctx = o.ctx)
		root = lambda o: parse_query(ctx = o.ctx,sel = [o.ctx["root"]])
		nodes = lambda o,nodes: parse_query(ctx = o.ctx,sel = nodes)
		
		
		# Selectors:
		
		
		def type(s,typee):
			nodes = None
			curr = s.ctx["types"]
			for x in typee:
				if x not in curr:
					return s.empty()
				
				nodes = list(curr[x][1])
				curr = curr[x][0]
			
			if not nodes and not curr.values():
				return s.empty()
			
			open = curr.values()
			while len(open):
				curr = open.pop()
				nodes += curr[1]
				open += curr[0].values()
			
			return s.setAnd(s.nodes(nodes))
		
		def clazz(s,clazz):
			if clazz not in s.ctx["class"]:
					return s.empty()
			
			r = s
			for x in s.ctx["class"][clazz]:
				r = r.type(x)
			
			return r
		
		def flag(s,flag):
			if flag not in s.ctx["flags"]:
					return s.empty()
			
			return s.setAnd(s.nodes(s.ctx["flags"][flag]))
		
		def group(s,group):
			if group not in s.ctx["groups"]:
					return s.empty()
			
			return s.setAnd(s.nodes(s.ctx["groups"][group]))
		
		
		# Properties (Navigate and Filter):
		
		
		def isNode(s):
			r = s.empty()
			r.sel = list(x for x in s.sel if isinstance(x,list) and x[0][:1] != ("data",))
			
			return r
		
		def isData(s):
			r = s.empty()
			r.sel = list(x for x in s.sel if not isinstance(x,list) or x[0][:1] == ("data",))
			
			return r
		
		def nodeProperties(s,properties = None,property_class = None):
			r = s.empty()
			
			if properties:
				r.sel = list(y for x in s.sel for p in properties if p in x[2] for y in x[2][p])
			else:
				r.sel = list(y for x in s.sel for p in x[2] for y in x[2][p])
			
			return r.isNode().unique()
		
		hasNodeProperties = lambda s,o,*arg,**kwarg: s.filter(lambda arg1: s.nodes([arg1["x"]]).nodeProperties(*arg,**kwarg).isNode().count())
		hasDataProperties = lambda s,o,*arg,**kwarg: s.filter(lambda arg1: s.nodes([arg1["x"]]).nodeProperties(*arg,**kwarg).isData().count())
		isNodePropertyOf = lambda s,o,*arg,**kwarg: o.nodeProperties(*arg,**kwarg).setAnd(s)
		reverseIsNodePropertyOf = lambda s,o,*arg,**kwarg: s.nodeProperties(*arg,**kwarg).setAnd(o)
		
		#dataPropertiesTextMatch(pattern,properties = *,property_class = None)
		#dataPropertiesTextRegex(#)
		
		
		# Navigation and Selection:
		
		
		#pick(order = Preorder,indices)
		#slice(order = Preorder,index,count = +Infinity)
		
		def next(s,order = None,*arg,**kwarg):
			if order is None: order = Preorder
			
			return order.next(s,*arg,**kwarg)
		
		parent = lambda s,*arg,**kwarg: s.next(*arg,order = s.Ancestor(**kwarg),**kwarg)
		children = lambda s,*arg,**kwarg: s.next(*arg,order = s.Descendant(**kwarg),**kwarg)
		leftSibling = lambda s,*arg,**kwarg: s.next(*arg,order = s.Sibling(reverse = True,**kwarg),**kwarg)
		rightSibling = lambda s,*arg,**kwarg: s.next(*arg,order = s.Sibling(**kwarg),**kwarg)
		adjacentSibling = lambda s,*arg,**kwarg: s.leftSibling(*arg,**kwarg).setOr(s.rightSibling(*arg,**kwarg))
		
		ancestors = lambda s,*arg,**kwarg: s.parent(*arg,all_following = True,**kwarg)
		descendants = lambda s,*arg,**kwarg: s.children(*arg,all_following = True,**kwarg)
		leftSiblings = lambda s,*arg,**kwarg: s.leftSibling(*arg,all_following = True,**kwarg)
		rightSiblings = lambda s,*arg,**kwarg: s.rightSibling(*arg,all_following = True,**kwarg)
		allSiblings = lambda s,*arg,**kwarg: s.rightSibling(*arg,all_following = True,wrap_at_end = True,**kwarg)
		
		#search(predicate(i,x,y),order = Preorder,count = 1,start = None,search_in_other = None,abort_before_miss = False,abort_after_miss = False)
		
		#topNodes()
		#leftmostNode()
		#rightmostNode()
		#bottomNodes()
		
		lowestCommonAncestor = lambda s: s.ancestors(include_original_selection = True).search(lambda x: s.nodes([x]).children().setAnd(s).count() > 1,start = s.root())
		
		#leftmostDescendant()
		#rightmostDescendant()
		#bottomDescendants()
		
		#indices(order = Preorder)
		
		
		# Set Algebra, Filter and Query:
		
		
		def unique(s):
			r = s.empty()
			r_ids = set()
			r.sel = []
			for x in s.sel:
				if id(x) not in r_ids:
					r_ids |= {id(x)}
					r.sel += [x]
			
			return r
		
		def setAnd(s,o):
			if s.sel is o.sel or o.sel is o.ctx["nodes"]: return s
			if s.sel is s.ctx["nodes"]: return o
			if s.count() < o.count(): t = s; s = o; o = t # small set, large loop
			
			o_ids = set(id(x) for x in o.sel)
			r = s.empty()
			r.sel = list(x for x in s.sel if id(x) in o_ids)
			
			return r
		
		def setOr(s,o):
			if s.count() > o.count(): t = s; s = o; o = t # small set, large loop
			
			r = s.copy()
			r_ids = set(id(x) for x in r.sel)
			r.sel += list(x for x in o.sel if id(x) not in r_ids)
			
			return r
		
		setXor = lambda s,o: s.setOr(o).setMinus(s.setAnd(o))
		
		def setMinus(s,o):
			
			o_ids = set(id(x) for x in o.sel)
			r = s.empty()
			r.sel = list(x for x in s.sel if id(x) not in o_ids)
			
			return r
		
		setNot = lambda s: s.init().setMinus(s)
		
		def filter(s,predicate,abort_after_miss = False,abort_after_match = False):
			r = s.empty()
			
			i = 0
			for x in s.sel:
				if callPredicate(predicate,i,s,x,None,None):
					r.sel.append(x)
				
				if abort_after_miss and not r.count(): return r
				if abort_after_match and r.count(): return r
				
				i += 1
			
			return r
		
		def map(s,mapping):
			r = s.empty()
			
			i = 0
			for x in s.sel:
				r.sel += callPredicate(mapping,i,s,x,None,None)
				i += 1
			
			return r.unique() # also works with arbitrary content
		
		contains = lambda s,o: s.count() >= o.count() and not o.setMinus(s).count()
		containsNotEqual = lambda s,o: s.count() > o.count() and not s.contains(o)
		reverseContains = lambda s,o: s.count() <= o.count() and not s.setMinus(o).count()
		reverseContainsNotEqual = lambda s,o: s.count() < o.count() and not s.reverseContains(o)
		isEqualTo = lambda s,o: s.count() == o.count() and set(id(x) for x in s.sel) == set(id(x) for x in o.sel)
		isDisjointTo = lambda s,o: not set(id(x) for x in s.sel) & set(id(x) for x in o.sel)
		
		exists = lambda s,predicate: s.filter(predicate,abort_after_match = True).count()
		forall = lambda s,predicate: s.filter(predicate,abort_after_miss = True).count() == s.count()
		
		count = lambda s: len(s.sel)
		
		
		# Traversal and Reading the Result:
		
		
		foreach = lambda s,predicate: s.filter(predicate)
		result = lambda s: s.sel
		
		
		# Built-In Orders:
		
		
		class Preorder(object):
			
			def __init__(order,reverse = False,wrap_at_end = False,**kwarg):
				order.reverse = reverse
				order.wrap_at_end = wrap_at_end
		
		class Postorder(object):
			
			def __init__(order,reverse = False,wrap_at_end = False,**kwarg):
				order.reverse = reverse
				order.wrap_at_end = wrap_at_end
		
		class PrintedTokens(object):
			
			def __init__(order,reverse = False,wrap_at_end = False,**kwarg):
				order.reverse = reverse
				order.wrap_at_end = wrap_at_end
		
		class Descendant(object):
			
			def __init__(order,reverse = False,**kwarg):
				order.reverse = reverse
			
			def step(order,s,reverse,r,c):
				if reverse ^ order.reverse: # parent
					c.sel = list(s.ctx["parents"][id(x)] for x in c.sel if id(x) in s.ctx["parents"])
				else: # children
					c.sel = list(y for x in c.sel for y in x[1] if isinstance(y,list))
				r.sel += c.sel
				
				return c.sel
			
			def next(order,s,reverse = False,distance = 1,all_following = False,include_original_selection = False,**kwarg):
				r = s.copy() if include_original_selection else s.empty()
				c = s.copy()
				left = 1 if all_following else distance
				while left and order.step(s,reverse,r,c):
					if not all_following:
						left -= 1
				
				return r.unique()
		
		class Ancestor(Descendant):
			
			def __init__(order,reverse = False,**kwarg):
				super(parse_query.Ancestor,order).__init__(not reverse,**kwarg)
		
		class Sibling(object):
			
			def __init__(order,reverse = False,wrap_at_end = False,**kwarg):
				order.reverse = reverse
				order.wrap_at_end = wrap_at_end
			
			def oneAdjacent(order,s,reverse,distance,rsel,siblings,index):
				distance = max(len(siblings),distance)
				
				if reverse ^ order.reverse: # left
					start = index - distance
					end = index
					rsel += list(reversed(siblings[max(0,start):end]))
				else: # right
					start = index + 1
					end = index + 1 + distance
					rsel += siblings[start:end]
				
				if order.wrap_at_end:
					if start < 0:
						rsel += list(reversed(siblings[start:]))
					if end > len(siblings):
						rsel += siblings[:end - len(siblings)]
			
			def allFollowing(order,s,reverse,distance,rsel,siblings,index):
				if reverse ^ order.reverse: # left
					rsel += list(reversed(siblings[:index]))
					if order.wrap_at_end:
						rsel += list(reversed(siblings[index:]))
				else: # right
					rsel += siblings[index + 1:]
					if order.wrap_at_end:
						rsel += siblings[:index + 1]
			
			def next(order,s,reverse = False,distance = 1,all_following = False,include_original_selection = False,**kwarg):
				operation = order.allFollowing if all_following else order.oneAdjacent
				
				r = s.copy() if include_original_selection else s.empty()
				for x in s.sel:
					if x in s.ctx["parents"][id(x)]:
						siblings = tuple(y for y in s.ctx["parents"][id(x)][1] if isinstance(y,list) and y[0][0] not in {"comment","whitespace","keyword"})
						index = tuple(i for i,z in enumerate(siblings) if z is x) or ()
						
						if not index:
							continue
						
						operation(order,s,reverse,distance,r.sel,siblings,index[0])
				
				return r.unique()
		
		class Outgoing(object):
			
			def __init__(order,reverse = False,depth_first = False,**kwarg):
				order.reverse = reverse
				order.depth_first = depth_first
		
		class Incoming(Outgoing):
			
			def __init__(order,reverse = False,**kwarg):
				super(parse_query.Incoming,order).__init__(not reverse,**kwarg)
		
		
		# Built-In Predicates:
		
		
		#PredicateAnd()
		#PredicateOr()
		#PredicateXor()
		#PredicateImplication()
		#PredicateReverseImplication()
		#PredicateEquivalence()
		#PredicateNot()
		
		class ContainedIn(object):
			
			def __init__(predicate,o,**kwarg):
				predicate.o = o
		
		#HasContainedNodeProperties(o,properties = *,property_class = None)
		#HasContainedNext(o,order = Preorder,distance = 1,all_following = False)
		#SameType()
		#SameProperties(properties = *,property_class = None)
		
		
		# Utility Functions:
		
		
		def path(s,o):
			assert s.count() == 1
			assert o.count() == 1
			
			a1 = s.ancestors(include_original_selection = True)
			a2 = o.ancestors(include_original_selection = True)
			lca = a1.setAnd(a2).sel[0]
			
			path = a1.setMinus(a2).sel + [lca] + list(reversed(a2.setMinus(a1).sel))
			
			return path
	
	
	
	tpls = {
		"rewrite 0": {"text": u"a\n\tb\n\t\tc"},
		"rewrite 1": {"text": u"a\n\tb\n\t\tc"},
		"rewrite 2-1": {"text": u""},
		"rewrite 2-2": {"text": u""},
		"rewrite 2-3": {"text": u"geschäftslogik.add(regel(bedingung = lambda: a,signale = (b),aktion = aktion))"},
		"rewrite 2-4": {"text": u"def aktion(): c"},
		"config 0-1": {"text": u"((c in b if isinstance(b,dict) or isinstance(b,set) else len(b) > (c if c >= 0 else -c - 1)) and a)"},
		"config 0-2": {"text": u"hasattr(b,c) and a"},
		"config 1-1": {"text": u"(a) >= (b) - tolerance and (a) <= (b) + tolerance"},
		"config 1-2": {"text": u"(a) < (b) - tolerance or (a) > (b) + tolerance"},
		"config 1-3": {"text": u"(a) >= (b) - tolerance"},
		"config 1-4": {"text": u"(a) <= (b) + tolerance"},
		"config 2": {"text": u"if interrupt: break"},
	}
	for x in tpls:
		parse(tpls[x])
	
	
	
	pygame.init()
	pygame.mixer.quit()
	
	#cursor1 = pygame.cursors.load_xbm("cursor31.xbm","cursor12.xbm")
	#pygame.mouse.set_cursor(*cursor1)
	
	surf_tile1 = cairo.ImageSurface.create_from_png("tile1.png")
	surf_tile2 = cairo.ImageSurface.create_from_png("tile2.png")
	
	resize()
	
	pygame.scrap.init()
	pygame.display.set_caption(titlee)
	pygame.key.set_repeat(200,50)
	
	
	wx_app = wx.App()
	
	
	#print wx.MessageBox(u"Die Datei wurde von einem anderen Programm verändert. Möchten Sie die Datei neu laden?\n\nDadurch geht der aktuelle Tabinhalt verloren.",u"Datei neu laden",wx.YES_NO | wx.NO_DEFAULT | wx.ICON_WARNING)
	#print wx.MessageBox(u"Die Datei wurde von einem anderen Programm verändert. Möchten Sie die Datei überschreiben?\n\nDadurch gehen die Änderungen des anderen Programms verloren.",u"Datei überschreiben",wx.YES_NO | wx.NO_DEFAULT | wx.ICON_WARNING)
	##print wx.MessageBox(u"Die Datei zum Tab existiert nicht mehr. Möchten Sie die Datei an einem anderen Ort speichern?",u"Datei an einem anderen Ort speichern",wx.YES_NO | wx.NO_DEFAULT | wx.ICON_WARNING)
	#print wx.MessageBox(u"Die neu geöffnete Datei (B) ist identisch zur Datei (A), zuletzt bearbeitet am XX.XX.XXXX um XX:XX. Möchten Sie den Datei-Verlauf von (A) nach (B) verschieben?\n\n(A) /////(Datei existiert nicht mehr)\n\n(B) //////",u"Datei-Verlauf verschieben",wx.YES_NO | wx.YES_DEFAULT | wx.ICON_WARNING)
	wx.Frame(None).Destroy()
	
	
	
	
	
	def get_row(y): #TODO
		for i,cr in enumerate(caret_row):
			if y < cr[2] + cr[3]:
				cs = cards_row[i]
				return i - 1 if y < cr[2] + cs[0] // 2 else i
		
		return er - 1
	
	
	
	def get_button(pos,buttons):
		for button in buttons:
			if pos[0] >= button[0][0]\
			and pos[0] < button[0][1]\
			and pos[1] >= button[0][2]\
			and pos[1] < button[0][3]:
				return button
		
		return None
	
	
	
	def rearrange(i1,i2,l):
		tmp = l[i1]
		del l[i1]
		l[i2:i2] = [tmp]
	
	
	
	## TODO KMOD_CTRL: caret: expand to node; home/end: first/last line
	
	
	
	def validate_caret(i,rel = (0,0),forget = True):
		
		caret = selects[ct][i]
		
		if rel[0]: # vertical, restore column
			cl = caret_line[caret[0]]
			
			if caret[2] < 0:
				caret[2] = (caret[1] + min(ec,cl[1] * 8) - cl[1]) % ec
			
			j = (caret[1] + min(ec,cl[1] * 8) - cl[1]) // ec
			cc = caret[2]
			
			if rel[0] < 0\
			and cl[1] * 8 >= ec\
			and j == 1: # skip first row with nothing but indent
				j = 0
			
			cr = cl[0] + j + rel[0]
			cr = max(0,cr)
			cr = min(er - 1,cr)
			cr = caret_row[cr]
			
			caret[0] = cr[0]
			caret[1] = cr[4] * ec - min(ec,cr[1] * 8) + cr[1] + cc
			
			cl = caret_line[caret[0]]
			
			if rel[0] > 0\
			and cl[1] * 8 >= ec\
			and caret[1] < cl[1]: # skip first row with nothing but indent
				caret[1] += ec
		
		if rel[1]: # horizontal, wrap at ends
			cl = caret_line[caret[0]]
			
			caret[1] += rel[1]
			
			if caret[0] > 0 and caret[1] < cl[1]:
				caret[0] -= 1
				caret[1] = len("".join(get_text(lines[ct][caret[0]])))
			elif caret[0] < len(lines[ct]) - 1 and caret[1] > len("".join(get_text(lines[ct][caret[0]]))):
				caret[0] += 1
				caret[1] = 0
		
		# validate caret
		
		caret[0] = max(0,caret[0])
		caret[0] = min(len(lines[ct]) - 1,caret[0])
		
		cl = caret_line[caret[0]]
		
		caret[1] = max(cl[1],caret[1])
		caret[1] = min(len("".join(get_text(lines[ct][caret[0]]))),caret[1])
		
		if forget: # not vertical, forget column
			caret[2] = -1
	
	
	
	def expand_selection(rel = 0):
		
		return # TODO remove node expansion # TODO add token/line/paragraph expansion on multi-click
		
		if request_parse: return
		
		
		select = selects[ct]
		
		
		if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
			
			pos = [0,0]
			poss = [None,None]
			
			def traverse_recur(ast):
				
				for x in ast:
					pos_prev = tuple(pos)
					
					if isinstance(x,list):
						if traverse_recur(x[1]):
							return True
					else:
						
						if x.count("\n"):
							pos[0] += x.count("\n")
							match = re.search("[^\\n]*\\Z",x)
							pos[1] = len(match.group(0))
						else:
							pos[1] += len(x)
						
						if not poss[0]:
							if pos[0] > select[1][0]\
							or (pos[0] == select[1][0] and pos[1] > select[1][1]):
								poss[0] = pos_prev
						
						if not poss[1]:
							if pos[0] > select[1][0]\
							or (pos[0] == select[1][0] and pos[1] >= select[1][1]):
								poss[1] = tuple(pos)
						
						if poss[0] and poss[1]:
							return True
			
			traverse_recur(ctx1[ct]["ast"])
			
			if poss[0] and poss[1]:
				if rel < 0:
					select[1] = [poss[0][0],poss[0][1],poss[0][1]]
				
				elif rel > 0:
					select[1] = [poss[1][0],poss[1][1],poss[1][1]]
	
	
	
	
	
	def actionNew(arg):
		pass
	
	def actionOpen(arg):
		
		dialog = wx.FileDialog(None,u"Tab öffnen",style = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
		qtn = dialog.ShowModal() == wx.ID_OK
		wx.Frame(None).Destroy()
		if qtn:
			print dialog.GetPath()
	
	def actionSave(arg): # arg: {set_ct}
		
		if ("permanent" in files[ct][3] and files[ct][3]["permanent"]):
			return False
		
		dialog = wx.FileDialog(None,u"Tab öffnen",style = wx.FD_SAVE)
		qtn = dialog.ShowModal() == wx.ID_OK
		wx.Frame(None).Destroy()
		if qtn:
			print dialog.GetPath()
		
		qtn = wx.MessageBox(u"Die ausgewählte Datei existiert bereits. Möchten Sie die Datei überschreiben?\n\nDadurch geht der aktuelle Inhalt der Datei verloren.",u"Datei überschreiben",wx.YES_NO | wx.YES_DEFAULT | wx.ICON_WARNING) == wx.YES
		wx.Frame(None).Destroy()
		if not qtn:
			return False
	
	def actionSaveAll(arg):
		pass
	
	def actionClose(arg): # arg: {set_ct}
		global ct,files,titles,lines,selects,scrolls1,histories,ctx1,closed_tabs,closed_tabs_index
		
		if ("permanent" in files[ct][3] and files[ct][3]["permanent"]):
			return False
		
		qtn = wx.MessageBox(u"Der aktuelle Tabinhalt wurde noch nicht gespeichert. Möchten Sie die Datei vor dem Schließen speichern?",u"Datei speichern",wx.YES_NO | wx.YES_DEFAULT | wx.ICON_QUESTION) == wx.YES
		wx.Frame(None).Destroy()
		if qtn\
		and not actionSave(None):
			return
		
		tab = {
			"files": files[ct],
			"titles": titles[ct],
			"lines": lines[ct],
			"selects": selects[ct],
			"scrolls1": scrolls1[ct],
			"histories": histories[ct],
			"ctx1": ctx1[ct],
		}
		closed_tabs += [tab]
		closed_tabs_index[files[ct][2]] = tab;
		
		del files[ct]
		del titles[ct]
		del lines[ct]
		del selects[ct]
		del scrolls1[ct]
		del histories[ct]
		del ctx1[ct]
		
		ct = min(ct,len(titles) - 1)
		
		if not len(titles):
			actionNew(None)
		
		
		actionTab(ct)
	
	def actionRestore(arg):
		global ct,closed_tabs_index
		
		# TODO list of closed tabs -> restore, delete permanently
		
		if not closed_tabs:
			return
		
		tab = pop.closed_tabs()
		del closed_tabs_index[tab["files"][2]]
		
		files[ct + 1:ct + 1] = tab["files"]
		titles[ct + 1:ct + 1] = tab["titles"]
		lines[ct + 1:ct + 1] = tab["lines"]
		selects[ct + 1:ct + 1] = tab["selects"]
		scrolls1[ct + 1:ct + 1] = tab["scrolls1"]
		histories[ct + 1:ct + 1] = tab["histories"]
		ctx1[ct + 1:ct + 1] = tab["ctx1"]
		
		ct += 1
		
		
		actionTab(ct)
	
	def actionTitle(arg): # arg: {set_ct}
		global request_layout
		
		if ("permanent" in files[ct][3] and files[ct][3]["permanent"]):
			return
		
		title = wx.GetTextFromUser(u"Titel festlegen",default_value = titles[ct])
		wx.Frame(None).Destroy()
		
		print repr(title)
		if title:
			titles[ct] = title
		
		request_layout |= {0}
	
	def actionSettings(arg):
		pass
	
	# ---
	
	def actionUndo(arg):
		global request_parse,dirty_editor,scroll_to_caret,request_layout
		
		## TODO snapshot and patch
		
		
		request_parse = True
		dirty_editor = True
		
		scroll_to_caret = True
		
		request_layout |= {3}
	
	def actionRedo(arg):
		global request_parse,dirty_editor,scroll_to_caret,request_layout
		
		## TODO snapshot and patch
		
		
		request_parse = True
		dirty_editor = True
		
		scroll_to_caret = True
		
		request_layout |= {3}
	
	def actionCut(arg):
		
		actionCopy(True)
	
	def actionCopy(cut):
		global request_parse,dirty_editor,scroll_to_caret,request_layout
		
		if select[0][0] == select[1][0]:
			textt = "".join(get_text(lines[ct][select[select_i1][0]]))[select[select_i1][1]:select[select_i2][1]]
		else:
			textt = "\n".join(
				("".join(get_text(lines[ct][select[select_i1][0]]))[select[select_i1][1]:],) +\
				tuple("".join(get_text(line)) for line in lines[ct][select[select_i1][0] + 1:select[select_i2][0]]) +\
				("".join(get_text(lines[ct][select[select_i2][0]]))[:select[select_i2][1]],)\
			)
		
		cl = caret_line[select[select_i1][0]]
		
		textt = "\t" * cl[1] + textt
		
		pygame.scrap.put("text/plain;charset=utf-8",textt.encode("utf-8","ignore"))
		pygame.scrap.put("UTF8_STRING",textt.encode("utf-8","ignore"))
		
		if cut\
		and select[0] != select[1]: # 'x' cut, remove selection
			seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
			seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
			lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
				lines[ct][select[select_i1][0]][:seg1[0]] + [
					lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
					lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
				] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
			]
			select[select_i2] = list(select[select_i1])
			
			
			request_parse = True
			dirty_editor = True
		
		
		scroll_to_caret = True
		
		request_layout |= {3}
	
	def actionPaste(arg):
		global request_parse,dirty_editor,scroll_to_caret,request_layout
		
		if select[0] != select[1]: # remove selection
			seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
			seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
			lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
				lines[ct][select[select_i1][0]][:seg1[0]] + [
					lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
					lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
				] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
			]
			select[select_i2] = list(select[select_i1])
		
		textt = u""
		try: textt = pygame.scrap.get("text/plain;charset=utf-8").decode("utf-8","ignore")
		except: pass
		try: textt = pygame.scrap.get("UTF8_STRING").decode("utf-8","ignore")
		except: pass
		
		cl = caret_line[select[1][0]]
		
		textt = [[x] for x in textt.split("\n")]
		
		match = re.search("(\\t*)",textt[0][0])
		ind = len(match.group(1))
		ind = (textt[0][0][ind:],ind)
		
		textt[0] = [ind[0]]
		for i,x in enumerate(textt):
			if i:
				if cl[1] > ind[1]:
					x[0] = "\t" * (cl[1] - ind[1]) + x[0]
				elif ind[1] > cl[1]:
					for j in xrange(ind[1] - cl[1]):
						if x[0][:1] == "\t":
							x[0] = x[0][1:]
		pos = len(textt[-1][0])
		
		line = lines[ct][select[1][0]]
		
		seg1 = get_segment(line,select[1][1])
		textt[0] = line[:seg1[0]] + [line[seg1[0]][:seg1[1]]] + textt[0]
		textt[-1] = textt[-1] + [line[seg1[0]][seg1[1]:]] + line[seg1[0] + 1:]
		lines[ct][select[1][0]:select[1][0] + 1] = textt
		
		select[1] = list(select[select_i1])
		if len(textt) > 1:
			select[1][0] += len(textt) - 1
			select[1][1] = 0
		select[1][1] += pos
		
		select[1][2] = -1
		
		select[0] = list(select[1])
		
		
		request_parse = True
		dirty_editor = True
		
		scroll_to_caret = True
		
		request_layout |= {3}
	
	def actionSelectAll(arg):
		global request_layout
		
		cl = caret_line[0]
		
		select[:] = [[0,cl[1],-1],[len(lines[ct]) - 1,len("".join(get_text(lines[ct][-1]))),-1]]
		
		
		request_layout |= {3}
	
	# ---
	
	def actionTab(arg):
		global ct,scroll3,times1,request_parse,discard,dirty_editor,request_layout,request_save,scroll1_resize
		
		ct = arg
		scroll3 = 1.
		times1 = []
		
		
		request_parse = True
		discard = 2 # discard events until reparse
		dirty_editor = True
		
		request_layout |= {0}
		
		request_save = True
		
		scroll1_resize = True
	
	def actionAccordion(arg):
		global ca,timer_set,request_layout
		
		ca = arg
		anim_ctrs[4] = max(3,anim_ctrs[4]) # start animation
		
		pygame.time.set_timer(pygame.USEREVENT,0) # shift animation
		timer_set = False
		
		
		request_layout |= {4}
	
	
	
	clock1 = pygame.time.Clock()
	clock2 = pygame.time.Clock()
	times1 = []
	timer_set = False
	
	scroll_to_caret = False # TODO
	
	request_parse = True
	request_resize = False
	request_layout = {0}
	request_save = False
	discard = 2
	def evtloop():
		global width,height,ct,scroll1_resize,ca,scroll2,scroll3,mousedown1,rearrange1,rearrange2,rearrange_pos1,rearrange_pos2,menu_actions,menu_pos1,menu_pos2,menu_mousedown,anim_tick
		global times1,timer_set,scroll_to_caret,request_parse,request_resize,request_layout,request_save,discard
		
		
		clock1.tick()
		
		
		if files[ct][1]:
			pass # TODO check external modification
		
		
		if request_parse:
			request_parse = False
			
			if not ("no parse" in files[ct][3] and files[ct][3]["no parse"]):
				
				ast_selected1[1] = None
				
				ctx1[ct] = {
					"text": "\n".join("".join(get_text(line)) for line in lines[ct]),
					"highlighted": False,
					"minimap arranged": False,
				}
				parse(ctx1[ct],rewrite = True)
				printt(ctx1[ct])
			
			validate_minimap()
		
		
		if request_resize:
			request_resize = False
			
			resize()
		
		
		if max(anim_ctrs) and not timer_set: # start or shift animation
			pygame.time.set_timer(pygame.USEREVENT,50) # 20 fps
			timer_set = True
		
		
		if request_layout:
			if max(anim_ctrs) and anim_tick < 0: # start animation
				clock2.tick()
				anim_tick = 50 # 20 fps
			elif anim_tick >= 0: # continue animation
				anim_tick = clock2.tick()
			
			layout(request_layout)
			request_layout = set()
			
			scroll_to_caret = False
			
			validate_caret(0,forget = False)
			validate_caret(1,forget = False)
			
			
			if request_save:
				request_save = False
				
				save_tabs()
			else:
				pygame.time.set_timer(pygame.USEREVENT + 1,1000) # 1 second
			
			save_state() # TODO save_history
			
			
			time2 = clock1.tick()
			
			time1prev = max(times1) if times1 else 0
			while len(times1) > 1 and sum(times1) > 3000:
				times1 = times1[1:]
			times1 += [time2]
			times1 = times1[-10:]
			time1curr = max(times1) if times1 else 0
			
			if time1curr != time1prev:
				pygame.display.set_caption("%s [%d]" % (titlee,time1curr))
		
		
		if not max(anim_ctrs): # stop animation
			anim_tick = -1
		
		
		dirty_editor = False
		
		events = pygame.event.get()
		if not events:
			discard = 0
			return
		elif discard: discard -= 1
		for e in events:
			
			
			buttons1_flat = tuple(y for x in buttons1 for y in x)
			
			
			if discard:
				if e.type == pygame.KEYDOWN:
					continue
				
				if e.type == pygame.MOUSEBUTTONDOWN:
					mousedown = get_button(e.pos,buttons1_flat)
					
					if e.button == 1:
						if mousedown\
						and mousedown[1][0] == actionTab:
							pass
						else:
							continue
					elif e.button in {4,5}:
						if get_button(e.pos,[((xs[1],xs[6],ys[1],ys[2]),)]):
							pass
						else:
							continue
					else:
						continue
				
				if e.type == pygame.MOUSEMOTION:
					continue
				
				if e.type == pygame.MOUSEBUTTONUP:
					mousedown = get_button(e.pos,buttons1_flat)
					
					if e.button == 1:
						if mousedown\
						and mousedown[1][0] == actionTab:
							pass
						else:
							continue
					else:
						continue
			
			
			if dirty_editor:
				dirty_editor = False
				
				validate_editor1()
				validate_editor2()
				validate_editor3()
			
			
			select = selects[ct]
			
			select_i1 = 0
			if select[0][0] > select[1][0]\
			or (\
				select[0][0] == select[1][0]\
				and select[0][1] > select[1][1]\
			):
				select_i1 = 1
			select_i2 = (select_i1 + 1) % 2
			
			
			if e.type == pygame.QUIT:
				exit()
			
			if e.type == pygame.KEYDOWN:
				print "KD",e.key,pygame.key.get_mods(),repr(e.unicode)
				
				if e.key == 27: # escape, quit
					exit()
				
				elif e.key == 273: # up
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT)\
					and select[0] != select[1]:
						select[:] = [select[select_i2],select[select_i1]]
					
					validate_caret(1,(-1,0),forget = False)
					
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
						select[0] = list(select[1])
					
					
					ast_selected1[1] = None
					
					scroll_to_caret = True
				
				elif e.key == 274: # down
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT)\
					and select[0] != select[1]:
						select[:] = [select[select_i1],select[select_i2]]
					
					validate_caret(1,(1,0),forget = False)
					
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
						select[0] = list(select[1])
					
					
					ast_selected1[1] = None
					
					scroll_to_caret = True
				
				elif e.key == 275: # right
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT)\
					and select[0] != select[1]:
						select[:] = [select[select_i1],select[select_i2]]
					
					validate_caret(1,(0,1))
					
					if pygame.key.get_mods() & pygame.KMOD_CTRL:
						expand_selection(1)
					
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
						select[0] = list(select[1])
					
					
					ast_selected1[1] = None
					
					scroll_to_caret = True
				
				elif e.key == 276: # left
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT)\
					and select[0] != select[1]:
						select[:] = [select[select_i2],select[select_i1]]
					
					validate_caret(1,(0,-1))
					
					if pygame.key.get_mods() & pygame.KMOD_CTRL:
						expand_selection(-1)
					
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
						select[0] = list(select[1])
					
					
					ast_selected1[1] = None
					
					scroll_to_caret = True
				
				elif e.key == 278: # home
					
					if pygame.key.get_mods() & pygame.KMOD_CTRL:
						cl = caret_line[0]
						
						select[1] = [0,cl[1],-1]
					else:
						cl = caret_line[select[1][0]]
						select[1][1] += min(ec,cl[1] * 8) - cl[1]
						
						select[1][1] = ((select[1][1] - 1) // ec) * ec
						
						select[1][1] -= min(ec,cl[1] * 8) - cl[1]
					
					validate_caret(1)
					
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
						select[0] = list(select[1])
					
					
					ast_selected1[1] = None
					
					scroll_to_caret = True
				
				elif e.key == 279: # end
					
					if pygame.key.get_mods() & pygame.KMOD_CTRL:
						select[1] = [len(lines[ct]) - 1,len("".join(get_text(lines[ct][-1]))),-1]
					else:
						cl = caret_line[select[1][0]]
						select[1][1] += min(ec,cl[1] * 8) - cl[1]
						
						select[1][1] = (select[1][1] // ec + 1) * ec
						
						select[1][1] -= min(ec,cl[1] * 8) - cl[1]
					
					validate_caret(1)
					
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
						select[0] = list(select[1])
					
					
					ast_selected1[1] = None
					
					scroll_to_caret = True
				
				elif e.key == 8: # backspace
					
					if select[0] == select[1]:
						if select[1][1] > 0: # remove char on left side
							select[0][1] -= 1
						elif select[1][0] > 0: # join with prev line
							select[0][0] -= 1
							select[0][1] = len("".join(get_text(lines[ct][select[1][0] - 1])))
					
					if select[0] != select[1]: # remove selection
						seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
						seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
						lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
							lines[ct][select[select_i1][0]][:seg1[0]] + [
								lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
								lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
							] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
						]
						select[select_i2] = list(select[select_i1])
					
					select[1][2] = -1
					
					
					request_parse = True
					dirty_editor = True
					
					scroll_to_caret = True
				
				elif e.key == 127: # delete
					
					if select[0] == select[1]:
						if select[1][1] < len("".join(get_text(lines[ct][select[1][0]]))): # remove char on right side
							select[1][1] += 1
						elif select[1][0] < len(lines[ct]) - 1: # join with next line
							
							cl = caret_line[select[1][0] + 1]
							
							select[1][0] += 1
							select[1][1] = cl[1]
					
					if select[0] != select[1]: # remove selection
						seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
						seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
						lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
							lines[ct][select[select_i1][0]][:seg1[0]] + [
								lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
								lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
							] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
						]
						select[select_i2] = list(select[select_i1])
					
					select[1][2] = -1
					
					
					request_parse = True
					dirty_editor = True
					
					scroll_to_caret = True
				
				elif e.key in {13,271}: # enter, split line
					
					if select[0] == select[1]:
						cl = caret_line[select[1][0]]
						
						line = lines[ct][select[1][0]]
						
						seg1 = get_segment(line,select[1][1])
						lines[ct][select[1][0]:select[1][0] + 1] = [
							line[:seg1[0]] + [line[seg1[0]][:seg1[1]]],
							["\t" * cl[1] + line[seg1[0]][seg1[1]:]] + line[seg1[0] + 1:],
						]
						
						select[1][0] += 1
						select[1][1] = cl[1]
						
						select[1][2] = -1
						
						select[0] = list(select[1])
						
						
						request_parse = True
						dirty_editor = True
					
					
					scroll_to_caret = True
				
				elif e.key == 9: # tab
					
					if not (pygame.key.get_mods() & pygame.KMOD_SHIFT): # indent selection
						for i in xrange(select[select_i1][0],select[select_i2][0] + 1):
							if isinstance(lines[ct][i][0],list):
								lines[ct][i] = [u""] + lines[ct][i]
							lines[ct][i][0] = "\t" + lines[ct][i][0]
						
						select[0][1] += 1
						select[1][1] += 1
					else: # unindent selection
						for i in xrange(select[select_i1][0],select[select_i2][0] + 1):
							if not isinstance(lines[ct][i][0],list)\
							and lines[ct][i][0][:1] == "\t":
								lines[ct][i][0] = lines[ct][i][0][1:]
								
								if i == select[0][0]:
									select[0][1] -= 1
									select[0][1] = max(0,select[0][1])
								
								if i == select[1][0]:
									select[1][1] -= 1
									select[1][1] = max(0,select[1][1])
					
					select[1][2] = -1
					
					
					request_parse = True
					dirty_editor = True
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 110: # 'n' new tab
					
					actionNew(None)
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 111: # 'o' open tab
					
					actionOpen(None)
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 115: # 's' save
					
					if pygame.key.get_mods() & pygame.KMOD_SHIFT:
						actionSave(None)
					else:
						actionSaveAll(None)
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 119: # 'w' close tab
					
					actionClose(None)
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 122: # 'z' undo
					
					actionUndo(None)
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 121: # 'y' redo
					
					actionRedo(None)
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 120: # 'x' cut
					
					actionCut(None)
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 99: # 'c' copy
					
					actionCopy(None)
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 118: # 'v' paste
					
					actionPaste(None)
				
				elif pygame.key.get_mods() & pygame.KMOD_CTRL\
				and e.key == 97: # 'a' select all
					
					actionSelectAll(None)
				
				elif not (pygame.key.get_mods() & pygame.KMOD_CTRL)\
				and len(e.unicode): # printable, prepend char
					
					if select[0] != select[1]: # remove selection
						seg1 = get_segment(lines[ct][select[select_i1][0]],select[select_i1][1])
						seg2 = get_segment(lines[ct][select[select_i2][0]],select[select_i2][1])
						lines[ct][select[select_i1][0]:select[select_i2][0] + 1] = [
							lines[ct][select[select_i1][0]][:seg1[0]] + [
								lines[ct][select[select_i1][0]][seg1[0]][:seg1[1]] +
								lines[ct][select[select_i2][0]][seg2[0]][seg2[1]:]
							] + lines[ct][select[select_i2][0]][seg2[0] + 1:]
						]
						select[select_i2] = list(select[select_i1])
					
					line = lines[ct][select[1][0]]
					
					seg1 = get_segment(line,select[1][1])
					line[seg1[0]] = line[seg1[0]][:seg1[1]] + e.unicode + line[seg1[0]][seg1[1]:]
					
					select[1] = list(select[select_i1])
					select[1][1] += len(e.unicode)
					
					select[1][2] = -1
					
					select[0] = list(select[1])
					
					
					request_parse = True
					dirty_editor = True
					
					scroll_to_caret = True
				
				
				request_layout |= {3}
			
			if e.type == pygame.MOUSEBUTTONDOWN:
				#print "MD",e.button,e.pos
				if e.button == 1:
					mousedown1 = get_button(e.pos,buttons1_flat)
					
					if mousedown1\
					and mousedown1[1][0] == actionAccordion:
						mousedown1[1][0](mousedown1[1][1])
				
				elif e.button == 3:
					mousedown = get_button(e.pos,buttons3_1)
					if mousedown:
						menu_actions = mousedown[1]
						menu_pos1 = e.pos
						menu_mousedown = get_button(e.pos,buttons1_flat)
							
				
				elif e.button in {4,5}:
					if get_button(e.pos,[((xs[1],xs[6],ys[1],ys[2]),)]): # tabs
						
						actionTab((ct + [-1,1][e.button - 4]) % len(titles))
					
					elif get_button(e.pos,[((xs[1],xs[3],ys[5],ys[6]),)]): # editor
						scrolls1[ct][0] += [-100,100][e.button - 4]
						
						
						request_layout |= {3}
					
					elif get_button(e.pos,[((xs[3],xs[4],ys[5],ys[6]),)]): # minimap
						if pygame.key.get_mods() & pygame.KMOD_CTRL:
							h_screen = ys[6] - ys[5]
							h_doc = eh
							
							scroll3 *= [3./2,2./3][e.button - 4]
							scroll3 = min(float(h_doc) / h_screen,scroll3)
							scroll3 = max(1,scroll3)
						else:
							scrolls1[ct][0] += [-10,10][e.button - 4]
						
						
						request_layout |= {3}
					
					elif get_button(e.pos,[((xs[7],xs[8],ys[1],ys[6]),)]): # accordion
						scroll2[0] += [-100,100][e.button - 4]
						scroll2[1] = -1
						
						
						request_layout |= {4}
			
			if e.type == pygame.MOUSEBUTTONDOWN\
			or e.type == pygame.MOUSEMOTION:
				#print "MM",e.pos
				if mousedown1\
				and mousedown1[1][0] == "editor":
					y = e.pos[1] - ys[5] + scrolls1[ct][0]
					cr = get_row(y)
					cr = max(0,cr)
					cr = min(er - 1,cr)
					cr = caret_row[cr]
					
					cc = (e.pos[0] - xs[2] + f3[2][0] // 2) // f3[2][0]
					cc = max(0,cc)
					cc = min(ec,cc)
					
					select[1][0] = cr[0]
					select[1][1] = cr[4] * ec - min(ec,cr[1] * 8) + cr[1] + cc
					
					validate_caret(1)
					
					if e.type == pygame.MOUSEBUTTONDOWN\
					and e.button == 1\
					and not (pygame.key.get_mods() & pygame.KMOD_SHIFT):
						select[0] = list(select[1])
					
					ast_selected1[1] = None
					
					
					request_layout |= {3}
				
				elif mousedown1\
				and mousedown1[1][0] == "minimap":
					h_screen = ys[6] - ys[5]
					h_doc = eh
					h_bar = max(5,int(round(min(1,float(h_screen) / h_doc) * h_screen)))
					h_sp_doc = max(0,h_doc - h_screen)
					h_sp_bar = max(1,h_screen - h_bar)
					
					y_bar = e.pos[1] - ys[5] - h_bar / 2.
					scrolls1[ct][0] = int(round(min(1,float(y_bar) / h_sp_bar) * h_sp_doc))
					
					# TODO CTRL + MU set ast_selected1
					# TODO CTRL + SCROLL y-zoom minimap
					
					
					request_layout |= {3}
				
				if menu_actions:
					menu_pos2 = e.pos
					
					
					request_layout |= {5}
			
			if e.type == pygame.MOUSEMOTION:
				#print "MM",e.pos
				if mousedown1\
				and mousedown1[1][0] == actionTab:
					if rearrange1 < 0:
						rearrange_pos1 = [
							e.pos[0] - e.rel[0] - mousedown1[0][0] - txs[0] // 2,
							e.pos[1] - e.rel[1] - mousedown1[0][2] - tys[0] // 2,
						]
					
					rearrange1 = mousedown1[1][1]
					rearrange2 = mousedown1[1][1]
					
					rearrange_pos2 = [
						e.pos[0] - rearrange_pos1[0],
						e.pos[1] - rearrange_pos1[1],
					]
					rearrange_pos2[0] = max(xs[1] - txs[1] // 2 + txs[0] // 2,rearrange_pos2[0])
					rearrange_pos2[0] = min(xs[6] - txs[1] // 2 - txs[0] // 2 - 1,rearrange_pos2[0])
					rearrange_pos2[1] = max(ys[1] - tys[1] // 2 + tys[0] // 2,rearrange_pos2[1])
					rearrange_pos2[1] = min(ys[2] - tys[1] // 2 - tys[0] // 2 - 1,rearrange_pos2[1])
					pos = [
						rearrange_pos2[0] + txs[1] // 2,
						rearrange_pos2[1] + tys[1] // 2,
					]
					
					mousedown = get_button(pos,buttons1_flat)
					if mousedown\
					and mousedown[1][0] == actionTab:
						rearrange2 = mousedown[1][1]
					
					
					request_layout |= {1}
			
			if e.type == pygame.MOUSEBUTTONUP:
				#print "MU",e.button,e.pos
				if e.button == 1:
					mousedown = get_button(e.pos,buttons1_flat)
					if mousedown1\
					and mousedown\
					and mousedown1[0] == mousedown[0]:
						if mousedown1[1][0] == "editor":
							if False and pygame.key.get_mods() & pygame.KMOD_CTRL: # TODO remove node expansion
								if select_i1:
									select[:] = [select[1],select[0]]
								expand_selection(1)
								select[:] = [select[1],select[0]]
								expand_selection(-1)
								if not select_i1:
									select[:] = [select[1],select[0]]
								
								ast_selected1[1] = None
								
								request_layout |= {3}
						
						if mousedown1[1][0] == "switch":
							state[mousedown1[1][1]] ^= 1
							
							request_layout |= {4}
							
							if isinstance(mousedown1[1][4],types.FunctionType):
								mousedown1[1][4](mousedown1[1][5])
						
						if mousedown1[1][0] == "choose":
							if not mousedown1[1][3]\
							and not state[mousedown1[1][1]][mousedown1[1][2]]: # not selected
								for value in state[mousedown1[1][1]]:
									state[mousedown1[1][1]][value] = 0
							
							state[mousedown1[1][1]][mousedown1[1][2]] ^= 1
							
							request_layout |= {4}
							
							if isinstance(mousedown1[1][4],types.FunctionType):
								mousedown1[1][4](mousedown1[1][5])
						
						if mousedown1[1][0] == "button":
							if isinstance(mousedown1[1][4],types.FunctionType):
								mousedown1[1][4](mousedown1[1][5])
						
						if isinstance(mousedown1[1][0],types.FunctionType)\
						and mousedown1[1][0] != actionAccordion:
							mousedown1[1][0](mousedown1[1][1])
						
						## TODO ...
					
					if rearrange1 >= 0:
						rearrange(rearrange1,rearrange2,files)
						rearrange(rearrange1,rearrange2,titles)
						rearrange(rearrange1,rearrange2,lines)
						rearrange(rearrange1,rearrange2,selects)
						rearrange(rearrange1,rearrange2,scrolls1)
						rearrange(rearrange1,rearrange2,histories)
						rearrange(rearrange1,rearrange2,ctx1)
						
						actionTab(rearrange2)
						
						rearrange1 = -1
						rearrange2 = -1
					
					mousedown1 = None
				
				if e.button in {1,3} and menu_actions:
					
					mousedown = get_button(e.pos,buttons3_2)
					if mousedown\
					and isinstance(mousedown[1][0],types.FunctionType):
						
						if menu_mousedown\
						and menu_mousedown[1][0] == actionTab\
						and isinstance(mousedown[1][1],dict)\
						and mousedown[1][1]["set_ct"]:
							menu_mousedown[1][0](menu_mousedown[1][1])
						
						mousedown[1][0](mousedown[1][1])
				
				if menu_actions:
					menu_actions = None
					
					
					request_layout |= {0}
			
			if e.type == pygame.VIDEORESIZE:
				(width,height) = e.size
				select[1][2] = -1
				
				
				discard = 2 # discard events until validate layout
				dirty_editor = True
				
				request_resize = True
				
				request_layout |= {0}
				
				scroll1_resize = True
			
			if e.type == pygame.USEREVENT:
				for i,anim_ctr in enumerate(anim_ctrs): # continue animation
					if anim_ctrs[i]:
						request_layout |= {i}
					
					anim_ctrs[i] -= 1
					anim_ctrs[i] = max(0,anim_ctrs[i])
				
				if not max(anim_ctrs): # stop animation
					pygame.time.set_timer(pygame.USEREVENT,0)
					timer_set = False
			
			if e.type == pygame.USEREVENT + 1: # save tabs and update accordion
				
				pygame.time.set_timer(pygame.USEREVENT + 1,0) # stop timer
				
				
				request_layout |= {4}
				
				request_save = True
		
		
		evtloop()
	
	
	
	js.eval("window.ready = true;")
	
	
	
except: print traceback.format_exc()


