# -*- coding: utf-8 -*-



class pygame:
	
	DOUBLEBUF = 0
	RESIZABLE = 0
	
	QUIT = 1
	KEYDOWN = 2
	MOUSEBUTTONDOWN = 3
	MOUSEBUTTONUP = 4
	MOUSEMOTION = 5
	VIDEORESIZE = 6
	USEREVENT = 7
	
	KMOD_CTRL = 1
	KMOD_SHIFT = 2
	
	@staticmethod
	def init(*arg):
		#print "API CALL: init"
		pass
	
	class display:
		
		@staticmethod
		def flip(*arg):
			#print "API CALL: display.flip"
			pass
		
		@staticmethod
		def get_surface(*arg):
			#TODO
			#print "API CALL: display.get_surface"
			pass
		
		@staticmethod
		def set_caption(*arg):
			#print "API CALL: display.set_caption"
			js.eval("document.title = '" + hexEncode(arg[0]) + "';")
		
		@staticmethod
		def set_mode(*arg):
			#print "API CALL: display.set_mode"
			pass
	
	class event:
		
		class Event:
			
			def __init__(self,*arg):
				#print "API CALL: event.Event.__init__"
				
				self.type = arg[0]
				self.key = arg[1]
				self.mod = arg[2]
				self.unicode = arg[3]
				self.button = arg[4]
				self.pos = arg[5]
				self.rel = arg[6]
				self.size = arg[7]
				
				if self.mod >= 0:
					pygame.key.mods = self.mod
				
				#print (self.type,self.key,self.mod,self.unicode,self.button,self.pos,self.rel,self.size)
		
		@staticmethod
		def events_available(*arg):
			#print "API CALL: event.events_available"
			return len(event_queue)
		
		@staticmethod
		def get(*arg):
			global event_queue
			#print "API CALL: event.get"
			tmp = event_queue
			event_queue = []
			return tmp
	
	class key:
		
		mods = 0
		
		@staticmethod
		def get_mods(*arg):
			#print "API CALL: key.get_mods"
			return pygame.key.mods
		
		@staticmethod
		def set_repeat(*arg):
			#print "API CALL: key.set_repeat"
			pass
	
	class mixer:
		
		@staticmethod
		def quit(*arg):
			#print "API CALL: mixer.quit"
			pass
	
	class scrap:
		
		@staticmethod
		def get(*arg):
			#TODO
			#print "API CALL: scrap.get"
			if arg[0] == "text/plain;charset=utf-8":
				return unicode(js.eval("document.getElementById('clipboard').value")).encode("utf-8","ignore")
		
		@staticmethod
		def init(*arg):
			#print "API CALL: scrap.init"
			pass
		
		@staticmethod
		def put(*arg):
			#TODO
			#print "API CALL: scrap.put"
			if arg[0] == "text/plain;charset=utf-8":
				js.eval("document.getElementById('clipboard').value = '" + hexEncode(arg[1].decode("utf-8","ignore")) + "';")
				js.eval("window.copycommand();")
				js.eval("setTimeout(window.copycommand,10);")
	
	class surfarray:
		
		class pixels2d:
			
			data = None
			
			def __init__(self,*arg):
				#TODO
				#print "API CALL: surfarray.pixels2d.__init__"
				pass
	
	class time:
		
		@staticmethod
		def set_timer(*arg):
			#print "API CALL: time.set_timer"
			
			n = arg[0] - pygame.USEREVENT + 1
			
			js.eval("clearInterval(window.userevent%d_timer);" % n)
			
			if arg[1] > 0:
				js.eval(("window.userevent%d_timer = setInterval(window.userevent%d," + str(arg[1]) + ");") % (n,n))
		
		class Clock:
			
			def __init__(self,*arg):
				#print "API CALL: time.Clock.__init__"
				self.time = int(js.eval("Date.now();"))
			
			def tick(self,*arg):
				#print "API CALL: time.Clock.tick"
				tmp = self.time
				self.time = int(js.eval("Date.now();"))
				return self.time - tmp


